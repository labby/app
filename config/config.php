<?php

if(defined('LEPTON_PATH')) { die('By security reasons it is not permitted to load \'config.php\' twice!! Forbidden call from \''.$_SERVER['SCRIPT_NAME'].'\'!'); }

// config file created by LEPTON 5.1.0

define('LEPTON_PATH', dirname(dirname(__FILE__)));
define('LEPTON_URL', 'http://localhost');
define('ADMIN_PATH', LEPTON_PATH.'/backend');
define('ADMIN_URL', LEPTON_URL.'/backend');

define('LEPTON_GUID', '0120d680-6cfc-439a-9eac-047143df59c0');

if (!defined('LEPTON_INSTALL')) require_once(LEPTON_PATH.'/framework/initialize.php');

?>