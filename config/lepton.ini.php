;
; <?php die(); ?>
; This file is part of LEPTON Core, released under the GNU GPL
; Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
;
; NOTICE:LEPTON CMS Package has several different licenses.
; Please see the individual license in the header of each single file or info.php of modules and templates.
;
; @author          LEPTON Project
; @copyright       2010-2020 LEPTON Project
; @link            https://www.LEPTON-cms.org
; @license         http://www.gnu.org/licenses/gpl.html
; @license_terms   please see LICENSE and COPYING files in your package
;
;

; DataBase-setup for LEPTON-CMS
;
;   Re-written by upgrade-script for L* 4.6.0
;

[database]
type = 'mysql'
host = 'localhost'
port = '3306'
user = 'root'
pass = 'usbw'
name = 'app'
prefix = 'lep_'
key = 'e414cdd477a946e283bd439a3a26fb03'
cipher = 'AES-256-CBC'
iv = 'l6:%@*r-KE2n_GD1'
options = '0'

[system_const]
AUTH_MIN_LOGIN_LENGTH = '3'
AUTH_MAX_LOGIN_LENGTH = '128'
AUTH_MIN_PASS_LENGTH = '6'
AUTH_MAX_PASS_LENGTH = '128'
SUBVERSION = ' - OS-App 1.0.0'
MAX_REGISTRATION_TIME = 3600
MAX_WYSIWYG_HISTORY = 6

[custom_vars]
additional_email_chars = ''
additional_usernames_chars = 'äöüÄÖÜ'
