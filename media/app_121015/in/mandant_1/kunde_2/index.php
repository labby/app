<?php

/**
 * @module          App-Basic
 * @author          cms-lab
 * @copyright       2018-2019 cms-lab
 * @link            https://cms-lab.com
 * @license         Custom License
 * @license_terms   see license
 *
 */

header('Location: ../');

?>