<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php



/**
 *	Define that this file has been loaded
 *
 *	To avoid double function-declarations (inside LEPTON) and to avoid a massiv use
 *	of "if(!function_exists('any_function_name_here')) {" we've to place it
 *	inside this condition-body!
 *
 */
if ( !defined( 'FUNCTIONS_FILE_LOADED' ) )
{
	define( 'FUNCTIONS_FILE_LOADED', true );

	// global array to catch header files
	global $HEADERS, $FOOTERS;
	$HEADERS = array(
		'frontend' => array(
			'css' => array(),
			'js' => array()
		),
		'backend' => array(
			'css' => array(),
			'js' => array()
		) 
	);
	
	$FOOTERS = array(
		'frontend' => array(
			'script' => array(),
			'js' => array() 
		),
		'backend' => array(
			'script' => array(),
			'js' => array() 
		) 
	);

	// include function files
	$file_names = array (
		"addItems",	
		"change_mode",
		"create_access_file",		
		"createGUID",		
		"delete_page",
		"directory_list",
		"extract_permission",
		"file_list",
		"get_active_sections",		
		"get_menu_title",
		"get_modul_version",		
		"get_page_footers",
		"get_page_headers",	
		"get_page_title",
		"get_page_trail",
		"get_parent_ids",
		"get_parent_titles",
		"get_subs",	
		"get_variable_content",
		"is_parent",
		"js_alert_encode",
		"level_count",
		"load_language",
		"load_module",
		"load_module_language",   // New in LEPTON 2 - load the module specific language file (used in backend/frontend)		
		"load_template",
		"make_dir",
		"make_thumb",
		"page_link",
		"rm_full_dir",	
		"root_parent",
		"save_filename",		
		"scan_current_dir",
		"upgrade_module"
	);
	 
	LEPTON_handle::register($file_names);

}
?>