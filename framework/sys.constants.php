<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2022 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/**
 *	definitions for LEPTON
 *
 */
if (!defined('CORE'))
{
    define('CORE', 'LEPTON');
}

if( (!defined ('LEPTON_INSTALL')) && (!defined ('LEPTON_VERSION')) )
{
	$lep_version = LEPTON_database::getInstance()->get_one('SELECT value from '.TABLE_PREFIX.'settings WHERE name = "lepton_version"');	
	define('LEPTON_VERSION', $lep_version);
}

/**
 * Constants used in field 'statusflags'of table 'users'      
 */
define( 'USERS_DELETED', 1 ); // user marked as deleted
define( 'USERS_ACTIVE', 2 ); // user is activated
define( 'USERS_CAN_SETTINGS', 4 ); // user can change own settings
define( 'USERS_CAN_SELFDELETE', 8 ); // user can delete himself
define( 'USERS_PROFILE_ALLOWED', 16 ); // user can create a profile page
define( 'USERS_PROFILE_AVAIL', 32 ); // user has fullfilled profile and can not be deleted via core
define( 'USERS_DEFAULT_SETTINGS', USERS_ACTIVE | USERS_CAN_SETTINGS );  // provided as statusflags = 6 in users table

if(!defined ('LEPTON_INSTALL'))
{
    /**
     *	Setting the correct default timezone
     *	to avoid "date" conflicts and warnings
     *
     */
    $timezone_string = ( isset( $_SESSION[ 'TIMEZONE_STRING' ] ) ? $_SESSION[ 'TIMEZONE_STRING' ] : DEFAULT_TIMEZONE_STRING );
    date_default_timezone_set( $timezone_string );
 
    // Get users date format
    define( 'DATE_FORMAT', ( isset( $_SESSION[ 'DATE_FORMAT' ] ) ? $_SESSION[ 'DATE_FORMAT' ] : DEFAULT_DATE_FORMAT ) );

    // Get users time format
    define( 'TIME_FORMAT', ( isset( $_SESSION[ 'TIME_FORMAT' ] ) ? $_SESSION[ 'TIME_FORMAT' ] : DEFAULT_TIME_FORMAT ) );

    // Set Theme dir
    define( 'THEME_URL', LEPTON_URL . '/templates/' . DEFAULT_THEME );
    define( 'THEME_PATH', LEPTON_PATH . '/templates/' . DEFAULT_THEME );

    // Set Module dir
    define( 'MODULE_URL', LEPTON_URL . '/modules');
    define( 'MODULE_PATH', LEPTON_PATH . '/modules');

    // Set Secure dir
    define( 'SECURE_URL', LEPTON_URL . '/temp/secure');
    define( 'SECURE_PATH', LEPTON_PATH . '/temp/secure');

} 
else 
{
   // Set Theme dir
    define( 'THEME_URL', LEPTON_URL . '/templates/lepsem');
    define( 'THEME_PATH', LEPTON_PATH . '/templates/lepsem');
}

