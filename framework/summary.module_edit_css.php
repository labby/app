<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * @class			module_edit_css
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

/**
 *	FILE REQUIRED TO EDIT THE OPTIONAL MODULE CSS FILES
 *
 * This file contains routines to edit optional module files (frontend.css and backend.css) and provides a global solution for all modules.
 * To use this function, include this file from your module (e.g. from modify.php) and simply call the function edit_css('your_module_directory') - that's it.
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


// include function files
$file_names = array (
	"mod_file_exists",		// Checks if the specified optional module file exists
	"edit_module_css",		// This function displays the "Edit CSS" button in modify.php
	"toggle_css_file"		// This function displays a button to toggle between CSS files (invoked from edit_css.php)
);
LEPTON_handle::register($file_names);

?>