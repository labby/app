<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2022 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
class LEPTON_tools
{
	/**
	 *	To use "var_dump" instead of "print_r" inside the "display"-method.
	 *
	 *	@property bool	For the use of 'var_dump'.
	 */
	static public $use_var_dump = false;
	
	/**
	 *	Method to change the var_dump_mode
	 *
	 *	@param	boolean	True, to use "var_dump" instead of "print_r" for the "display"-method, false if not. Default is "true".
	 *	@see	display
	 *
	 */
	static public function use_var_dump( $bUseVarDump=true )
	{
		self::$use_var_dump = (bool) $bUseVarDump;
	}
	
	/**
	 *	Method to return the result of a "print_r" call for a given object/address.
	 * 
	 *	@param	mixed 	Any (e.g. mostly an object instance, or e.g. an array)
	 *	@param	string	Optional a "tag" (-name). Default is "pre".
	 *	@param	string	Optional a class name for the tag.
	 *
	 *	example given:
	 *	@code{.php}
	 *		LEPTON_tools::display( $result_array, "code", "example_class" )
	 *	@endcode
	 *		will return:
	 *	@code{.xml}
	 *
	 *		<code class="example_class">
	 *			array( [1] => "whatever");
	 *		</code>
	 *
	 *	@endcode
	 *
	 */
	static function display( $something_to_display ="", $tag="pre", $css_class=NULL ) 
	{
	
		$s = "\n<".$tag.( NULL === $css_class ? "" : " class='".$css_class."'").">\n";
		ob_start();
			(true === self::$use_var_dump)
			? var_dump( $something_to_display )
			: print_r( $something_to_display )
			;
		$s .= ob_get_clean();
		$s .= "\n</".$tag.">\n";
	
		return $s;
	}

	
	/**
	 *	Method to convert a base64 string into an image file
	 * 
	 *	@param	string 	Any valid base64 string
	 *	@param	string	Any local path
	 *
	 *	example given:
	 *	@code{.php}
	 *		LEPTON_tools::base64ToImage($base64_string, $output_file);
	 *	@endcode
	 *	
	 *
	 */	
			// create signature image
	static function base64ToImage($base64_string, $output_file) 
	{
		$file = fopen($output_file, "wb");

			$data = explode(',', $base64_string);

			fwrite($file, base64_decode($data[1]));
			fclose($file);

			return $output_file;
	}
}
