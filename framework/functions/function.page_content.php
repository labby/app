<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		page_content
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


	function page_content( $block = 1 )
	{
		// Get outside objects
		global $TEXT, $MENU, $HEADING, $MESSAGE;
		global $globals;
		global $database;
		global $oLEPTON;

		if ( $oLEPTON->page_access_denied === true )
		{
			echo $MESSAGE[ 'FRONTEND_SORRY_NO_VIEWING_PERMISSIONS' ];
			return;
		}
		if ( $oLEPTON->page_no_active_sections === true )
		{
			echo $MESSAGE[ 'FRONTEND_SORRY_NO_ACTIVE_SECTIONS' ];
			return;
		}

		// Make sure block is numeric
		if ( !is_numeric( $block ) )
		{
			$block = 1;
		}
		// Include page content
		if ( !defined( 'PAGE_CONTENT' ) || $block != 1 )
		{
			$page_id = intval( $oLEPTON->page_id );
			// set session variable to save page_id only if PAGE_CONTENT is empty
			$_SESSION[ 'PAGE_ID' ] = !isset( $_SESSION[ 'PAGE_ID' ] ) ? $page_id : $_SESSION[ 'PAGE_ID' ];
			// set to new value if page_id changed and not 0
			if ( ( $page_id != 0 ) && ( $_SESSION[ 'PAGE_ID' ] <> $page_id ) )
			{
				$_SESSION[ 'PAGE_ID' ] = $page_id;
			}
			
			// First get all sections for this page
			$aSections = array();
			$database->execute_query( 
				"SELECT section_id,module,publ_start,publ_end from ".TABLE_PREFIX."sections WHERE page_id = '".$page_id."'  AND block = '".$block."' ORDER BY position",
				true,
				$aSections,
				true
			);			

			// If none were found, check if default content is supposed to be shown
			if ( count($aSections) == 0 )
			{
				if ( $oLEPTON->default_block_content == 'none' )
				{
					return true;
				}
				if ( is_numeric( $oLEPTON->default_block_content ) )
				{
					$page_id = $oLEPTON->default_block_content;
				}
				else
				{
					$page_id = $oLEPTON->default_page_id;
				}

				$aSections = array();
				$database->execute_query( 
					"SELECT section_id,module,publ_start,publ_end from ".TABLE_PREFIX."sections WHERE page_id = '".$page_id."'  AND block = '".$block."' ORDER BY position",
					true,
					$aSections,
					true
				);	
				// Still no cotent found? Give it up, there's just nothing to show!
				if ( empty($aSections))
				{
					return false;
				}
			}
			// Loop through them and include their module file
			foreach ($aSections as $section)
			{
				// skip this section if it is out of publication-date
				$now = time();
				if ( !( ( $now <= $section[ 'publ_end' ] || $section[ 'publ_end' ] == 0 ) && ( $now >= $section[ 'publ_start' ] || $section[ 'publ_start' ] == 0 ) ) )
				{
					continue;
				}
				$section_id = $section[ 'section_id' ];
				$module     = $section[ 'module' ];
				// make a anchor for every section.
				if ( defined( 'SEC_ANCHOR' ) && SEC_ANCHOR != '' )
				{
					echo '<a class="section_anchor" id="' . SEC_ANCHOR . $section_id . '"></a>';
				}
				// check if module exists - feature: write in errorlog
				if ( file_exists( LEPTON_PATH . '/modules/' . $module . '/view.php' ) )
				{
					// fetch content -- this is where to place possible output-filters (before highlighting)
					ob_start(); // fetch original content
					require( LEPTON_PATH . '/modules/' . $module . '/view.php' );
					$content = ob_get_clean();
				}
				else
				{
					continue;
				}
				
                // highlights searchresults
                // Aldus: L* 5.3
                $sSearchResult  = (LEPTON_request::getRequest("searchresult")   ?? NULL );
                $sString        = (LEPTON_request::getRequest("sstring")        ?? "");
                $sNoHighlight   = (LEPTON_request::getRequest("nohighlight")    ?? NULL );
				
				//if ( isset( $_GET[ 'searchresult' ] ) && is_numeric( $_GET[ 'searchresult' ] ) && !isset( $_GET[ 'nohighlight' ] ) && isset( $_GET[ 'sstring' ] ) && !empty( $_GET[ 'sstring' ] ) )
				if( ($sSearchResult != NULL) && ($sNoHighlight === NULL ) && ($sString != "") )
				{
					$arr_string = explode( " ", $sString );
					if ( intval($sSearchResult) == 2 ) // exact match
					{
						$arr_string[ 0 ] = str_replace( "_", " ", $arr_string[ 0 ] );
					}
					echo search_highlight( $content, $arr_string );
				}
				else
				{
					echo $content;
				}
			}
		}
		else
		{
			require( PAGE_CONTENT );
		}
	}


?>