<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		media_dirs_rw
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/**
 * Create directories recursive
 *
 * @param string   $dir_name - directory to create
 * @param ocatal   $dir_mode - access mode
 * @return boolean result of operation
 *
 * @internal ralf 2011-08-05 - added recursive parameter for mkdir()
 *
 */
function make_dir($dir_name, $dir_mode = OCTAL_DIR_MODE) : bool
{
    if (!is_dir($dir_name))
    {
        $umask = umask(); // get current mask
        mkdir($dir_name, $dir_mode, true);
        umask($umask);
        return true;
    }
    return false;
}
