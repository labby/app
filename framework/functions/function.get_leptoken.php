<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		get_leptoken
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/**
 *	Try to geht the current leptoken.
 *
 *	@param	int		Any root-(page) id. Default = 0.
 *
 */
function get_leptoken() {
	//  [1] -- first get
	$leptoken = filter_input(
	    INPUT_GET,
	    "leptoken",
	    FILTER_VALIDATE_REGEXP,
	    [
	         "options" => [
	            "regexp"    => "~^[a-zA-Z0-9\.]{32}$~",
	            "default"   => NULL
	         ]
	    ]
	);
	//  [2] -- not found - look for the $_get 
	if( NULL === $leptoken )
	{
        $leptoken = filter_input(
            INPUT_POST,
            "leptoken",
            FILTER_VALIDATE_REGEXP,
            [
                 "options" => [
                    "regexp"    => "~^[a-zA-Z0-9\.]{32}$~",
                    "default"   => NULL
                 ]
            ]
        );
	}
	//  [3] --- not found -- look for a diff. name
    if( NULL === $leptoken )
	{
        $leptoken = filter_input(
            INPUT_GET,
            "amp;leptoken",
            FILTER_VALIDATE_REGEXP,
            [
                 "options" => [
                    "regexp"    => "~^[a-zA-Z0-9\.]{32}$~",
                    "default"   => NULL
                 ]
            ]
        );
	}
	
	return ( $leptoken ?? "" );
}
 