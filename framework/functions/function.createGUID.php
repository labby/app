<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		createGUID
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


	/**
	 * Generate a globally unique identifier (GUID)
	 * Uses COM extension under Windows otherwise
	 * create a random GUID in the same style
	 * @return STR GUID
	 */
	function createGUID()
	{
		if ( function_exists( 'com_create_guid' ) )
		{
			$guid = com_create_guid();
			$guid = strtolower( $guid );
			if ( strpos( $guid, '{' ) == 0 )
			{
				$guid = substr( $guid, 1 );
			} //strpos( $guid, '{' ) == 0
			if ( strpos( $guid, '}' ) == strlen( $guid ) - 1 )
			{
				$guid = substr( $guid, 0, strlen( $guid ) - 1 );
			} //strpos( $guid, '}' ) == strlen( $guid ) - 1
			return $guid;
		} //function_exists( 'com_create_guid' )
		else
		{
			return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x', 
			    random_int( 0, 0xffff ),
			    random_int( 0, 0xffff ),
			    random_int( 0, 0xffff ),
			    random_int( 0, 0x0fff ) | 0x4000,
			    random_int( 0, 0x3fff ) | 0x8000,
			    random_int( 0, 0xffff ),
			    random_int( 0, 0xffff ),
			    random_int( 0, 0xffff )
			);
		}
	} // end function createGUID()

?>