<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		root_parent
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


	/**
	 * Function to work out root parent
	 *
	 * @access public
	 * @param  integer $page_id
	 * @return integer ID of the root page
	 *
	 **/
	function root_parent( $page_id )
	{
		global $database;
		// Get page details
		$aDetails = array();
		$database->execute_query( 
			"SELECT parent, level from ".TABLE_PREFIX."pages WHERE page_id = '".$page_id."' ",
			true,
			$aDetails,
			false
		);

		$parent     = $aDetails[ 'parent' ];
		$level      = $aDetails[ 'level' ];
		
		if ( $level == 1 )
		{
			return $parent;
		} 
		elseif ( $parent == 0 )
		{
			return $page_id;
		} 
		else
		{
			// Figure out what the root parents id is
			$parent_ids = array_reverse( get_parent_ids( $page_id ) );
			return $parent_ids[ 0 ];
		}
	}

?>