<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		droplet_exists
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/**
 * Check if the Droplet $droplet_name exists in a WYSIWYG section of $page_id or
 * if the Droplet is placed in a NEWs or TOPICs article.
 * 
 * @param string $droplet_name
 * @param integer $page_id
 * @param array $option
 * @return boolean true on success
 */
function droplet_exists($droplet_name, $page_id, &$option=array()) : bool
{
    $database   = LEPTON_database::getInstance();
    $iPageId    = 0;
    $iSectionId = 0;
	
    if (isset($option['POST_ID']) || defined('POST_ID')) {
        // Droplet may be placed at a NEWs article
        $post_id = defined('POST_ID') ? POST_ID : $option['POST_ID'];
		$iPageId = $database->get_one("SELECT page_id FROM ".TABLE_PREFIX."mod_news_posts WHERE post_id = ".$post_id." AND ((content_long LIKE '%[[".$droplet_name."?%') OR (content_long LIKE '%[[".$droplet_name."]]%')) ");
        if ($iPageId > 0)
        {
            return true;
        }
    }
    
    if (isset($option['TOPIC_ID']) || defined('TOPIC_ID')) {
        // Droplet may be placed at a TOPICs article
        $topic_id = defined('TOPIC_ID') ? TOPIC_ID : $option['TOPIC_ID'];
		$iPageId = $database->get_one("SELECT page_id FROM ".TABLE_PREFIX."mod_topics WHERE topic_id = ".$topic_id." AND ((content_long LIKE '%[[".$droplet_name."?%') OR (content_long LIKE '%[[".$droplet_name."]]%')) ");
        if ($iPageId > 0)
        {
            return true;
        }
    }

	$iSectionId = $database->get_one("SELECT section_id FROM ".TABLE_PREFIX."mod_wysiwyg WHERE page_id = ".$page_id." AND ((text LIKE '%[[".$droplet_name."?%') OR (text LIKE '%[[".$droplet_name."]]%')) ");
    if ($iSectionId > 0)
    {
        return true;
    }
    
    return false;
}

?>