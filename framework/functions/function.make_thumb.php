<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		make_thumb
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/**
 *	Generate a thumbnail from an image
 *
 */
function make_thumb( $source, $destination, $size )
{
	// Check if GD is installed
	if(extension_loaded('gd') && function_exists('imageCreateFromJpeg') && function_exists('imageCreateFromPng'))
	{
		
		// Get the type
		$aTemp = explode(".", $source);
		$sMimeType = strtolower( array_pop($aTemp) );
		
		// First figure out the size of the thumbnail
		list($original_x, $original_y) = getimagesize($source);
		if ($original_x > $original_y)
		{
			$thumb_w = $size;
			$thumb_h = $original_y*($size/$original_x);
		}
		if ($original_x < $original_y)
		{
			$thumb_w = $original_x*($size/$original_y);
			$thumb_h = $size;
		}
		if ($original_x == $original_y)
		{
			$thumb_w = $size;
			$thumb_h = $size;	
		}
		
		// Now make the thumbnail
		switch( $sMimeType )
		{
		    case "jpeg":
		    case "jpg":
		    
		        $source = imageCreateFromJpeg($source);
		        $dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);
		        imagecopyresampled($dst_img,$source,0,0,0,0,$thumb_w,$thumb_h,$original_x,$original_y);
		        imagejpeg($dst_img, $destination, 90);
		        
		        break;
		        
		    case "png":
		    
		        $source = imagecreatefrompng ($source);
		        $dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);
		        
                imagealphablending( $dst_img, false );
                imagesavealpha( $dst_img, true );		        
		        
		        imagecopyresampled($dst_img,$source,0,0,0,0,$thumb_w,$thumb_h,$original_x,$original_y);
		        imagepng($dst_img, $destination);

		        break;
		        
		    case "gif":
		    
		        $source = imagecreatefromgif ($source);
		        $dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);
		        imagecopyresampled($dst_img,$source,0,0,0,0,$thumb_w,$thumb_h,$original_x,$original_y);
		        imagegif($dst_img, $destination);		        
		        break;
		        
		    default:
		        return false;
		}
		
		// Clear memory
		imagedestroy($dst_img);
		imagedestroy($source);
        
		return true;
	} else {
		return false;
	}
}

?>