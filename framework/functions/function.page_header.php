<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		page_header
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/**
 *	Function to get the page header
 *
 *	@param	bool	Return-Mode: true returns the value, false will direct output the string.
 *
 */

	function page_header( $mode = false )
	{
		echo "Deprecated: please use 'echo WEBSITE_HEADER' in your frontend template instead of function 'page_header'";		
		if ( true === $mode )
		{
			return WEBSITE_HEADER;
		}
		else
		{
			echo WEBSITE_HEADER;
			return true;
		}
	}

?>