<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		load_language
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


	/**
	 *  Load language information from a given language-file into the current DB
	 *
	 *  @param  string  Any valid path.
	 *
	 */
	function load_language( $file )
	{
		global $database, $admin, $MESSAGE;
		if ( file_exists( $file ) && preg_match( '#^([A-Z]{2}[_custom]{0,1}.php)#', basename( $file ) ) )
		{
			$language_license  = null;
			$language_code     = null;
			$language_version  = null;
			$language_guid     = null;
			$language_name     = null;
			$language_author   = null;
			$language_platform = null;
			
			require $file;
			
			if ( $language_name != NULL )
			{
				if (    ( $language_license == NULL )
				    ||  ( $language_code == NULL )
				    ||  ( $language_version == NULL )
				    ||  ( $language_guid == NULL ) 
				)
				{
					$admin->print_error( $MESSAGE[ "LANG_MISSING_PARTS_NOTICE" ], $language_name );
				}
				
				// Check that it doesn't already exist
				$sqlwhere = '`type` = \'language\' AND `directory` = \'' . $language_directory . '\'';
				$sql      = 'SELECT COUNT(*) FROM `' . TABLE_PREFIX . 'addons` WHERE ' . $sqlwhere;
				if ( $database->get_one( $sql ) )
				{
					$sql_job = "update";
				}
				else
				{
					$sql_job = "insert";
					$sqlwhere = '';
				}

				$fields = array(
					'directory'	=> $language_directory,
					'name'		=> $language_name,
					'type'		=> 'language',
					'version'	=> $language_version,
					'platform'	=> $language_platform,
					'author'	=> addslashes( $language_author ),
					'license'	=> addslashes( $language_license ),
					'guid'		=> $language_guid,
					'description' => ""
				);

				$database->build_and_execute(
					$sql_job,
					TABLE_PREFIX . "addons",
					$fields,
					$sqlwhere
				);
				
				if ( $database->is_error() )
				{
					$admin->print_error( $database->get_error() );
				}
			}
		}
	}

?>