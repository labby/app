<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		rm_full_dir
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


	/**
	 *	Function to remove a non-empty directory
	 *
	 *	@param string $directory
	 *	@return boolean
	 */
	function rm_full_dir( $directory )
	{
		// If suplied dirname is a file then unlink it
		if ( is_file( $directory ) )
		{
			return unlink( $directory );
		}
		//	Empty the folder
		if ( is_dir( $directory ) )
		{
			$dir = dir( $directory );
			while ( false !== $entry = $dir->read() )
			{
				// Skip pointers
				if ( $entry == '.' || $entry == '..' )
				{
					continue;
				}
				// Deep delete directories
				if ( is_dir( $directory . '/' . $entry ) )
				{
					rm_full_dir( $directory . '/' . $entry );
				}
				else
				{
					unlink( $directory . '/' . $entry );
				}
			}
			// Now delete the folder
			$dir->close();
			return rmdir( $directory );
		}
	}
	// end function rm_full_dir()

?>