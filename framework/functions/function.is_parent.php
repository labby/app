<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function		is_parent
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/**
 * check if the page with the given id has children
 *
 * @access public
 * @param  integer $page_id - page ID
 * @return mixed   (false if page hasn't children, parent id if not)
 *
 */

function is_parent( $page_id )
{
    global $database;
    
    // Get parent
    $parent = intval($database->get_one( 'SELECT `parent` FROM `' . TABLE_PREFIX . 'pages` WHERE `page_id` = ' . $page_id ));
    // If parent isnt 0 return its ID
    if ( $parent == 0 )
    {
        return false;
    }
    else
    {
        return $parent;
    }
}
