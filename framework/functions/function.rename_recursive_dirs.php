<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 *
 * @function        rename_recursive_dirs
 * @author          LEPTON Project
 * @copyright       2010-2022 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ %s ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/**
 *	Recursive function to rename all subdirectories and contents
 *  (PHP-function "rename" did not work on all windows installations)
 *
 *	@param	str		A valid path to the source directory
 *	@param	str		A valid path to the destination directory
 *	@param	int		Counter for the recursion-deep. Default is 0.
 *	@return	bool
 *
 */

if (!function_exists("make_dir"))
{
    require_once dirname(__FILE__)."/function.make_dir.php";
}

if (!function_exists("change_mode"))
{
    require_once dirname(__FILE__)."/function.change_mode.php";
}

if (!function_exists("rm_full_dir"))
{
    require_once( dirname(__FILE__)."/function.rm_full_dir.php");
}

function rename_recursive_dirs( $dirsource, $dirdest, $deep=0 )
{
    if ( true === is_dir($dirsource) )
    {
        if(false === is_dir($dirdest))
        {
            make_dir($dirdest);
        }
        
        $dir= dir($dirsource);
        while ( $file = $dir->read() )
        {
            if( $file[0] != "." )
            {
                if( !is_dir($dirsource."/".$file) )
                {
                    copy ($dirsource."/".$file, $dirdest."/".$file);
                    change_mode($dirdest."/".$file);
                } else {
                    make_dir($dirdest."/".$file);
                    rename_recursive_dirs($dirsource."/".$file, $dirdest.'/'.$file, $deep+1);
                }
            }
        }
        $dir->close();
    }
    if ($deep == 0)
    {
        rm_full_dir( $dirsource );
    }
    return true;
}
