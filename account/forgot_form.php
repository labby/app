<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://www.gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// Aldus 2021-07-23
global $info_text;

// init empty string as we've got no message to display here in this line!
$info_text = "";

// create hash 
$confirm_hash = time();

// create confirmation link
$enter_pw_link = LEPTON_URL.'/account/new_password.php?hash='.$confirm_hash;

// Check if the user has already submitted the form, otherwise show it
if( (isset($_POST['email'])) && ($_POST['email'] != "") && (true === LEPTON_handle::checkEmailChars( $_POST['email']) ) )
{
	$email = strip_tags($_POST['email']);

	//	check if mail is in database
	$subscriber = array();
	$database->execute_query(
		"SELECT * FROM `".TABLE_PREFIX."users` WHERE email = '".$email."' ",
		true,
		$subscriber,
		false
	);

	if(empty($subscriber)) 
	{
		// info that email doesn't exist	
		$info_text = $MESSAGE['FORGOT_PASS_EMAIL_NOT_FOUND'];
	
	} 
	else 
	{
		// Check if the password has been reset in the last 2 hours
		$last_reset = intval($subscriber['login_ip']); 
		$time_diff = time()-$last_reset; // Time since last reset in seconds
		if($time_diff < MAX_REGISTRATION_TIME)
		{
			// Tell the user that their password cannot be reset more than once per hour
			$info_text = $MESSAGE['FORGOT_PASS_ALREADY_RESET'];	
	
		} 
		else 
		{
			//send confirmation link to email
			//Create a new PHPMailer instance and clear adresses
			$mail = LEPTON_mailer::getInstance();
		
			$mail->CharSet = DEFAULT_CHARSET;	
			//Set who the message is to be sent from
			$mail->setFrom(SERVER_EMAIL);
			//Set who the message is to be sent to
			$mail->addAddress($email);
			//Set the subject line
			$mail->Subject = $MESSAGE['SIGNUP2_SUBJECT_LOGIN_INFO'];
			//Switch to TEXT messages
			$mail->IsHTML(true);
			$mail->Body = sprintf($MESSAGE['FORGOT_PASS_PASSWORD_CONFIRM'],$enter_pw_link,$enter_pw_link);	

			//send the message, check for errors
			if (!$mail->send()) {
				$info_text = "Mailer Error: " . $mail->ErrorInfo;
			} else {
				//save into database
				$fields = array(
					'login_ip'=>	$confirm_hash
				);
				$database->build_and_execute( 'UPDATE', TABLE_PREFIX."users", $fields,"email = '".$email."'");
											
				if ( $database->is_error() ) {
					// Error updating database
					$info_text = $database->get_error();
				}
			
				$info_text = $MESSAGE['FORGOT_PASS_PASSWORD_RESET'];						
			
			}
		}
	}
}

if($info_text == '')
{
	$info_text = $MESSAGE['FORGOT_PASS_NO_DATA'];
	$info_color = '000000';
} else {
	$info_color = 'FF0000';
}
	
// see if there exists a frontend template file or use the fallback
if( true === LEPTON_handle::require_alternative('/templates/'.DEFAULT_TEMPLATE.'/frontend/login/forgot_form.php') )
{
	die();
}
else
{

	// Include template engine 
	$oTWIG = lib_twig_box::getInstance();
	
	// register path to make sure twig is looking in this module template folder first
	$oTWIG->registerPath( LEPTON_PATH."/account/templates/" );


	$hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'] );
	$_SESSION['wb_apf_hash'] = $hash;

	$redirect_url = (isset($_SESSION['HTTP_REFERER']) ? $_SESSION['HTTP_REFERER'] : "");

	unset($_SESSION['result_message']);

	$data = array( 
		'MESSAGE_COLOR'		=>	$info_color,
		'MESSAGE'		    =>	$info_text,
		'FORGOT_URL'		=>	FORGOT_URL,  
		'URL'			    =>	$redirect_url,
		'AUTH_MAX_LOGIN_LENGTH'	=>	AUTH_MAX_LOGIN_LENGTH,		
		'HASH'				=>	$hash
	);

	echo $oTWIG->render("forgot_form.lte", $data);		

}
