<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         https://www.gnu.org/licenses/gpl-3.0.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php



require_once(LEPTON_PATH.'/modules/captcha_control/captcha/captcha.php');

if (isset($_GET['err']) && (int)($_GET['err']) == ($_GET['err'])) {
	$err_msg = '';
	switch ((int)$_GET['err']) {
		case 1: $err_msg = $MESSAGE['USERS_NO_GROUP']; break;
		case 2: $err_msg = $MESSAGE['USERS_NAME_INVALID_CHARS'].' / '.$MESSAGE['USERS_USERNAME_TOO_SHORT']; break;
		case 3: $err_msg = $MESSAGE['USERS_INVALID_EMAIL']; break;
		case 4: $err_msg = $MESSAGE['SIGNUP_NO_EMAIL']; break;
		case 5: $err_msg = $MESSAGE['MOD_FORM_INCORRECT_CAPTCHA']; break;
		case 6: $err_msg = $MESSAGE['USERS_USERNAME_TAKEN']; break;
		case 7: $err_msg = $MESSAGE['USERS_EMAIL_TAKEN']; break;
		case 8: $err_msg = $MESSAGE['USERS_INVALID_EMAIL']; break;
		case 9: $err_msg = $MESSAGE['FORGOT_PASS_CANNOT_EMAIL']; break;
		default: echo "<p style='color:red'>no matching case!</p>"; break;
	}
	if ($err_msg != '') {
		echo "<p style='color:red'>$err_msg</p>";
	}
}


// see if there exists a frontend template file or use the fallback
if( true === LEPTON_handle::require_alternative('/templates/'.DEFAULT_TEMPLATE.'/frontend/login/signup_form.php') )
{
    return 0;
}
else
{
	/* Include template engine */
	$oTWIG = lib_twig_box::getInstance();
	
	// register path to make sure twig is looking in this module template folder first
	$oTWIG->registerPath( LEPTON_PATH."/account/templates/" );


	// additional hash
	$hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'] );
	$_SESSION['wb_apf_hash'] = $hash;

	/**
	 *	Getting the captha
	 *
	 */
	ob_start();
		call_captcha( 
			captcha_control::getInstance()->pluginName,
			NULL,
			NULL
		);
		$captcha = ob_get_clean();

	$submitted_when = time();
	$_SESSION['submitted_when'] = $submitted_when;

	unset($_SESSION['result_message']);

	$data = array(
		'TEMPLATE_DIR'	=>	TEMPLATE_DIR,
		'SIGNUP_URL'	=>	SIGNUP_URL,
		'LOGOUT_URL'	=>	LOGOUT_URL,
		'FORGOT_URL'	=>	FORGOT_URL,  
		'CALL_CAPTCHA'		=>	$captcha,
		'AUTH_MAX_LOGIN_LENGTH'	=>	AUTH_MAX_LOGIN_LENGTH,		
		'HASH'				=>	$hash, 
		'submitted_when'	=> $submitted_when
	);
	
	echo $oTWIG->render("signup_form.lte", $data);	
}
?>