### App
============

Complete software for the generation and sending of customer invoices and collection of delivery invoices including handover to the accounting.
Includes and extends the standard [LEPTON CMS][1], latest release.

#### Availabilty

* available for using local or on an usb-stick (including nginx server)
* available as package to install on your own webserver/webspace


#### Installation

depends on chosen package


#### Notice

For all/further details please see [homepage][4].


[1]: https://lepton-cms.org "LEPTON CMS"
[4]: https://cms-lab.com