<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!

if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


// Initial page
if( (class_exists("initial_page", true)) && (isset($_SESSION['USER_ID'])) && (!isset($_GET['CallByIcon']) ) )
{
    $oInitPage = initial_page::getInstance();
    $oInitPage->execute( $_SESSION['USER_ID'], $_SERVER['SCRIPT_NAME'] );
}


// get twig instance
$admin = LEPTON_admin::getInstance();
$oTWIG = lib_twig_box::getInstance();

//	Pre-load the theme langages 
LEPTON_basics::getInstance();

// get some infos and current release no
$is_uptodate = 0;
$latest_release = '';
$version_info = '';
$info = "https://lepton-cms.org/version_info.txt";
$version = "https://lepton-cms.org/version.txt";
$check = @file_get_contents($info, false);	

// check version for updates if possible
if(($check == true) || $check != '')
{

	$value = parse_ini_file(LEPTON_PATH.'/config/lepton.ini.php'); 
	if($value['pass'] != 'usbw')
		{
			$version_info = file_get_contents($info, false);
			$latest_release = file_get_contents($version, false);
			$is_uptodate = (version_compare( $latest_release, LEPTON_VERSION,  ">" )) ? 1 : 0;
		}
}

// see if there exists a frontend template file or use the fallback
if( true == LEPTON_handle::require_alternative('/templates/'.DEFAULT_THEME.'/backend/backend/start/index.php') )
{
    return 0;
}

// get pages and sections info
$pages = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."pages` ORDER BY `modified_when` DESC ",
	true,
	$pages,
	true
);

$last_pmodified = date("d.m.Y - H:i", $pages[0]['modified_when']);
$page_link_fe = LEPTON_URL.PAGES_DIRECTORY.$pages[0]['link'].PAGE_EXTENSION;
$page_link_be = ADMIN_URL.'/pages/modify.php?page_id='.$pages[0]['page_id'];

$count_pages = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."pages`");
$count_section = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."sections` ");
$count_sections = ($count_section -1);

// get addons info
$count_modules = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."addons` WHERE `type`='module' ");
$count_templates = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."addons` WHERE `type`='template' ");
$count_languages = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."addons` WHERE `type`='language' ");

// get users and groups info
$count_users = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."users` ");
$count_groups = $database->get_one("SELECT COUNT(*) FROM `".TABLE_PREFIX."groups` ");

$page_values = array(
	'count_sections'=> $count_sections,
	'count_pages'	=> $count_pages,
	'count_modules' => $count_modules,
	'count_templates'=> $count_templates,
	'count_languages'=> $count_languages,
	'count_users' 	=> $count_users,
	'count_groups' 	=> $count_groups,	
	'last_pmodified'=> $last_pmodified,
	'lepton_link' 	=> 'https://lepton-cms.org',
	'page_link_fe' 	=> $page_link_fe,
	'page_link_be' 	=> $page_link_be,	
	'current_release'=> $latest_release,
	'is_uptodate' 	=> $is_uptodate,
	'version_info' 	=> $version_info,
	'php_version'	=> PHP_VERSION,
	'THEME'	=> $THEME
);

$oTWIG->registerPath( THEME_PATH."theme","start" );
echo $oTWIG->render(
	"@theme/start.lte",
	$page_values
);

$admin->print_footer();

?>