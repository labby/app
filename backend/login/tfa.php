<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 *
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

	$oTFA = LEPTON_tfa::getInstance();
	$oTFA->initialize($_SESSION['USERNAME']);
	if ($oTFA->key_new == 1)
	{			

		if(isset($_POST['save']) && $_POST['save'] != '' && isset($_POST['token'] ))
		{
			$oTFA->set_be_pin('save');
			exit();
		} 
		else 
		{
			header('Location: '.ADMIN_URL.'/logout/index.php');
			exit();
		}
		
	} 
	else 
	{
		if (isset($_POST['job']) && $_POST['job'] == 'forward' && isset($_POST['token'] ))
		{
			
			$oTFA->display_be_pin('forward');
			exit();
		}
		else 
		{
			header('Location: '.ADMIN_URL.'/logout/index.php');
			exit();
		}		
	}
 


?>