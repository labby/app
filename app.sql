-- Adminer 4.8.1 MySQL 5.1.72-community dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `app` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `app`;

DROP TABLE IF EXISTS `lep_addons`;
CREATE TABLE `lep_addons` (
  `addon_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) NOT NULL DEFAULT '',
  `directory` varchar(128) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `function` varchar(255) NOT NULL DEFAULT '',
  `version` varchar(255) NOT NULL DEFAULT '',
  `guid` varchar(50) DEFAULT NULL,
  `platform` varchar(255) NOT NULL DEFAULT '',
  `author` varchar(255) NOT NULL DEFAULT '',
  `license` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`addon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_addons` (`addon_id`, `type`, `directory`, `name`, `description`, `function`, `version`, `guid`, `platform`, `author`, `license`) VALUES
(1,	'module',	'captcha_control',	'Captcha and Advanced-Spam-Protection (ASP) Control',	'Admin-Tool to control CAPTCHA and ASP',	'tool',	'2.4.0',	'c29c5f1a-a72a-4137-b5cd-62982809bd38',	'5.x',	'W. Studer, LEPTON Project',	'GNU General Public License'),
(2,	'module',	'code2',	'Code2',	'This module allows you to execute PHP, HTML, Javascript commands and internal comments (<span style=\"color:#FF0000;\">limit access to users you can trust!</span>).',	'page',	'2.9.0',	'e5e36d7f-877a-4233-8dac-e1481c681c8d',	'5.0.x',	'Ryan Djurovich, Chio Maisriml, Thorn, Aldus.',	'GNU General Public License'),
(3,	'module',	'droplets',	'Droplets',	'This tool allows you to manage your local droplets.',	'tool',	'2.5.1',	'8b5b5074-993e-421a-9aff-2e32ae1601d5',	'5.x',	'LEPTON Project',	'GNU General Public License'),
(4,	'module',	'edit_area',	'Editarea',	'Small and easy code editor.',	'wysiwyg',	'2.2.1',	'7E293596-59AC-4010-8351-5836313DE387',	'5.x',	'Christophe Dolivet, Christian Sommer, LEPTON Project',	'GNU General Public License'),
(5,	'module',	'initial_page',	'Initial Page',	'This module allows to set up an initial_page in the backend for each user.',	'tool',	'1.4.1',	'237D63F7-4199-48C7-89B2-DF8E2D8AEE5F',	'5.x',	'LEPTON project',	'copyright, all rights reserved'),
(225,	'language',	'RU',	'Russian',	'',	'',	'2.3',	'38748A1C-C86D-4B8A-8793-2796ED4CB282',	'3.x',	'konstantinmsk',	'GNU General Public License'),
(7,	'module',	'lib_jquery',	'jQuery Initial Library',	'This module installs basic files of jQuery JavaScript Library. You may use it as a lib for your own JavaScripts and modules.',	'library',	'3.6.0.3',	'8FB09FFD-B11C-4B75-984E-F54082B4DEEA',	'5.x',	'LEPTON Project',	'GNU General Public License'),
(8,	'module',	'lib_lepton',	'LEPTON Library',	'Library, scripts to improve LEPTON',	'library',	'1.2.2',	'64ed06d4-c3f6-4e88-b7f3-248594c2f9a7',	'5.x',	'LEPTON team, several independent authors',	'GNU General Public License'),
(9,	'module',	'lib_phpmailer',	'PHPMailer Library',	'PHP Mailer for LEPTON',	'library',	'6.5.4.0',	'5BF5013A-1204-4AE7-88B2-2E2662AF0E4D',	'5.x',	'Andy Prevost, Marcus Bointon, Brent R. Matzelle',	'GNU General Public License'),
(10,	'module',	'lib_r_filemanager',	'Responsive Filemanager',	'Filemanager for use with LEPTON',	'library',	'9.14.0.8',	'071e6320-e081-4d50-a3a7-e84bd8080f2d',	'5.x',	'Alberto Peripolli, LEPTON team',	'special license, see <a href=\"http://localhost/modules/lib_r_filemanager/license.txt\" target=\"_blank\">included license file</a>.'),
(11,	'module',	'lib_search',	'LEPTON Search Engine',	'The search engine for LEPTON CMS',	'library',	'2.2.3',	'3EAE6351-30A1-4DDB-8F25-A2EB3CF3ECE5',	'5.x',	'LEPTON team',	'GNU General Public License'),
(13,	'module',	'lib_twig',	'Twig Library for LEPTON',	'Twig PHP Template Engine. Please visit <a href=\"https://twig.symfony.com/\" target=\"_new\">https://twig.symfony.com/\" for details.</a>',	'library',	'3.3.8.0',	'19fb9aba-7f31-4fee-81ea-1db03e83c6cc',	'5.x',	'sensiolabs.org, LEPTON Team',	'GNU General Public License for LEPTON Addon, https://twig.symfony.com/license for Twig'),
(14,	'module',	'menu_link',	'Menu Link',	'This module allows you to insert a link into the menu.',	'page',	'3.5.0',	'452f0da3-3bc1-43bc-b2ad-491ae8494c6e',	'5.x',	'Ryan Djurovich, thorn, LEPTON Project ',	'GNU General Public License'),
(15,	'module',	'news',	'News',	'This page type is designed for making a news page (including patch with backend pagination and image upload).',	'page',	'4.0.4',	'200a3816-e0f6-4fb9-aea8-8e7749896a34',	'5.x',	'Ryan Djurovich, Rob Smith, Christian M. Stefan, Jurgen Nijhuis, Dietrich Roland Pehlke (last)',	'GNU General Public License'),
(16,	'module',	'quickform',	'QuickForm',	'This module allows you to create a quick and simple form without complicated settings using TWIG template engine.',	'page',	'2.0.2',	'6d1a4304-88f0-4356-a1bf-7b5fcb69cf9f',	'5.x',	'Ruud Eisinga, LEPTON project, W. Studer',	'<a href=\"http://www.gnu.org/licenses/lgpl.html\" target=\"_blank\">GNU Lesser General Public License</a>'),
(17,	'module',	'show_menu2',	'show_menu2',	'A code snippet providing a complete replacement for the built-in menu functions. See <a href=\"https://doc.lepton-cms.org/documentation/sm2\" target=\"_blank\">documentation</a> for details or view the <a href=\"http://localhost/modules/show_menu2/README.en.txt\" target=\"_blank\">readme</a> file.',	'snippet',	'5.1.0.3',	'b859d102-881d-4259-b91d-b5a1b57ab100',	'5.x',	'Brodie Thiesfield, Aldus, erpe',	'GNU General Public License'),
(224,	'language',	'PL',	'Polski',	'',	'',	'2.3',	'1e84df1e-850d-4782-86a8-e560e7ebb90f',	'3.x',	'Marek Stepien, Krzysztof Winnicki',	'GNU General Public License'),
(19,	'module',	'wrapper',	'Wrapper',	'This module allows you to wrap your site around another using an inline frame.',	'page',	'2.10.1',	'a5830654-06f3-402a-9d25-a03c53fc5574',	'4.x',	'Ryan Djurovich, Dietrich Roland Pehlke (last)',	'GNU General Public License'),
(20,	'module',	'wysiwyg',	'WYSIWYG',	'This module allows you to edit the contents of a page using a graphical editor, including draft and history',	'page',	'4.0.1',	'DA07DFA3-7592-4781-89C2-D549DD77B017',	'5.x',	'LEPTON project, thanks to Thorn for the idea',	'GNU General Public License'),
(21,	'module',	'wysiwyg_admin',	'wysiwyg Admin',	'This module allows to manage some basic settings of the choosen wysiwyg-editor.',	'tool',	'2.3.2',	'895FD071-DA62-4E90-87C8-F3E11BC1F9AB',	'5.x',	'Dietrich Roland Pehlke (Aldus)',	'<a href=\"http://www.gnu.org/licenses/lgpl.html\" target=\"_blank\">lgpl</a>'),
(215,	'template',	'talgos',	'Talgos Theme',	'Backend theme for LEPTON CMS',	'theme',	'1.2.4',	'60de3b94-3659-451c-a876-b9e17b49784b',	'5.x',	'LEPTON project',	'<a href=\"http://www.gnu.org/licenses/gpl.html\">GNU General Public License</a>'),
(212,	'template',	'blank',	'Blank',	'This template is for use on page where you do not want anything wrapping the content.',	'template',	'1.1.1',	'8f6b513e-ee82-47d8-a0d2-415a06ec8f0a',	'Lepton 1.x',	'erpe',	'<a href=\"http://www.gnu.org/licenses/gpl.html\">GNU General Public License</a>'),
(214,	'template',	'semantic',	'Semantic-Frontend',	'This template bases on <a href=\"https://fomantic-ui.com\" target=\"_blank\">Fomantic</a>',	'template',	'3.0.7',	'd4365367-db03-4f3c-8304-42e88abc008f',	'5.0',	'CMS-LAB',	'https://creativecommons.org/licenses/by/3.0/'),
(128,	'module',	'app_supplier',	'App Supplier',	'Part of complete App',	'page',	'1.0.0',	'bf02f557-8d4c-41df-b4cf-1f0c61224432',	'5.x',	'cms-lab',	'<a href=\"https://os-app.org/free/license.php\" target=\"_blank\">Custom License</a>'),
(221,	'language',	'FR',	'Française',	'',	'',	'2.3',	'32E0F6E0-2FA3-4033-9F9D-77E0EA3B4745',	'3.x',	'Frédéric Bonain',	'GNU General Public License'),
(223,	'language',	'NL',	'Nederlands',	'',	'',	'2.3',	'18bb3637-6f95-4a81-b1c0-796df2d326f0',	'3.x',	'bpe',	'GNU General Public License'),
(222,	'language',	'IT',	'Italiano',	'',	'',	'2.3',	'5b92610a-2f4f-456c-b3db-28edf506a52d',	'3.x',	'Mte90',	'GNU General Public License'),
(220,	'language',	'FI',	'Finnish',	'',	'',	'2.3',	'2a4f8878-0b11-4715-8910-bfc719024727',	'3.x',	'Jouni Reivolahti',	'GNU General Public License'),
(217,	'language',	'DE',	'Deutsch',	'',	'',	'2.3',	'f49419c8-eb27-4a69-bffb-af61fce6b0c9',	'3.x',	'Stefan Braunewell, Matthias Gallas, LEPTON project',	'GNU General Public License'),
(219,	'language',	'EN',	'English',	'',	'',	'2.3',	'1412c11c-378f-44ea-9a0e-a9223a2027ef',	'3.x',	'Ryan Djurovich, Christian Sommer',	'GNU General Public License'),
(218,	'language',	'DK',	'Dansk',	'',	'',	'2.3',	'2BF67D57-4B19-40CC-A63E-50CBDD81048D',	'3.x',	'Hoerts',	'GNU General Public License'),
(213,	'template',	'lepsem',	'Semantic Theme',	'Semantic Backend Theme for LEPTON CMS',	'theme',	'2.3.3',	'd910f924-3aa5-491e-92eb-73f7469629e9',	'5.x',	'cms-lab',	'<a href=\"http://www.gnu.org/licenses/gpl.html\">GNU General Public License</a>'),
(211,	'template',	'app',	'App',	'This template is for use on page where you do not want anything wrapping the content.',	'template',	'1.0.0',	'3aca2615-ad97-4e71-ba82-f5e708efc68c',	'Lepton 4.x',	'erpe',	'<a href=\"http://www.gnu.org/licenses/gpl.html\">GNU General Public License</a>'),
(38,	'module',	'tinymce',	'TinyMCE',	'<a href=\"https://www.tiny.cloud\" target=\"_blank\">Current TinyMCE </a>allows you to edit the content<br />of a page and see media image folder.\r\n						<br />Please keep in mind that there are <a href=\"http://localhost/modules/tinymce/tinymce/TinyMCE-kbd-shortcuts-rev1701.pdf\" target=\"_blank\"><b>shortcuts</b></a> to use tinymce',	'wysiwyg',	'5.10.3.0',	'0ad7e8dd-2f6b-4525-b4bf-db326b0f5ae8',	'5.x',	'erpe, Aldus',	'GNU General Public License, TINYMCE is LGPL'),
(39,	'module',	'cookie',	'Cookie',	'Tool to get users informed about cookies.',	'tool',	'3.3.0',	'd7a7c31e-a197-45a4-9131-66297f0c0cc8',	'5.x',	'<a href=\"http://cms-lab.com\" target=\"_blank\">CMS-LAB</a>',	'<a href=\"http://cms-lab.com/_documentation/cookie/license.php\" class=\"info\" target=\"_blank\">Custom license</a>'),
(216,	'language',	'CZ',	'Czech',	'',	'',	'2.3',	'b6bb0738-b323-46b9-924e-3d068413653a',	'3.x',	'Ryan Djurovich, Aleš Kuklínek',	'GNU General Public License'),
(55,	'module',	'addon_info',	'Addon Info',	'Get all addons listed on LEPAdoR.',	'tool',	'1.1.1',	'ec2a7bfc-610d-4f82-b31b-41fafa7ce8ff',	'5.x',	'CMS-LAB',	'<a href=\"http://cms-lab.com/_documentation/addon_info/license.php\" target=\"_blank\">GNU General Public License</a>'),
(86,	'module',	'lib_fomantic',	'Fomantic Library',	'This module installs basic files <a href=\"https://fomantic-ui.com/\" target=\"_blank\">Fomantic UI</a>.',	'library',	'2.8.8.0',	'316ca2d0-cb0a-4d23-89d0-424a7a51e125',	'4.x',	'cms-lab',	'<a href=\"http://opensource.org/licenses/MIT\" target=\"_blank\">MIT license</a>'),
(87,	'module',	'reset_pin',	'Reset PIN',	'Tool to reset PIN if <a href=\'https://doc.lepton-cms.org/docu/english/home/core/features/two-factor-authentication.php\' class=\'admintools_link\' target=\'_blank\'>TFA</a> failed',	'tool',	'0.2.0',	'767281a0-096a-40dc-8987-bc132d76578b',	'5.x',	'<a href=\'https://cms-lab.com\' target=\'_blank\'>CMS-LAB</a>',	'<a href=\'https://cms-lab.com/_documentation/reset-pin/license.php\' class=\'admintools_link\' target=\'_blank\'>Custom license</a>'),
(108,	'module',	'app_admin',	'App Admin',	'Part of complete App',	'tool',	'1.0.0',	'43bacd08-86a7-404b-89a5-12d8373ee8a8',	'5.x',	'cms-lab',	'<a href=\"https://os-app.org/free/license.php\" target=\"_blank\">Custom License</a>'),
(109,	'module',	'app_basic',	'App Basic',	'Part of complete App',	'page',	'1.0.0',	'7ccfb652-609e-435e-a61c-4526f770e894',	'5.x',	'cms-lab',	'<a href=\"https://os-app.org/free/license.php\" target=\"_blank\">Custom License</a>'),
(110,	'module',	'lib_html2pdf',	'html2pdf Library',	'html2pdf allows the conversion of valid HTML in PDF format, to generate documents like invoices, documentation.',	'library',	'5.2.2.0',	'66519ed9-d6c1-4339-bcee-7bf955cd2488',	'5.x',	'cms-lab',	'https://github.com/spipu/html2pdf/blob/master/LICENSE.md'),
(126,	'module',	'app_customer',	'App Customer',	'Part of complete App',	'page',	'1.0.0',	'9d449353-de7b-45cc-bbd8-a216b370746d',	'5.x',	'cms-lab',	'<a href=\"https://os-app.org/free/license.php\" target=\"_blank\">Custom License</a>'),
(127,	'module',	'app_client',	'App Client',	'Part of complete App',	'page',	'1.0.0',	'b5018740-28d6-4716-b006-720c01cd773b',	'5.x',	'cms-lab',	'<a href=\"https://os-app.org/free/license.php\" target=\"_blank\">Custom License</a>'),
(176,	'module',	'lib_comp',	'Library-Compatibility',	'Library for LEPTON to secure backward compatibility for outdated addons',	'library',	'1.0.1',	'0e87e505-3a84-470b-a21e-85d3668a62b7',	'5.x',	'various',	'GNU General Public License'),
(193,	'module',	'app_accounts',	'App Accounts',	'Part of complete App',	'page',	'1.0.0',	'74779895-ec61-426b-8eea-d9b1c4544b39',	'5.x',	'cms-lab',	'<a href=\"https://os-app.org/free/license.php\" target=\"_blank\">Custom License</a>'),
(194,	'module',	'app_articles',	'App Articles',	'Part of complete App',	'page',	'1.0.0',	'635aff11-3df7-4396-88c5-0a56a9cba4d6',	'5.x',	'cms-lab',	'<a href=\"https://os-app.org/free/license.php\" target=\"_blank\">Custom License</a>'),
(195,	'module',	'app_suppliertasks',	'App Suppliertasks',	'Part of complete App',	'page',	'1.0.0',	'4141090c-42c7-491c-af66-6b25ff2d81de',	'5.x',	'cms-lab',	'<a href=\"https://os-app.org/free/license.php\" target=\"_blank\">Custom License</a>');

DROP TABLE IF EXISTS `lep_groups`;
CREATE TABLE `lep_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `system_permissions` text NOT NULL,
  `module_permissions` text NOT NULL,
  `template_permissions` text NOT NULL,
  `language_permissions` text NOT NULL,
  `backend_permission` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_groups` (`group_id`, `name`, `system_permissions`, `module_permissions`, `template_permissions`, `language_permissions`, `backend_permission`) VALUES
(1,	'Administrators',	'pages,pages_view,pages_add,pages_add_l0,pages_settings,pages_modify,pages_delete,media,media_view,media_upload,media_rename,media_delete,media_create,addons,modules,modules_view,modules_install,modules_uninstall,templates,templates_view,templates_install,templates_uninstall,languages,languages_view,languages_install,languages_uninstall,settings,settings_basic,settings_advanced,access,users,users_view,users_add,users_modify,users_delete,groups,groups_view,groups_add,groups_modify,groups_delete,admintools,service,preferences,preferences_access',	'',	'',	'',	1),
(2,	'Register',	'pages_view,pages,languages_view,languages,preferences,preferences_access',	'',	'',	'',	1);

DROP TABLE IF EXISTS `lep_log`;
CREATE TABLE `lep_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logged` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL DEFAULT '-1',
  `username` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(255) NOT NULL DEFAULT '',
  `log_text` text NOT NULL,
  `check` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_log` (`id`, `logged`, `user_id`, `username`, `action`, `log_text`, `check`, `comment`) VALUES
(1,	'2021-02-18 19:51:58',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'czwrppuoANM/VbME+wpJQg==',	'DXMIa7+U3eJ0fKjZH3hiV+IVekj7D/ZWfLDkkwqCIQ3LmkJGnTCJM6K7u90UaqywJ5gLDzrnaTLcaDcnZHDaFPQo/bTVU87Z1E14XXZJcNg=',	'2021-02-18 15:53:47',	'Test Zeiten'),
(2,	'2021-02-18 15:53:47',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'czwrppuoANM/VbME+wpJQg==',	'DXMIa7+U3eJ0fKjZH3hiV+IVekj7D/ZWfLDkkwqCIQ12Kao8NkBt8e67jA6GK+xR',	'2021-02-18 15:53:47',	''),
(3,	'2021-02-19 09:37:43',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-19 09:37:43',	''),
(4,	'2021-02-20 10:25:59',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-20 10:25:59',	''),
(5,	'2021-02-21 10:42:29',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-21 10:42:29',	''),
(6,	'2021-02-21 12:40:11',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-21 12:40:11',	''),
(7,	'2021-02-21 14:13:07',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-21 14:13:07',	''),
(8,	'2021-02-21 14:26:43',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-21 14:26:43',	''),
(9,	'2021-02-21 14:41:04',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'czwrppuoANM/VbME+wpJQg==',	'DXMIa7+U3eJ0fKjZH3hiV+IVekj7D/ZWfLDkkwqCIQ12Kao8NkBt8e67jA6GK+xR',	'2021-02-21 14:41:04',	''),
(10,	'2021-02-22 14:05:21',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-22 14:05:21',	''),
(11,	'2021-02-22 14:26:14',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-22 14:26:14',	''),
(12,	'2021-02-23 12:03:43',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-02-23 12:03:43',	''),
(13,	'2021-04-13 17:50:04',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2021-04-13 16:50:04',	''),
(14,	'2022-05-21 11:36:46',	1,	'4e3MU8qCbs9hHKNI71Uprg==',	'FU0TF9wzlGx6R98CzDMI4Q==',	'QnaOdFJjtZR5/Atqw63p/Q556VxkDS3h7fB4XOEIJhcbn8E6Qeqioa6xy05g60YlxHisTfRiv3+ZGTzP5JVCKQ==',	'2022-05-21 10:36:46',	'');

DROP TABLE IF EXISTS `lep_mod_app_accounts`;
CREATE TABLE `lep_mod_app_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `skr` int(11) NOT NULL DEFAULT '3',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `account_type` int(11) NOT NULL DEFAULT '-1',
  `account_no` int(11) NOT NULL DEFAULT '-1',
  `account_name` varchar(256) NOT NULL DEFAULT '',
  `account_description` varchar(256) NOT NULL DEFAULT '',
  `need_details` int(11) NOT NULL DEFAULT '-1',
  `account_use` int(11) NOT NULL DEFAULT '-1' COMMENT 'display account_use table',
  `automatic_account` int(11) NOT NULL DEFAULT '0' COMMENT 'Automatisches Konto für UST-Berechnung/EU-Einkauf',
  `form_relation_1` int(11) NOT NULL DEFAULT '-1' COMMENT 'Formular Einnahme-Überschuss Rechnung',
  `form_relation_2` int(11) NOT NULL DEFAULT '-1' COMMENT 'Formular Zusammenfassende Meldung',
  `form_relation_3` int(11) NOT NULL DEFAULT '-1' COMMENT 'Formular Umsatzsteuererklärung',
  `vat_key` int(11) NOT NULL DEFAULT '-99' COMMENT 'Umsatzsteuerschlüssel',
  `eu_relation` int(11) NOT NULL DEFAULT '-99' COMMENT 'EU-Steuerschlüssel ja/nein',
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_no` (`account_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_accounts` (`id`, `client_id`, `skr`, `section_id`, `page_id`, `account_type`, `account_no`, `account_name`, `account_description`, `need_details`, `account_use`, `automatic_account`, `form_relation_1`, `form_relation_2`, `form_relation_3`, `vat_key`, `eu_relation`, `last_modified`, `active`) VALUES
(1,	1,	3,	7,	6,	1,	1200,	'cLgCpGTjHFvlzDfbpjxfmQ==',	'6WvLaz6yjcJZCglZsoWPMA==',	2,	-1,	0,	-1,	-1,	-1,	-99,	-99,	'2021-01-30 13:23:56',	1),
(2,	1,	3,	7,	6,	1,	1000,	've2qA03EwVpuYV9bYqr5bA==',	'o/8M7xvG1RF6etLVfCHqMA==',	1,	-1,	0,	-1,	-1,	-1,	-99,	-99,	'2021-01-28 18:24:43',	1),
(8,	1,	3,	7,	6,	1,	1002,	'jVqc23ecXDIV8BSb9XwzIA==',	'rEYf5LoNNjTFS9B4zJmStQ==',	1,	-1,	0,	-1,	-1,	-1,	-99,	-99,	'2021-01-29 12:39:33',	1),
(7,	1,	3,	7,	6,	1,	1001,	'o/8M7xvG1RF6etLVfCHqMA==',	'msB/s0rocz5Pbnkh6U+BBg==',	1,	-1,	0,	-1,	-1,	-1,	-99,	-99,	'2021-01-28 17:45:35',	1),
(10,	1,	3,	7,	6,	6,	3400,	'qZvK47MtFcpH9GFI/J+0rA==',	'XuKUAYj9m7mWQU0Gm5mDWX9jIkmi+NMNMf2Em2Dnp2k=',	-1,	45,	0,	1,	-1,	-1,	2,	-99,	'2021-02-11 15:27:55',	1),
(11,	1,	3,	7,	6,	5,	8400,	'8o+DQn+m1nRms1RPkEAC7Q==',	'r+Xr6+B9rKu/duWmhArrOg==',	-1,	26,	0,	6,	1,	5,	2,	-99,	'2021-02-11 14:26:14',	1),
(12,	1,	3,	7,	6,	5,	8336,	'Qsg13ih51VMTcMc4XOT2SuCzN3gxUNA+bc6z4PsEvgA=',	'U6wD3YpmSATPK+G7w0ATpkJKbh6ypLsPNKyCvwn/Ekb7Z1PdE3D74sNtVBKgMmEG',	-1,	26,	0,	7,	11,	9,	4,	1,	'2021-02-11 15:27:06',	1);

DROP TABLE IF EXISTS `lep_mod_app_accounts_booking`;
CREATE TABLE `lep_mod_app_accounts_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_type` varchar(256) NOT NULL DEFAULT '' COMMENT 'Zahlung, Überweisung, Buchung, etc',
  `action_date` date NOT NULL DEFAULT '0000-00-00' COMMENT 'Tag der Buchung, NICHT Aktualisierung',
  `account_no` int(11) NOT NULL DEFAULT '-1' COMMENT 'Konto-Nummer',
  `account_no_contra` int(11) NOT NULL DEFAULT '-1' COMMENT 'Gegenkonto-Nummer',
  `record_no` varchar(128) NOT NULL DEFAULT '' COMMENT 'Beleg-Nr.',
  `purpose` varchar(256) NOT NULL DEFAULT '' COMMENT 'Verwendungszweck',
  `debit` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT 'Account SOLL',
  `credit` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT 'Account HABEN',
  `note1` varchar(256) NOT NULL DEFAULT '',
  `last modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Aktualisierung der Buchung',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_mod_app_accounts_details_iban`;
CREATE TABLE `lep_mod_app_accounts_details_iban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '-1',
  `iban` varchar(256) NOT NULL DEFAULT 'DE',
  `bic` varchar(256) NOT NULL DEFAULT '',
  `owner` varchar(256) NOT NULL DEFAULT '',
  `bank_name` varchar(256) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_accounts_details_iban` (`id`, `account_id`, `iban`, `bic`, `owner`, `bank_name`, `active`) VALUES
(4,	1,	'BxyRXITpaAm24FkMD+EaJg==',	'dC5ct3N33f7omQggCaI+eA==',	'7x0EvLBVCwvIH31850Rndw==',	'zna9a8N7D0vdZ05Agk17RQ==',	1);

DROP TABLE IF EXISTS `lep_mod_app_accounts_details_start`;
CREATE TABLE `lep_mod_app_accounts_details_start` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '-1',
  `account_start` date NOT NULL DEFAULT '0000-00-00',
  `account_amount` decimal(15,2) NOT NULL DEFAULT '-1.00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_accounts_details_start` (`id`, `account_id`, `account_start`, `account_amount`, `active`) VALUES
(16,	2,	'2021-01-28',	547.35,	1),
(21,	1,	'2021-01-30',	58697.23,	1),
(14,	7,	'2021-01-28',	1200.45,	1);

DROP TABLE IF EXISTS `lep_mod_app_admin_account_type`;
CREATE TABLE `lep_mod_app_admin_account_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(256) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_admin_account_type` (`id`, `text`, `active`) VALUES
(1,	'Finanzkonten',	1),
(2,	'Kreditkartenkonten',	1),
(3,	'Vermögen',	1),
(4,	'Schulden',	1),
(5,	'Einnahmen',	1),
(6,	'Ausgaben',	1),
(7,	'Kapital',	1);

DROP TABLE IF EXISTS `lep_mod_app_admin_account_use`;
CREATE TABLE `lep_mod_app_admin_account_use` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(256) NOT NULL DEFAULT '',
  `account_type` int(11) NOT NULL DEFAULT '1',
  `extra` varchar(256) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_admin_account_use` (`id`, `text`, `account_type`, `extra`, `active`) VALUES
(1,	'Sachanlagen',	3,	'',	1),
(2,	'offene Kundenrechnungen',	3,	'',	1),
(3,	'Vorräte',	3,	'',	1),
(4,	'betriebseigene Wertpapiere',	3,	'',	1),
(5,	'Forderungen',	3,	'',	1),
(6,	'sonstige Vermögensgegenstände',	3,	'',	1),
(7,	'Schecks, Kassenbestand, Bankguthaben',	3,	'',	1),
(8,	'Aufwendungen Ingangsetzung Geschäftsbetrieb',	3,	'',	1),
(9,	'Ausstehende Einlagen auf das gezeichnete Kapital',	3,	'',	1),
(10,	'Immaterielle Vermögensgegenstände',	3,	'',	1),
(11,	'Finanzanlagen',	3,	'',	1),
(12,	'Aktive Rechnungsabgrenzungsposten',	3,	'',	1),
(13,	'aktive latente Steuern',	3,	'',	1),
(14,	'Verbindlichkeiten gegenüber Kreditinstituten',	4,	'',	1),
(15,	'Offene Lieferantenrechnungen',	4,	'',	1),
(16,	'Sonstige Schulden',	4,	'',	1),
(17,	'Kurzfristige betiebsbezogene Kredite',	4,	'',	1),
(18,	'Gewinnrücklagen',	4,	'',	1),
(19,	'Rückstellungen für Pensionen',	4,	'',	1),
(20,	'Steuerrückstellungen',	4,	'',	1),
(21,	'Sonstige Rückstellungen',	4,	'',	1),
(22,	'Passive Rechnungsabgrenzungsposten',	4,	'',	1),
(23,	'Langfristige Verbindlichkeiten',	4,	'',	1),
(24,	'Passive latente Steuern',	4,	'',	1),
(25,	'Anzahlungen',	4,	'',	1),
(26,	'Umsatzerlöse',	5,	'',	1),
(27,	'sonstige Erlöse',	5,	'',	1),
(28,	'andere aktivierte Eigenleistungen',	5,	'',	1),
(29,	'Eigenverbrauch',	5,	'',	1),
(30,	'Zinserträge',	5,	'',	1),
(31,	'außerordentliche Erträge',	5,	'',	1),
(32,	'Erträge aus anderen Wertpapieren und Ausleihen',	5,	'',	1),
(33,	'Erträge aus Beteiligungen',	5,	'',	1),
(34,	'Sonstiger Eigenverbrauch',	5,	'',	1),
(35,	'Zinsen',	6,	'',	1),
(36,	'Versicherungen und Beiträge',	6,	'',	1),
(37,	'Abschreibungen',	6,	'',	1),
(38,	'Personalkosten',	6,	'',	1),
(39,	'Telefon und Porto',	6,	'',	1),
(40,	'KFZ-Kosten',	6,	'',	1),
(41,	'Raumkosten',	6,	'',	1),
(42,	'Reise und Bewirtung',	6,	'',	1),
(43,	'Betriebliche Stuern',	6,	'',	1),
(44,	'Sonstige Ausgaben',	6,	'',	1),
(45,	'Kosten für Waren',	6,	'',	1),
(46,	'Aufwendungen für Leistungen',	6,	'',	1),
(47,	'Steuern vpm Einkommen und Ertrag',	6,	'',	1),
(48,	'Außerordentliche Ausgaben',	6,	'',	1),
(49,	'Abschreibungen auf Finanzanlagen und Wertpapiere',	6,	'',	1),
(50,	'Abschreibungen auf Vermögensgegenstände des Umlaufvermögens',	6,	'',	1),
(51,	'Abgaben zur Sozialversicherung',	6,	'',	1),
(52,	'Bestandsveränderungen an fertigen und unfertigen Erzeugnissen',	6,	'',	1),
(53,	'Private Entnahme von Geld und Gütern',	7,	'',	1),
(54,	'Private Einlage von Geld und Gütern',	7,	'',	1),
(55,	'Gezeichnetes Kapital',	7,	'',	1),
(56,	'Kapital zum Geschäftsjahresbeginn',	7,	'',	1),
(57,	'Kapitalrücklagen',	7,	'',	1),
(58,	'Gewinnrücklagen',	7,	'',	1),
(59,	'Sonstige Verbindlichkeiten',	7,	'',	1),
(60,	'Ausstehende Einlagen auf das gezeichnete Kapital',	7,	'',	1),
(61,	'Gewinnvortrag/Verlustvortrag',	7,	'',	1);

DROP TABLE IF EXISTS `lep_mod_app_admin_form_relation`;
CREATE TABLE `lep_mod_app_admin_form_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '-1',
  `text` varchar(256) NOT NULL DEFAULT '',
  `form_line` int(11) NOT NULL DEFAULT '-1',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_admin_form_relation` (`id`, `type`, `text`, `form_line`, `active`) VALUES
(1,	-1,	'Nicht Relevant',	0,	1),
(2,	1,	'Steuern, Versicherungen und Maut',	145,	1),
(3,	1,	'Abziehbare Bewirtungskosten',	175,	1),
(4,	1,	'Aufwendungen für Telekommunikation',	210,	1),
(5,	3,	'Lieferungen und sonstige Leistungen zu 19%',	177,	1),
(6,	1,	'Umsatzsteuerpflichtige Betriebseinnahmen',	112,	1),
(7,	1,	'Umsatzsteuerfreie, nicht umsatzsteuerbare Btriebseinnahmen',	103,	1),
(8,	2,	'Innergemeinschaftliche Lieferung',	0,	1),
(9,	3,	'Nicht steuerbare sonstige Leistungen',	721,	1),
(10,	2,	'Innergemeinschaftliche Lieferung Dreiecksgeschäft',	0,	1),
(11,	2,	'Innergemeinschaftliche Leistung',	0,	1);

DROP TABLE IF EXISTS `lep_mod_app_admin_month`;
CREATE TABLE `lep_mod_app_admin_month` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_admin_month` (`id`, `month`) VALUES
(1,	'Januar'),
(2,	'Februar'),
(3,	'März'),
(4,	'April'),
(5,	'Mai'),
(6,	'Juni'),
(7,	'Juli'),
(8,	'August'),
(9,	'September'),
(10,	'Oktober'),
(11,	'November'),
(12,	'Dezember');

DROP TABLE IF EXISTS `lep_mod_app_admin_payment_terms`;
CREATE TABLE `lep_mod_app_admin_payment_terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `days` int(11) NOT NULL DEFAULT '0',
  `percent` int(11) NOT NULL DEFAULT '0',
  `text` varchar(256) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_admin_payment_terms` (`id`, `days`, `percent`, `text`, `active`) VALUES
(1,	0,	0,	'sofort',	1),
(2,	10,	3,	'10 Tage: 3%, 30 Tage netto',	1),
(3,	30,	0,	'30 Tage netto',	1);

DROP TABLE IF EXISTS `lep_mod_app_admin_state`;
CREATE TABLE `lep_mod_app_admin_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(128) NOT NULL DEFAULT '',
  `capital` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_admin_state` (`id`, `state`, `capital`) VALUES
(1,	'Baden-Württemberg',	'Stuttgart'),
(2,	'Bayern',	'München'),
(3,	'Berlin',	'Berlin'),
(4,	'Brandenburg',	'Potsdam'),
(5,	'Bremen',	'Bremen'),
(6,	'Hamburg',	'Hamburg'),
(7,	'Hessen',	'Wiesbaden'),
(8,	'Mecklenburg-Vorpommern',	'Schwerin'),
(9,	'Niedersachsen',	'Hannover'),
(10,	'Nordrhein-Westfalen',	'Düsseldorf'),
(11,	'Rheinland-Pfalz',	'Mainz'),
(12,	'Saarland',	'Saarbrücken'),
(13,	'Sachsen',	'Dresden'),
(14,	'Sachsen-Anhalt',	'Magdeburg'),
(15,	'Schleswig-Holstein	',	'Kiel'),
(16,	'Thüringen',	'Erfurt');

DROP TABLE IF EXISTS `lep_mod_app_admin_vat_key`;
CREATE TABLE `lep_mod_app_admin_vat_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL DEFAULT '',
  `percent` int(11) NOT NULL DEFAULT '-1',
  `text` varchar(32) NOT NULL DEFAULT '',
  `vat_eu` int(11) NOT NULL DEFAULT '0',
  `relate_1` int(11) NOT NULL DEFAULT '-1',
  `relate_2` int(11) NOT NULL DEFAULT '-1',
  `relate_3` int(11) NOT NULL DEFAULT '-1',
  `relate_4` int(11) NOT NULL DEFAULT '-1',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_admin_vat_key` (`id`, `key`, `percent`, `text`, `vat_eu`, `relate_1`, `relate_2`, `relate_3`, `relate_4`, `active`) VALUES
(4,	'EUB',	0,	'EU steuerbefreit',	1,	41,	91,	791,	-1,	1),
(2,	'19%',	19,	'Inland 19%',	0,	81,	66,	320,	-1,	1),
(3,	'7%',	7,	'Inland 7%',	0,	86,	66,	320,	-1,	1),
(1,	'0',	0,	'Keine Steuer',	0,	-1,	-1,	-1,	-1,	1),
(0,	'0',	0,	'',	0,	-1,	-1,	-1,	-1,	1);

DROP TABLE IF EXISTS `lep_mod_app_admin_vat_relation`;
CREATE TABLE `lep_mod_app_admin_vat_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT '-1',
  `text` varchar(256) NOT NULL DEFAULT '',
  `form_line` int(11) NOT NULL DEFAULT '-1',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_admin_vat_relation` (`id`, `type`, `text`, `form_line`, `active`) VALUES
(1,	-1,	'Kein',	0,	1),
(2,	1,	'Steuerpflichtige Umsätze zum Steuersatzt von 7%',	86,	1),
(3,	2,	'Vorsteuerbeträge von Rechnungen anderer Unternehmer',	66,	1),
(4,	0,	'Vorsteuerbeträge aus Rechnungen von anderen Unternehmen',	320,	1);

DROP TABLE IF EXISTS `lep_mod_app_articles`;
CREATE TABLE `lep_mod_app_articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `article_type` int(11) NOT NULL DEFAULT '-1',
  `article_group` int(11) NOT NULL DEFAULT '-1',
  `article_no` int(11) NOT NULL DEFAULT '-1',
  `article_name` varchar(255) NOT NULL DEFAULT '',
  `article_unit` int(11) NOT NULL DEFAULT '-1',
  `in_description` varchar(255) NOT NULL DEFAULT '',
  `in_netto` decimal(15,2) NOT NULL DEFAULT '-1.00',
  `in_brutto` decimal(15,2) NOT NULL DEFAULT '-1.00',
  `in_vat_key` int(11) NOT NULL DEFAULT '-1',
  `in_account_id` int(11) NOT NULL DEFAULT '-1',
  `in_supplier_id` int(11) NOT NULL DEFAULT '-1',
  `out_description` varchar(255) NOT NULL DEFAULT '',
  `out_netto` decimal(15,2) NOT NULL DEFAULT '-1.00',
  `out_brutto` decimal(15,2) NOT NULL DEFAULT '-1.00',
  `out_vat_key` int(11) NOT NULL DEFAULT '-1',
  `out_account_id` int(11) NOT NULL DEFAULT '-1',
  `eu_vat_key` int(11) NOT NULL DEFAULT '-1',
  `eu_account_id` int(11) NOT NULL DEFAULT '-1',
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_no` (`article_no`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_articles` (`id`, `client_id`, `section_id`, `page_id`, `article_type`, `article_group`, `article_no`, `article_name`, `article_unit`, `in_description`, `in_netto`, `in_brutto`, `in_vat_key`, `in_account_id`, `in_supplier_id`, `out_description`, `out_netto`, `out_brutto`, `out_vat_key`, `out_account_id`, `eu_vat_key`, `eu_account_id`, `last_modified`, `active`) VALUES
(1,	1,	7,	6,	1,	1,	10,	'Mein Artikel',	1,	'Text Einkauf',	25.01,	29.76,	4,	10,	1,	'Test Verkauf',	50.27,	59.82,	2,	12,	-1,	-1,	'2021-02-11 15:38:30',	1);

DROP TABLE IF EXISTS `lep_mod_app_article_group`;
CREATE TABLE `lep_mod_app_article_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_article_group` (`id`, `client_id`, `section_id`, `page_id`, `name`, `active`) VALUES
(1,	1,	7,	6,	'Keine',	1),
(2,	1,	7,	6,	'Hardware',	1),
(3,	1,	7,	6,	'Software',	1),
(4,	1,	7,	6,	'Sonstiges',	1);

DROP TABLE IF EXISTS `lep_mod_app_article_type`;
CREATE TABLE `lep_mod_app_article_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_article_type` (`id`, `client_id`, `section_id`, `page_id`, `name`, `active`) VALUES
(1,	1,	7,	9,	'Dienstleistung',	1),
(2,	1,	7,	9,	'Handel',	1);

DROP TABLE IF EXISTS `lep_mod_app_article_units`;
CREATE TABLE `lep_mod_app_article_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(255) NOT NULL DEFAULT '',
  `name_short` varchar(255) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_article_units` (`id`, `client_id`, `section_id`, `page_id`, `name`, `name_short`, `active`) VALUES
(1,	1,	7,	6,	'Stück',	'Stk',	1);

DROP TABLE IF EXISTS `lep_mod_app_basic`;
CREATE TABLE `lep_mod_app_basic` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `random` int(11) NOT NULL DEFAULT '-1',
  `company` varchar(255) NOT NULL DEFAULT '',
  `extra_1` varchar(255) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `no` varchar(32) NOT NULL DEFAULT '',
  `zip` varchar(32) NOT NULL DEFAULT '',
  `city` varchar(128) NOT NULL DEFAULT '',
  `country` varchar(128) NOT NULL DEFAULT '',
  `fon` varchar(128) NOT NULL DEFAULT '',
  `fax` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `internet` varchar(128) NOT NULL DEFAULT '',
  `company_type` varchar(128) NOT NULL DEFAULT '',
  `legal_form` varchar(128) NOT NULL DEFAULT '',
  `state` int(2) NOT NULL DEFAULT '-1',
  `first_month` int(2) NOT NULL DEFAULT '-1',
  `tax_number` varchar(128) NOT NULL DEFAULT '',
  `vat_id` varchar(128) NOT NULL DEFAULT '',
  `bank_name` varchar(128) NOT NULL DEFAULT '',
  `iban` varchar(64) NOT NULL DEFAULT '',
  `bic` varchar(32) NOT NULL DEFAULT '',
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_basic` (`client_id`, `section_id`, `page_id`, `random`, `company`, `extra_1`, `street`, `no`, `zip`, `city`, `country`, `fon`, `fax`, `email`, `internet`, `company_type`, `legal_form`, `state`, `first_month`, `tax_number`, `vat_id`, `bank_name`, `iban`, `bic`, `last_modified`, `active`) VALUES
(1,	6,	5,	121015,	'0zmk6jtDWxFaAm4IeeKY4Q==',	'SQPC9NpbODzHlkuarqW/nw==',	'Vd0SroEcrZ934XnkYMTsjQ==',	'ZsFTxozxcRpki8tyvPpfpg==',	'kocgkTGiIERXX3xpetvBJQ==',	'dJBecGXRjHODp7C+XbAErA==',	'tylaVBWLdDz6zfFYgKKKLg==',	'QM5L4eUZSEpp/FYdO/ohKg==',	'JzvUx/bWij7VV+oxffRraA==',	'vYG8rLmjOCUJvBE9z2RjPQ==',	'1dEevJyMp1VRZc85Rv1Ggjt4Y6RhahD44qkFOuj3exs=',	'AJIoRzgih3oHzlOHV2DHtDm3TrKx5y0539Nkh/hNTWI=',	'ymFcc16Efu8oU+ApHODS6g==',	2,	1,	'rJnnwOWnrOs24VEgA/Owxg==',	'OdNs1K2S3VZRgxR/EFcfRQ==',	'FrOEz72I1ZS1Q+btdKTGdA==',	'QdglweNycnZ68GPVVG+E+Au0aNGSYW2YxTVHTGJbhbU=',	'tzcagtWafrF5J0346QRI1A==',	'2020-02-23 10:57:02',	1),
(2,	6,	5,	121015,	'tfSOsAu8+RpwyqIODxmxN904YiCO0uDHSEDhSYT8aFI=',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'xCTdGa1+8//h8r3SyUtg4XzDgbRl/gw2AnCOyLh/Pv8=',	'KSkkFtW09SFrHYibggpPHQ==',	6,	1,	'rJnnwOWnrOs24VEgA/Owxg==',	'OdNs1K2S3VZRgxR/EFcfRQ==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'2019-08-13 13:57:25',	1);

DROP TABLE IF EXISTS `lep_mod_app_basic_settings`;
CREATE TABLE `lep_mod_app_basic_settings` (
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `customer_account` int(11) NOT NULL DEFAULT '10000',
  `supplier_account` int(11) NOT NULL DEFAULT '30000',
  `customer_invoice` int(11) NOT NULL DEFAULT '1',
  `skr` int(11) NOT NULL DEFAULT '-1',
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_basic_settings` (`client_id`, `section_id`, `page_id`, `customer_account`, `supplier_account`, `customer_invoice`, `skr`, `last_modified`, `active`) VALUES
(1,	6,	5,	10000,	30000,	1,	3,	'2021-02-05 13:34:27',	1),
(2,	6,	5,	10000,	30000,	200,	-1,	'2019-08-13 19:07:00',	1);

DROP TABLE IF EXISTS `lep_mod_app_customer`;
CREATE TABLE `lep_mod_app_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `company_short` varchar(255) NOT NULL DEFAULT '',
  `company` varchar(255) NOT NULL DEFAULT '',
  `extra_1` varchar(255) NOT NULL DEFAULT '',
  `vat_id` varchar(128) NOT NULL DEFAULT '',
  `eu_trade` varchar(128) NOT NULL DEFAULT '',
  `company_type` varchar(128) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `no` varchar(32) NOT NULL DEFAULT '',
  `zip` varchar(32) NOT NULL DEFAULT '',
  `city` varchar(128) NOT NULL DEFAULT '',
  `country` varchar(128) NOT NULL DEFAULT 'Deutschland',
  `company_salutation` varchar(64) NOT NULL DEFAULT '',
  `company_name1` varchar(255) NOT NULL DEFAULT '',
  `company_name2` varchar(255) NOT NULL DEFAULT '',
  `fon` varchar(128) NOT NULL DEFAULT '',
  `fax` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `internet` varchar(128) NOT NULL DEFAULT '',
  `company_del` varchar(255) NOT NULL DEFAULT '',
  `extra_1_del` varchar(255) NOT NULL DEFAULT '',
  `street_del` varchar(255) NOT NULL DEFAULT '',
  `no_del` varchar(32) NOT NULL DEFAULT '',
  `zip_del` varchar(32) NOT NULL DEFAULT '',
  `city_del` varchar(128) NOT NULL DEFAULT '',
  `country_del` varchar(128) NOT NULL DEFAULT '',
  `bank_name` varchar(128) NOT NULL DEFAULT '',
  `iban` varchar(64) NOT NULL DEFAULT '',
  `bic` varchar(32) NOT NULL DEFAULT '',
  `sepa` int(11) NOT NULL DEFAULT '0',
  `account_no` int(11) NOT NULL DEFAULT '0',
  `payment_terms` varchar(32) NOT NULL DEFAULT '',
  `price_terms` varchar(32) NOT NULL DEFAULT '',
  `send_invoice` int(11) NOT NULL DEFAULT '0',
  `invoice_type` varchar(32) NOT NULL DEFAULT '',
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_customer` (`id`, `client_id`, `section_id`, `page_id`, `company_short`, `company`, `extra_1`, `vat_id`, `eu_trade`, `company_type`, `street`, `no`, `zip`, `city`, `country`, `company_salutation`, `company_name1`, `company_name2`, `fon`, `fax`, `email`, `internet`, `company_del`, `extra_1_del`, `street_del`, `no_del`, `zip_del`, `city_del`, `country_del`, `bank_name`, `iban`, `bic`, `sepa`, `account_no`, `payment_terms`, `price_terms`, `send_invoice`, `invoice_type`, `last_modified`, `active`) VALUES
(1,	1,	7,	6,	'Vs/n2wy23sKtejULYl+23A==',	'0zmk6jtDWxFaAm4IeeKY4Q==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'OdNs1K2S3VZRgxR/EFcfRQ==',	'nCUhfQZyjSHoDYy+3V/NMQ==',	'AJIoRzgih3oHzlOHV2DHtDm3TrKx5y0539Nkh/hNTWI=',	'HAhhfo/8vuCvw2ig51UdCg==',	'aGMdGtXZj2QDqmL88sG9Lw==',	'kocgkTGiIERXX3xpetvBJQ==',	'dJBecGXRjHODp7C+XbAErA==',	'tylaVBWLdDz6zfFYgKKKLg==',	'Herr',	'Klaus',	'Juppzupp',	'mjdNVuJcPRT/BS6kSeUH/w==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'vYG8rLmjOCUJvBE9z2RjPQ==',	'1dEevJyMp1VRZc85Rv1Ggjt4Y6RhahD44qkFOuj3exs=',	'deliver',	'ohne',	'deine str',	'20',	'80001',	'München',	'Deutschland',	'EsFGgkgKa1LaFg+AYlXrPg==',	'QdglweNycnZ68GPVVG+E+Au0aNGSYW2YxTVHTGJbhbU=',	'tzcagtWafrF5J0346QRI1A==',	0,	10001,	'3',	'0',	0,	'b2b.lte',	'2021-02-18 15:50:35',	1),
(2,	1,	7,	6,	'ESvCYqXuWisVol7JfuyDEA==',	'oOUbrfZEe6g9Cqw8Kp363w==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'OdNs1K2S3VZRgxR/EFcfRQ==',	'/7cSQzpKH48W5HrT1hY1EQ==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'Vd0SroEcrZ934XnkYMTsjQ==',	'm68P/wJJzDFEUVDDHAA9ng==',	'kocgkTGiIERXX3xpetvBJQ==',	'dJBecGXRjHODp7C+XbAErA==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'',	'',	'',	'QM5L4eUZSEpp/FYdO/ohKg==',	'',	'vYG8rLmjOCUJvBE9z2RjPQ==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'',	'',	'',	'',	'',	'',	'',	'',	'',	'',	0,	10002,	'-1',	'-1',	0,	'',	'2019-08-07 18:38:57',	1);

DROP TABLE IF EXISTS `lep_mod_app_supplier`;
CREATE TABLE `lep_mod_app_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `company_short` varchar(255) NOT NULL DEFAULT '',
  `company` varchar(255) NOT NULL DEFAULT '',
  `extra_1` varchar(255) NOT NULL DEFAULT '',
  `vat_id` varchar(128) NOT NULL DEFAULT '',
  `eu_trade` varchar(128) NOT NULL DEFAULT '',
  `company_type` varchar(128) NOT NULL DEFAULT '',
  `street` varchar(255) NOT NULL DEFAULT '',
  `no` varchar(32) NOT NULL DEFAULT '',
  `zip` varchar(32) NOT NULL DEFAULT '',
  `city` varchar(128) NOT NULL DEFAULT '',
  `country` varchar(128) NOT NULL DEFAULT 'DEUTSCHLAND',
  `company_salutation` varchar(64) NOT NULL DEFAULT '',
  `company_name1` varchar(255) NOT NULL DEFAULT '',
  `company_name2` varchar(255) NOT NULL DEFAULT '',
  `fon` varchar(128) NOT NULL DEFAULT '',
  `fax` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `internet` varchar(128) NOT NULL DEFAULT '',
  `company_del` varchar(255) NOT NULL,
  `extra_1_del` varchar(255) NOT NULL,
  `street_del` varchar(255) NOT NULL,
  `no_del` varchar(32) NOT NULL,
  `zip_del` varchar(32) NOT NULL,
  `city_del` varchar(255) NOT NULL,
  `country_del` varchar(255) NOT NULL,
  `bank_name` varchar(128) NOT NULL DEFAULT '',
  `iban` varchar(64) NOT NULL DEFAULT '',
  `bic` varchar(32) NOT NULL DEFAULT '',
  `sepa` int(11) NOT NULL DEFAULT '0',
  `account_no` int(11) NOT NULL DEFAULT '-99',
  `payment_terms` varchar(32) NOT NULL DEFAULT '',
  `price_terms` varchar(32) NOT NULL,
  `supplier_no` varchar(32) NOT NULL DEFAULT '',
  `send_invoice` varchar(11) NOT NULL,
  `invoice_type` varchar(32) NOT NULL,
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_app_supplier` (`id`, `client_id`, `section_id`, `page_id`, `company_short`, `company`, `extra_1`, `vat_id`, `eu_trade`, `company_type`, `street`, `no`, `zip`, `city`, `country`, `company_salutation`, `company_name1`, `company_name2`, `fon`, `fax`, `email`, `internet`, `company_del`, `extra_1_del`, `street_del`, `no_del`, `zip_del`, `city_del`, `country_del`, `bank_name`, `iban`, `bic`, `sepa`, `account_no`, `payment_terms`, `price_terms`, `supplier_no`, `send_invoice`, `invoice_type`, `last_modified`, `active`) VALUES
(1,	1,	13,	10,	'tMoVL2pZJkaiLZ3JoB9kYQ==',	'RGq26PzUkD/9CYQOXeMM7l5ZZzMUmjucOBnoxc2eoWM=',	'P9fGiSOiTCvAPLzJ74AAkA==',	'OdNs1K2S3VZRgxR/EFcfRQ==',	'/7cSQzpKH48W5HrT1hY1EQ==',	'wvidyrB4Gqo3B1LaWJ3wkw==',	'akAHFiA3Ac2xwGGK/kb2Xg==',	'3hA7N0A4pPn92nBpltYNqw==',	'kocgkTGiIERXX3xpetvBJQ==',	'x8sKRNe06C1ICB8Bou5mvw==',	'tylaVBWLdDz6zfFYgKKKLg==',	'',	'',	'',	'QM5L4eUZSEpp/FYdO/ohKg==',	'P9fGiSOiTCvAPLzJ74AAkA==',	'lstc0bWx/KzMcOKIdx9dfgmHGpEzDHfFfAk6pI08QlE=',	'UcYVJYftsgKKAXSQgm1V0LY0c0MZz8azI8jQuspALeY=',	'Lieferanten GmbH & Co KG',	'',	'Meine Straße',	'25',	'80000',	'München',	'Deutschland',	'/8L8FG6rkeNbsrrSOUyYxA==',	'W4tfiM3a3ZOwkviojuOcS4RBgXx51pJMUVdRUkPmy8M=',	'rwLvQirTH7M/Cq65mcvyTw==',	0,	30001,	'1',	'1',	'',	'1',	'b2b.lte',	'2021-01-25 14:39:21',	1);

DROP TABLE IF EXISTS `lep_mod_app_suppliertasks`;
CREATE TABLE `lep_mod_app_suppliertasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '-1',
  `section_id` int(11) NOT NULL DEFAULT '-1',
  `page_id` int(11) NOT NULL DEFAULT '-1',
  `supplier_id` int(11) NOT NULL DEFAULT '-1',
  `record_type` int(11) NOT NULL DEFAULT '-1' COMMENT 'invoice or credit note',
  `record_kind` int(11) NOT NULL DEFAULT '-1' COMMENT 'short or with articles',
  `record_date` date NOT NULL DEFAULT '0000-00-00',
  `record_no` varchar(64) NOT NULL DEFAULT '',
  `record_link` varchar(256) NOT NULL DEFAULT '',
  `payable` date NOT NULL DEFAULT '0000-00-00',
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  `payment_terms` int(11) NOT NULL DEFAULT '-1',
  `purpose` varchar(128) NOT NULL DEFAULT '',
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_mod_app_suppliertasks_details`;
CREATE TABLE `lep_mod_app_suppliertasks_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL DEFAULT '-1',
  `article_id` int(11) NOT NULL DEFAULT '-1',
  `count` decimal(6,2) NOT NULL DEFAULT '-1.00',
  `article_price` decimal(15,2) NOT NULL DEFAULT '-1.00',
  `vat_id` int(11) NOT NULL DEFAULT '-1',
  `article_row` decimal(15,2) NOT NULL DEFAULT '-1.00',
  `vat_row` decimal(15,2) NOT NULL DEFAULT '-1.00',
  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_mod_captcha_control`;
CREATE TABLE `lep_mod_captcha_control` (
  `enabled_captcha` varchar(1) NOT NULL DEFAULT '1',
  `enabled_asp` varchar(1) NOT NULL DEFAULT '0',
  `captcha_type` varchar(255) NOT NULL DEFAULT 'calc_text',
  `asp_session_min_age` int(11) NOT NULL DEFAULT '20',
  `asp_view_min_age` int(11) NOT NULL DEFAULT '10',
  `asp_input_min_age` int(11) NOT NULL DEFAULT '5',
  `ct_text` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_captcha_control` (`enabled_captcha`, `enabled_asp`, `captcha_type`, `asp_session_min_age`, `asp_view_min_age`, `asp_input_min_age`, `ct_text`) VALUES
('1',	'1',	'calc_text',	20,	10,	5,	'');

DROP TABLE IF EXISTS `lep_mod_code2`;
CREATE TABLE `lep_mod_code2` (
  `section_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `whatis` int(11) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_code2` (`section_id`, `page_id`, `whatis`, `content`) VALUES
(4,	3,	10,	'');

DROP TABLE IF EXISTS `lep_mod_cookie`;
CREATE TABLE `lep_mod_cookie` (
  `cookie_id` int(11) NOT NULL AUTO_INCREMENT,
  `pop_bg` varchar(16) NOT NULL DEFAULT '#aaa',
  `pop_text` varchar(16) NOT NULL DEFAULT '#fff',
  `but_bg` varchar(16) NOT NULL DEFAULT 'transparent',
  `but_text` varchar(16) NOT NULL DEFAULT '#fff',
  `but_border` varchar(16) NOT NULL DEFAULT '#fff',
  `position` varchar(32) NOT NULL DEFAULT 'bottom-left',
  `layout` varchar(32) NOT NULL DEFAULT 'classic',
  `type` varchar(32) NOT NULL DEFAULT 'show',
  `overwrite` tinyint(1) NOT NULL DEFAULT '0',
  `message` varchar(512) NOT NULL DEFAULT 'here the message text',
  `dismiss` varchar(128) NOT NULL DEFAULT 'Agree',
  `allow` varchar(128) NOT NULL DEFAULT 'Accept',
  `deny` varchar(128) NOT NULL DEFAULT 'Deny',
  `link` varchar(64) NOT NULL DEFAULT 'policy link',
  `href` varchar(256) NOT NULL DEFAULT 'http://cms-lab.com',
  PRIMARY KEY (`cookie_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_cookie` (`cookie_id`, `pop_bg`, `pop_text`, `but_bg`, `but_text`, `but_border`, `position`, `layout`, `type`, `overwrite`, `message`, `dismiss`, `allow`, `deny`, `link`, `href`) VALUES
(1,	'#aaa',	'#fff',	'transparent',	'#fff',	'#fff',	'bottom-left',	'classic',	'show',	0,	'here the message text',	'Agree',	'Accept',	'Deny',	'policy link',	'http://cms-lab.com');

DROP TABLE IF EXISTS `lep_mod_droplets`;
CREATE TABLE `lep_mod_droplets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `code` longtext NOT NULL,
  `description` text NOT NULL,
  `modified_when` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `admin_edit` int(11) NOT NULL DEFAULT '0',
  `admin_view` int(11) NOT NULL DEFAULT '0',
  `show_wysiwyg` int(11) NOT NULL DEFAULT '0',
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_droplets` (`id`, `name`, `code`, `description`, `modified_when`, `modified_by`, `active`, `admin_edit`, `admin_view`, `show_wysiwyg`, `comments`) VALUES
(1,	'check-css',	'// ---- begin droplet\r\n$ch = curl_init();\r\ncurl_setopt($ch, CURLOPT_URL, \"https://doc.lepton-cms.org/_packinstall/check-css.txt\" );\r\ncurl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);\r\n$source = curl_exec($ch);\r\ncurl_close($ch);\r\n       \r\nreturn $source;\r\n// end droplet',	'this droplets displays standard css of your template file',	1498639688,	1,	1,	0,	0,	0,	'Usage:[[check-css]]'),
(2,	'EditThisPage',	'/**\r\n * Version 2 for L* >= 4.5\r\n * This Droplet will show a link to the backend page editor. This is only shown when the user is logged in and has the correct permissions to edit the page in question.\r\n */\r\nglobal $oLEPTON, $page_id, $HEADING, $database, $admin;\r\n$str = \" \";		\r\nif (FRONTEND_LOGIN == \'enabled\' AND is_numeric($oLEPTON->get_session(\'USER_ID\')))\r\n{\r\n	if (true === isset($page_id))\r\n	{ \r\n		$this_page = $page_id;\r\n	} else {\r\n		$this_page = $oLEPTON->default_page_id;\r\n		$page_id = $this_page;\r\n	}\r\n	\r\n	$results_array = [];\r\n	$database->execute_query(\r\n	    \"SELECT * FROM `\".TABLE_PREFIX.\"pages` WHERE `page_id` = \".$this_page,\r\n	    true,\r\n	    $results_array,\r\n	    false\r\n	);\r\n\r\n	// fix aldus 2020-02-17\r\n	if( count($results_array) == 0)\r\n	{\r\n		return \"\";\r\n	}\r\n	\r\n	$old_admin_groups   = explode(\',\', $results_array[\'admin_groups\']);\r\n	$old_admin_users    = explode(\',\', $results_array[\'admin_users\']);\r\n	$this_user          = $oLEPTON->get_session(\'GROUP_ID\');\r\n\r\n	$page = [];\r\n\r\n	$database->execute_query(\r\n	    \"SELECT * FROM \".TABLE_PREFIX.\"pages WHERE page_id = \'\".$page_id.\"\'\",\r\n	    true,\r\n	    $page,\r\n	    false\r\n	);\r\n\r\n	$admin_groups = explode(\',\', str_replace(\'_\', \'\', $page[\'admin_groups\']));\r\n	$admin_users = explode(\',\', str_replace(\'_\', \'\', $page[\'admin_users\']));\r\n	$in_group = FALSE;\r\n\r\n	foreach($admin->get_groups_id() as $cur_gid)\r\n		if (in_array($cur_gid, $admin_groups)) $in_group = TRUE;\r\n	if (($in_group) OR is_numeric(array_search($this_user, $old_admin_groups)) ) {\r\n		$str  = \'<a href=\"\' . ADMIN_URL . \'/pages/modify.php?page_id=\'.$this_page;\r\n		$str .= \'\" target=\"_blank\"><img align=\"left\" border=\"0\" src=\"\';\r\n		$str .= LEPTON_URL . \'/modules/lib_lepton/backend_images/modify_16.png\" alt=\"\' . $HEADING[\'MODIFY_PAGE\'] . \'\" />Edit Page</a>\';\r\n	}      \r\n}\r\n\r\nreturn $str;\r\n// end droplet\r\n',	'Shows an \\\"Edit page\\\" link in the frontend',	1653129609,	1,	1,	0,	0,	0,	'Usage: [[editthispage]]'),
(3,	'EmailFilter',	' \r\n// You can configure the output filtering with the options below.\r\n// Tip: Mailto links can be encrypted by a Javascript function. \r\n// To make use of this option, one needs to add the PHP code \r\n//       register_frontend_modfiles(\'js\');\r\n// into the <head> section of the index.php of your template. \r\n// Without this modification, only the @ character in the mailto part will be replaced.\r\n\r\n// Basic Email Configuration: \r\n// Filter Email addresses in text 0 = no, 1 = yes - default 1\r\n$filter_settings[\'email_filter\'] = \'1\';\r\n\r\n// Filter Email addresses in mailto links 0 = no, 1 = yes - default 1\r\n$filter_settings[\'mailto_filter\'] = \'1\';\r\n\r\n// Email Replacements, replace the \'@\' and the \'.\' by default (at) and (dot)\r\n$filter_settings[\'at_replacement\']  = \'(at)\';\r\n$filter_settings[\'dot_replacement\'] = \'(dot)\';\r\n\r\n// No need to change stuff underneatch unless you know what you are doing.\r\n\r\n// work out the defined output filter mode: possible output filter modes: [0], 1, 2, 3, 6, 7\r\n// 2^0 * (0.. disable, 1.. enable) filtering of mail addresses in text\r\n// 2^1 * (0.. disable, 1.. enable) filtering of mail addresses in mailto links\r\n// 2^2 * (0.. disable, 1.. enable) Javascript mailto encryption (only if mailto filtering enabled)\r\n\r\n// only filter output if we are supposed to\r\nif($filter_settings[\'email_filter\'] != \'1\' && $filter_settings[\'mailto_filter\'] != \'1\'){\r\n	// nothing to do ...\r\n	return true;\r\n}\r\n\r\n// check if non mailto mail addresses needs to be filtered\r\n$output_filter_mode = ($filter_settings[\'email_filter\'] == \'1\') ? 1 : 0;		// 0|1\r\n	\r\n// check if mailto mail addresses needs to be filtered\r\n$wb_page_data = \'\';\r\nif($filter_settings[\'mailto_filter\'] == \'1\')\r\n{\r\n	$output_filter_mode = $output_filter_mode + 2;								// 0|2\r\n					\r\n        // check if Javascript mailto encryption is enabled (call register_frontend_functions in the template)\r\n        $search_pattern = \'/<.*src=\\\".*\\/mdcr.js.*>/iU\';\r\n        if(preg_match($search_pattern, $wb_page_data))\r\n        {\r\n          $output_filter_mode = $output_filter_mode + 4;       // 0|4\r\n        } else {\r\n        	$mdcr_script_url = LEPTON_URL.\"/modules/droplets/js/mdcr.js\";\r\n        	$mdcr_script_tag = \"\\n<script type=\'text/javascript\' src=\'\".$mdcr_script_url.\"\'></script>\\n\";\r\n        	$wb_page_data = str_replace(\"</head>\", $mdcr_script_tag.\"</head>\", $wb_page_data);\r\n        	$output_filter_mode = $output_filter_mode + 4;       // 0|4\r\n        }\r\n}\r\n		\r\n// define some constants so we do not call the database in the callback function again\r\ndefine(\'OUTPUT_FILTER_MODE\', (int) $output_filter_mode);\r\ndefine(\'OUTPUT_FILTER_AT_REPLACEMENT\', $filter_settings[\'at_replacement\']);\r\ndefine(\'OUTPUT_FILTER_DOT_REPLACEMENT\', $filter_settings[\'dot_replacement\']);\r\n	\r\n// function to filter mail addresses embedded in text or mailto links before outputing them on the frontend\r\nif (!function_exists(\'filter_mail_addresses\')) {\r\n	function filter_mail_addresses($match) { \r\n		\r\n	// check if required output filter mode is defined\r\n		if(!(defined(\'OUTPUT_FILTER_MODE\') && defined(\'OUTPUT_FILTER_MODE\') && defined(\'OUTPUT_FILTER_MODE\'))) {\r\n			return $match[0];\r\n		}\r\n		\r\n		$search = array(\'@\', \'.\');\r\n		$replace = array(OUTPUT_FILTER_AT_REPLACEMENT ,OUTPUT_FILTER_DOT_REPLACEMENT);\r\n		\r\n		// check if the match contains the expected number of subpatterns (6|8)\r\n		if(count($match) == 8) {\r\n			/**\r\n				OUTPUT FILTER FOR EMAIL ADDRESSES EMBEDDED IN TEXT\r\n			**/\r\n			\r\n			// 1.. text mails only, 3.. text mails + mailto (no JS), 7 text mails + mailto (JS)\r\n			if(!in_array(OUTPUT_FILTER_MODE, array(1,3,7))) return $match[0];\r\n\r\n			// do not filter mail addresses included in input tags (<input ... value = \"test@mail)\r\n			if (strpos($match[6], \'value\') !== false) return $match[0];\r\n			\r\n			// filtering of non mailto email addresses enabled\r\n			return str_replace($search, $replace, $match[0]);\r\n				\r\n		} elseif(count($match) == 6) {\r\n			/**\r\n				OUTPUT FILTER FOR EMAIL ADDRESSES EMBEDDED IN MAILTO LINKS\r\n			**/\r\n\r\n			// 2.. mailto only (no JS), 3.. text mails + mailto (no JS), 6.. mailto only (JS), 7.. all filters active\r\n			if(!in_array(OUTPUT_FILTER_MODE, array(2,3,6,7))) return $match[0];\r\n			\r\n			// check if last part of the a href link: >xxxx</a> contains a email address we need to filter\r\n			$pattern = \'#[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4}#i\';\r\n			if(preg_match_all($pattern, $match[5], $matches)) {\r\n				foreach($matches as $submatch) {\r\n					foreach($submatch as $value) {\r\n						// replace all . and all @ in email address parts by (dot) and (at) strings\r\n						$match[5] = str_replace($value, str_replace($search, $replace, $value), $match[5]);\r\n					}\r\n				}\r\n			}\r\n\r\n			// check if Javascript encryption routine is enabled\r\n			if(in_array(OUTPUT_FILTER_MODE, array(6,7))) {\r\n				/** USE JAVASCRIPT ENCRYPTION FOR MAILTO LINKS **/\r\n				\r\n				// extract possible class and id attribute from ahref link\r\n				preg_match(\'/class\\s*?=\\s*?(\"|\\\')(.*?)\\1/ix\', $match[0], $class_attr);\r\n				$class_attr = empty($class_attr) ? \'\' : \'class=\"\' . $class_attr[2] . \'\" \';\r\n				preg_match(\'/id\\s*?=\\s*?(\"|\\\')(.*?)\\1/ix\', $match[0], $id_attr);\r\n				$id_attr = empty($id_attr) ? \'\' : \'id=\"\' . $id_attr[2] . \'\" \';\r\n				\r\n				// preprocess mailto link parts for further usage\r\n				$search = array(\'@\', \'.\', \'_\', \'-\'); $replace = array(\'F\', \'Z\', \'X\', \'K\');\r\n				$email_address = str_replace($search, $replace, strtolower($match[2]));\r\n				$email_subject = rawurlencode(html_entity_decode($match[3]));\r\n				\r\n				// create a random encryption key for the Caesar cipher\r\n				mt_srand((double)microtime()*1000000);	// (PHP < 4.2.0)\r\n				$shift = mt_rand(1, 25);\r\n				\r\n				// encrypt the email using an adapted Caesar cipher\r\n		  		$encrypted_email = \"\";\r\n				for($i = strlen($email_address) -1; $i > -1; $i--) {\r\n					if(preg_match(\'#[FZXK0-9]#\', $email_address[$i], $characters)) {\r\n						$encrypted_email .= $email_address[$i];\r\n					} else {	\r\n						$encrypted_email .= chr((ord($email_address[$i]) -97 + $shift) % 26 + 97);\r\n					}\r\n				}\r\n				$encrypted_email .= chr($shift + 97);\r\n\r\n				// build the encrypted Javascript mailto link\r\n				$mailto_link  = \"<a {$class_attr}{$id_attr}href=\\\"javascript:mdcr(\'$encrypted_email\',\'$email_subject\')\\\">\" .$match[5] .\"</a>\";\r\n				\r\n				return $mailto_link;	\r\n\r\n			} else {\r\n				/** DO NOT USE JAVASCRIPT ENCRYPTION FOR MAILTO LINKS **/\r\n\r\n				// as minimum protection, replace replace @ in the mailto part by (at)\r\n				// dots are not transformed as this would transform my.name@domain.com into: my(dot)name(at)domain(dot)com\r\n				\r\n				// rebuild the mailto link from the subpatterns (at the missing characters \" and </a>\")\r\n				return $match[1] .str_replace(\'@\', OUTPUT_FILTER_AT_REPLACEMENT, $match[2]) .$match[3] .\'\"\' .$match[4] .$match[5] .\'</a>\';\r\n				// if you want to protect both, @ and dots, comment out the line above and remove the comment from the line below\r\n				// return $match[1] .str_replace($search, $replace, $match[2]) .$match[3] .\'\"\' .$match[4] .$match[5] .\'</a>\';\r\n			}\r\n		\r\n		}\r\n		\r\n		// number of subpatterns do not match the requirements ... do nothing\r\n		return $match[0];\r\n	}		\r\n}\r\n	\r\n// first search part to find all mailto email addresses\r\n$pattern = \'#(<a[^<]*href\\s*?=\\s*?\"\\s*?mailto\\s*?:\\s*?)([A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4})([^\"]*?)\"([^>]*>)(.*?)</a>\';\r\n// second part to find all non mailto email addresses\r\n$pattern .= \'|(value\\s*=\\s*\"|\\\')??\\b([A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4})\\b#i\';\r\n\r\n// Sub 1:\\b(<a.[^<]*href\\s*?=\\s*?\"\\s*?mailto\\s*?:\\s*?)		-->	\"<a id=\"yyy\" class=\"xxx\" href = \" mailto :\" ignoring white spaces\r\n// Sub 2:([A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4})		-->	the email address in the mailto: part of the mail link\r\n// Sub 3:([^\"]*?)\"							--> possible ?Subject&cc... stuff attached to the mail address\r\n// Sub 4:([^>]*>)							--> all class or id statements after the mailto but before closing ..>\r\n// Sub 5:(.*?)</a>\\b						--> the mailto text; all characters between >xxxxx</a>\r\n// Sub 6:|\\b([A-Z0-9._%+-]+@(?:[A-Z0-9-]+\\.)+[A-Z]{2,4})\\b		--> email addresses which may appear in the text (require word boundaries)\r\n$content = $wb_page_data;			\r\n// find all email addresses embedded in the content and filter them using a callback function\r\n$content = preg_replace_callback($pattern, \'filter_mail_addresses\', $content);\r\n$wb_page_data = $content;\r\nreturn true;\r\n		\r\n',	'Emailfiltering on your output - output filtering with the options below - Mailto links can be encrypted by a Javascript',	1653129609,	1,	1,	0,	0,	0,	'usage:  [[EmailFilter]]'),
(4,	'LoginBox',	'global $oLEPTON, $TEXT, $MENU;\r\nif (!isset ($redirect_login)) { $redirect_login = LEPTON_URL.\"/index.php\"; } \r\nif (!isset ($redirect_logout)) { $redirect_logout = $redirect_login ; }\r\n$return_value = \" \";\r\n// not yet logged in\r\nif(FRONTEND_LOGIN == \'enabled\' && VISIBILITY != \'private\' && $oLEPTON->get_session(\'USER_ID\') == \'\') {\r\n	$return_value  = \'<form name=\"login\" action=\"\'.LOGIN_URL.\'?redirect=\'.$redirect_login.\'\" method=\"post\" class=\"login_table\">\';\r\n	$return_value .= \'<h2>\'.$TEXT[\'LOGIN\'].\'</h2>\';\r\n	$return_value .= $TEXT[\'USERNAME\'].\':<input type=\"text\" name=\"username\" /><br />\';\r\n	$return_value .= $TEXT[\'PASSWORD\'].\':<input type=\"password\" name=\"password\" /><br />\';\r\n	$return_value .= \'<input type=\"submit\" name=\"submit\" value=\"\'.$TEXT[\'LOGIN\'].\'\" class=\"dbutton\" /><br />\';\r\n	$return_value .= \'<a href=\"\'.FORGOT_URL.\'\">\'.$TEXT[\'FORGOT_DETAILS\'].\'</a><br />\';\r\n	if(is_numeric(FRONTEND_SIGNUP))  \r\n		$return_value .= \'<a href=\"\'.SIGNUP_URL.\'\">\'.$TEXT[\'SIGNUP\'].\'</a>\';\r\n	$return_value .= \'</form>\';\r\n// logged in\r\n} elseif(FRONTEND_LOGIN == \'enabled\' && is_numeric($oLEPTON->get_session(\'USER_ID\'))) {\r\n	$return_value = \'<form name=\"logout\" action=\"\'.LOGOUT_URL.\'?redirect=\'.$redirect_logout.\'\" method=\"post\" class=\"login_table\">\';\r\n	$return_value .= \'<h2>\'.$TEXT[\'LOGGED_IN\'].\'</h2>\';\r\n	$return_value .= $TEXT[\'WELCOME_BACK\'].\', \'.$oLEPTON->get_display_name().\'<br />\';\r\n	$return_value .= \'<input type=\"submit\" name=\"submit\" value=\"\'.$MENU[\'LOGOUT\'].\'\" class=\"dbutton\" /><br />\';\r\n	$return_value .= \'</form>\';\r\n}\r\nreturn $return_value;',	'Puts a Login / Logout box on your page.',	1653129609,	1,	1,	0,	0,	0,	'Use: [[LoginBox]]. Remember to enable frontend login in your website settings. For details please see:  https://doc.lepton-cms.org/docu/english/home/tutorials/frontend-login.php'),
(5,	'Lorem',	'$lorem = array();\n$lorem[] = \"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Ut odio. Nam sed est. Nam a risus et est iaculis adipiscing. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer ut justo. In tincidunt viverra nisl. Donec dictum malesuada magna. Curabitur id nibh auctor tellus adipiscing pharetra. Fusce vel justo non orci semper feugiat. Cras eu leo at purus ultrices tristique.<br /><br />\";\n$lorem[] = \"Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.<br /><br />\";\n$lorem[] = \"Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br /><br />\";\n$lorem[] = \"Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.<br /><br />\";\n$lorem[] = \"Cras consequat magna ac tellus. Duis sed metus sit amet nunc faucibus blandit. Fusce tempus cursus urna. Sed bibendum, dolor et volutpat nonummy, wisi justo convallis neque, eu feugiat leo ligula nec quam. Nulla in mi. Integer ac mauris vel ligula laoreet tristique. Nunc eget tortor in diam rhoncus vehicula. Nulla quis mi. Fusce porta fringilla mauris. Vestibulum sed dolor. Aliquam tincidunt interdum arcu. Vestibulum eget lacus. Curabitur pellentesque egestas lectus. Duis dolor. Aliquam erat volutpat. Aliquam erat volutpat. Duis egestas rhoncus dui. Sed iaculis, metus et mollis tincidunt, mauris dolor ornare odio, in cursus justo felis sit amet arcu. Aenean sollicitudin. Duis lectus leo, eleifend mollis, consequat ut, venenatis at, ante.<br /><br />\";\n$lorem[] = \"Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.<br /><br />\"; \nif (!isset($blocks)) $blocks = 1;\n$blocks = (int)$blocks - 1;\nif ($blocks <= 0) $blocks = 0;\nif ($blocks > 5) $blocks = 5;\n$returnvalue = \"\";\nfor ( $i=0 ; $i<=$blocks ; $i++) {\n    $returnvalue .= $lorem[$i];\n}\nreturn $returnvalue;',	'Create Lorum Ipsum text',	1498639688,	1,	1,	0,	0,	0,	'Use: [[Lorem?blocks=6]] (max 6 paragraphs)'),
(6,	'year',	'$datum = date(\"Y\");\r\nreturn $datum;',	'displays the current year',	1498639688,	1,	1,	0,	0,	0,	'Usage:[[year]]'),
(8,	'topics_rss_statistic',	'\r\n/**\r\n * @link https://addons.phpmanufaktur.de/de/name/topics/about.php\r\n * @copyright 2012 phpManufaktur\r\n * @license MIT License (MIT) http://www.opensource.org/licenses/MIT\r\n */\r\n\r\nglobal $database;\r\n\r\n// set the mode for the chart\r\n$chart_mode = (isset($mode) && in_array($mode, array(\'day\', \'week\', \'month\'))) ? $mode : \'day\';\r\n// how many items maximum?\r\n$max_items = (isset($items)) ? (int) $items : 30;\r\n// width of the chart\r\n$chart_width = (isset($width)) ? (int) $width : 450;\r\n// height of the chart\r\n$chart_height = (isset($height)) ? (int) $height : 300;\r\n\r\nif ($chart_mode == \'day\') {\r\n  $end_date = date(\'Y-m-d\');\r\n  $start_date = date(\'Y-m-d\', mktime(0, 0, 0, date(\'m\'), date(\'d\')-$max_items, date(\'Y\')));\r\n  $SQL = \"SELECT `date`,`callers`,`views` FROM `\".TABLE_PREFIX.\"mod_topics_rss_statistic` WHERE `date`>=\'$start_date\' AND `date`<=\'$end_date\'\";\r\n  if (null == ($query = $database->query($SQL)))\r\n    return $database->get_error();\r\n  $title = \"RSS Statistik - letzte $max_items Tage\";\r\n  $data = \"[\'Tag\', \'Abonnenten\', \'Abrufe\']\";\r\n  while (false !== ($day = $query->fetchRow(MYSQL_ASSOC))) {\r\n    $data .= sprintf(\",[\'%s\',%d,%d]\", date(\'d.m\', strtotime($day[\'date\'])), $day[\'callers\'], $day[\'views\']);\r\n  }\r\n}\r\nelseif ($chart_mode == \'week\') {\r\n  $end_week = date(\'W\');\r\n  $start_week = date(\'W\')-$max_items;\r\n  $title = \"RSS Statistik - letzte $max_items Wochen\";\r\n  $data = \"[\'Woche\', \'Abonnenten\', \'Abrufe\']\";\r\n  for ($i=$start_week; $i<$end_week+1; $i++) {\r\n    if ($i < 1) {\r\n      $week = 52+$i;\r\n      $year = date(\'Y\')-1;\r\n    }\r\n    else {\r\n      $week = $i;\r\n      $year = date(\'Y\');\r\n    }\r\n    $monday = date(\'Y-m-d\', strtotime(\"{$year}-W{$week}\"));\r\n    $sunday = date(\'Y-m-d\', strtotime(\"{$year}-W{$week}-7\"));\r\n    $SQL = \"SELECT sum(`callers`) AS \'sum_callers\', sum(`views`) AS \'sum_views\' FROM `\".TABLE_PREFIX.\"mod_topics_rss_statistic` WHERE `date`>=\'$monday\' AND `date`<=\'$sunday\'\";\r\n    if (null == ($query = $database->query($SQL)))\r\n      return $database->get_error();\r\n    $week_sum = $query->fetchRow(MYSQL_ASSOC);\r\n    $data .= sprintf(\",[\'%02d/%d\',%d,%d]\", $week, $year, $week_sum[\'sum_callers\'], $week_sum[\'sum_views\']);\r\n  }\r\n}\r\nelse {\r\n  // chart mode is month\r\n  $end_month = date(\'n\');\r\n  $start_month = date(\'n\')-$max_items;\r\n  $title = \"RSS Statistik - letzte $max_items Monate\";\r\n  $data = \"[\'Monat\', \'Abonnenten\', \'Abrufe\']\";\r\n  for ($i=$start_month; $i < $end_month+1; $i++) {\r\n    if ($i < 1) {\r\n      $month = 12+($i % 12);\r\n      $year = date(\'Y\')+ ((int) ($i/12))-1;\r\n    }\r\n    else {\r\n      $month = $i;\r\n      $year = date(\'Y\');\r\n    }\r\n    $first_day = date(\'Y-m-d\', mktime(0, 0, 0, $month, 1, $year));\r\n    $last_day = date(\'Y-m-d\', mktime(0, 0, 0, $month+1, 0, $year));\r\n    $SQL = \"SELECT sum(`callers`) AS \'sum_callers\', sum(`views`) AS \'sum_views\' FROM `\".TABLE_PREFIX.\"mod_topics_rss_statistic` WHERE `date`>=\'$first_day\' AND `date`<=\'$last_day\'\";\r\n    if (null == ($query = $database->query($SQL)))\r\n      return $database->get_error();\r\n    $month_sum = $query->fetchRow(MYSQL_ASSOC);\r\n    $data .= sprintf(\",[\'%02d.%d\',%d,%d]\", $month, $year, $month_sum[\'sum_callers\'], $month_sum[\'sum_views\']);\r\n  }\r\n}\r\n\r\n// build the chart\r\n$content = <<<EOD\r\n<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script><script type=\"text/javascript\">\r\n      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});\r\n      google.setOnLoadCallback(drawChart);\r\n      function drawChart() {\r\n        var data = google.visualization.arrayToDataTable([\r\n          $data\r\n        ]);\r\n        var options = {\r\n          title: \'$title\'\r\n        };\r\n        var chart = new google.visualization.LineChart(document.getElementById(\'chart_div\'));\r\n        chart.draw(data, options);\r\n      }\r\n    </script>\r\n<div id=\"chart_div\" style=\"width:{$chart_width}px; height:{$chart_height}px;\">&nbsp;</div>\r\nEOD;\r\nreturn $content;',	'Use Google Charts to show the statistic for the TOPICS RSS feeds',	1498721592,	1,	1,	0,	0,	0,	'[[topics_rss_statistic?mode=day&items=30&width=450&height=200]]\r mode => statistics for day, week or month\r items => max. number of day, week or month items\r width => the width of the graph in pixels\r height => the height of the graph in pixels'),
(9,	'site-cookie',	'return cookie::getInstance()->build_js();',	'sets a site cookie',	1653129610,	1,	1,	0,	0,	0,	'[[site-cookie]]');

DROP TABLE IF EXISTS `lep_mod_droplets_permissions`;
CREATE TABLE `lep_mod_droplets_permissions` (
  `id` int(10) unsigned NOT NULL,
  `edit_perm` varchar(50) NOT NULL,
  `view_perm` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_mod_droplets_settings`;
CREATE TABLE `lep_mod_droplets_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute` varchar(50) NOT NULL DEFAULT '0',
  `value` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_droplets_settings` (`id`, `attribute`, `value`) VALUES
(1,	'Manage_backups',	'1'),
(2,	'Import_droplets',	'1'),
(3,	'Delete_droplets',	'1'),
(4,	'Add_droplets',	'1'),
(5,	'Export_droplets',	'1'),
(6,	'Modify_droplets',	'1'),
(7,	'Manage_perms',	'1');

DROP TABLE IF EXISTS `lep_mod_initial_page`;
CREATE TABLE `lep_mod_initial_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '1',
  `init_page` text NOT NULL,
  `page_param` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_initial_page` (`id`, `user_id`, `init_page`, `page_param`) VALUES
(1,	1,	'start/index.php',	'');

DROP TABLE IF EXISTS `lep_mod_jsadmin`;
CREATE TABLE `lep_mod_jsadmin` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '0',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_jsadmin` (`id`, `name`, `value`) VALUES
(1,	'mod_jsadmin_persist_order',	1),
(2,	'mod_jsadmin_ajax_order_pages',	1),
(3,	'mod_jsadmin_ajax_order_sections',	1);

DROP TABLE IF EXISTS `lep_mod_menu_link`;
CREATE TABLE `lep_mod_menu_link` (
  `section_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `target_page_id` int(11) NOT NULL DEFAULT '0',
  `redirect_type` int(11) NOT NULL DEFAULT '302',
  `anchor` varchar(255) NOT NULL DEFAULT '0',
  `extern` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_menu_link` (`section_id`, `page_id`, `target_page_id`, `redirect_type`, `anchor`, `extern`) VALUES
(10,	8,	-1,	301,	'0',	'http://localhost/account/login.php'),
(11,	9,	-1,	301,	'0',	'http://localhost/account/logout.php?redirect=http://localhost'),
(15,	12,	5,	301,	'0',	'');

DROP TABLE IF EXISTS `lep_mod_news_comments`;
CREATE TABLE `lep_mod_news_comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `post_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `commented_when` int(11) NOT NULL DEFAULT '0',
  `commented_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_news_comments` (`comment_id`, `section_id`, `page_id`, `post_id`, `title`, `comment`, `commented_when`, `commented_by`) VALUES
(1,	0,	0,	0,	'',	'',	0,	0);

DROP TABLE IF EXISTS `lep_mod_news_groups`;
CREATE TABLE `lep_mod_news_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_news_groups` (`group_id`, `section_id`, `page_id`, `active`, `position`, `title`) VALUES
(1,	0,	0,	0,	0,	'');

DROP TABLE IF EXISTS `lep_mod_news_posts`;
CREATE TABLE `lep_mod_news_posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `link` text NOT NULL,
  `content_short` text NOT NULL,
  `content_long` text NOT NULL,
  `commenting` varchar(7) NOT NULL DEFAULT '',
  `published_when` int(11) NOT NULL DEFAULT '0',
  `published_until` int(11) NOT NULL DEFAULT '0',
  `posted_when` int(11) NOT NULL DEFAULT '0',
  `posted_by` int(11) NOT NULL DEFAULT '0',
  `history_post_id` int(11) DEFAULT '-1',
  `history_max` int(1) NOT NULL DEFAULT '-1',
  `history_user` int(11) NOT NULL DEFAULT '-1',
  `history_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `history_type` int(1) NOT NULL DEFAULT '-1',
  `history_comment` text NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_news_posts` (`post_id`, `section_id`, `page_id`, `group_id`, `active`, `position`, `title`, `link`, `content_short`, `content_long`, `commenting`, `published_when`, `published_until`, `posted_when`, `posted_by`, `history_post_id`, `history_max`, `history_user`, `history_time`, `history_type`, `history_comment`) VALUES
(1,	0,	0,	0,	0,	0,	'',	'',	'',	'',	'',	0,	0,	0,	0,	-1,	-1,	-1,	'0000-00-00 00:00:00',	-1,	'');

DROP TABLE IF EXISTS `lep_mod_news_settings`;
CREATE TABLE `lep_mod_news_settings` (
  `section_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `posts_per_page` int(11) NOT NULL DEFAULT '5',
  `commenting` varchar(7) NOT NULL DEFAULT '',
  `resize` int(11) NOT NULL DEFAULT '0',
  `use_captcha` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_mod_quickform`;
CREATE TABLE `lep_mod_quickform` (
  `section_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(128) NOT NULL DEFAULT '',
  `subject` varchar(128) NOT NULL DEFAULT '',
  `template` varchar(64) NOT NULL DEFAULT 'form',
  `successpage` int(11) NOT NULL DEFAULT '0',
  `usenbritems` int(11) NOT NULL DEFAULT '50',
  `useview` varchar(12) NOT NULL DEFAULT 'TABLE',
  `use_honeypot` tinyint(1) DEFAULT '0',
  `spam_logging` tinyint(1) DEFAULT '0',
  `spam_checktime` int(11) DEFAULT '0',
  `spam_honeypot` varchar(256) DEFAULT '',
  `spam_failpage` int(11) DEFAULT '0',
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_mod_quickform_data`;
CREATE TABLE `lep_mod_quickform_data` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `msg_group` varchar(32) NOT NULL DEFAULT 'INBOX',
  `data` text NOT NULL,
  `submitted_when` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_mod_wrapper`;
CREATE TABLE `lep_mod_wrapper` (
  `section_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `url` text NOT NULL,
  `height` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_mod_wysiwyg`;
CREATE TABLE `lep_mod_wysiwyg` (
  `section_id` int(11) NOT NULL DEFAULT '0',
  `page_id` int(11) NOT NULL DEFAULT '0',
  `last_version` int(11) NOT NULL DEFAULT '0',
  `hist_autosave` tinyint(4) NOT NULL DEFAULT '0',
  `max_history` int(11) NOT NULL DEFAULT '6',
  `use_workingcopy` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '-1',
  `user_id_working` int(11) NOT NULL DEFAULT '-1',
  `content` longtext NOT NULL,
  `content_comment` text NOT NULL,
  `text` longtext NOT NULL,
  `working_content` longtext NOT NULL,
  `working_content_comment` text NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_wysiwyg` (`section_id`, `page_id`, `last_version`, `hist_autosave`, `max_history`, `use_workingcopy`, `user_id`, `user_id_working`, `content`, `content_comment`, `text`, `working_content`, `working_content_comment`) VALUES
(-1,	-120,	0,	0,	6,	0,	-1,	-1,	'<p><b>Berthold\'s</b> quick brown fox jumps over the lazy dog and feels as if he were in the seventh heaven of typography.</p>',	'',	'Berthold\'s quick brown fox jumps over the lazy dog and feels as if he were in the seventh heaven of typography.',	'',	''),
(9,	7,	0,	0,	6,	0,	-1,	-1,	'<p>Hier ist die Startseite</p>',	'',	'Hier ist die Startseite',	'',	''),
(19,	15,	0,	0,	6,	0,	-1,	-1,	'',	'',	'',	'',	'');

DROP TABLE IF EXISTS `lep_mod_wysiwyg_admin`;
CREATE TABLE `lep_mod_wysiwyg_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skin` varchar(255) NOT NULL DEFAULT 'cirkuit',
  `menu` varchar(255) NOT NULL DEFAULT 'Smart',
  `width` varchar(64) NOT NULL DEFAULT '100%',
  `height` varchar(64) NOT NULL DEFAULT '250px',
  `editor` varchar(255) NOT NULL DEFAULT 'tiny_mce_jq',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_mod_wysiwyg_admin` (`id`, `skin`, `menu`, `width`, `height`, `editor`) VALUES
(1,	'none',	'none',	'100%',	'250px',	'none'),
(2,	'kama',	'Smart',	'100%',	'250px',	'ckeditor'),
(3,	'cirkuit',	'Smart',	'100%',	'250px',	'tiny_mce_jq'),
(4,	'default',	'default',	'100%',	'250px',	'edit_area'),
(5,	'lightgray',	'Full',	'100%',	'250px',	'tiny_mce_4'),
(6,	'lightgray',	'Full',	'100%',	'250px',	'tinymce');

DROP TABLE IF EXISTS `lep_mod_wysiwyg_history`;
CREATE TABLE `lep_mod_wysiwyg_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `autosaved` int(11) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '-1' COMMENT 'User who last saved this content',
  `user_id_hist` int(11) NOT NULL DEFAULT '-1' COMMENT 'User who pushed this content to history',
  `comment` text NOT NULL,
  `content` longtext NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lep_pages`;
CREATE TABLE `lep_pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL DEFAULT '0',
  `root_parent` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0',
  `link` text NOT NULL,
  `target` varchar(7) NOT NULL DEFAULT '',
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `menu_title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `keywords` text NOT NULL,
  `page_trail` text NOT NULL,
  `template` varchar(255) NOT NULL DEFAULT '',
  `visibility` varchar(255) NOT NULL DEFAULT '',
  `position` int(11) NOT NULL DEFAULT '0',
  `menu` int(11) NOT NULL DEFAULT '0',
  `language` varchar(5) NOT NULL DEFAULT '',
  `page_code` varchar(100) NOT NULL DEFAULT '',
  `searching` int(11) NOT NULL DEFAULT '0',
  `admin_groups` text NOT NULL,
  `admin_users` text NOT NULL,
  `viewing_groups` text NOT NULL,
  `viewing_users` text NOT NULL,
  `modified_when` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_pages` (`page_id`, `parent`, `root_parent`, `level`, `link`, `target`, `page_title`, `menu_title`, `description`, `keywords`, `page_trail`, `template`, `visibility`, `position`, `menu`, `language`, `page_code`, `searching`, `admin_groups`, `admin_users`, `viewing_groups`, `viewing_users`, `modified_when`, `modified_by`) VALUES
(9,	0,	0,	0,	'/logout',	'_top',	'Logout',	'Logout',	'',	'',	'9',	'',	'private',	5,	3,	'DE',	'',	1,	'1',	'',	'1',	'',	1565626635,	1),
(8,	0,	0,	0,	'/login',	'_top',	'Login',	'Login',	'',	'',	'8',	'',	'registered',	4,	3,	'DE',	'',	1,	'1',	'',	'1',	'',	1565626534,	1),
(7,	0,	7,	0,	'/start',	'_top',	'Start',	'Start',	'',	'',	'7',	'',	'public',	1,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1565626174,	1),
(6,	12,	1,	2,	'/mandant/firma/kunden',	'_top',	'Kunden',	'Kunden',	'',	'',	'1,12,6',	'',	'private',	4,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1565168722,	1),
(5,	12,	1,	2,	'/mandant/firma/firmendaten',	'_top',	'Mandant | Firma | Firmendaten',	'Firmendaten',	'',	'',	'1,12,5',	'',	'private',	1,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1564751808,	1),
(3,	0,	3,	0,	'/code',	'_top',	'code',	'code',	'',	'',	'3',	'',	'public',	3,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1614082562,	1),
(1,	0,	0,	0,	'/mandantenwahl',	'_self',	'Mandantenwahl',	'Mandant',	'',	'',	'1',	'',	'private',	2,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1565628392,	1),
(10,	12,	1,	2,	'/mandant/firma/lieferanten',	'_top',	'Lieferanten',	'Lieferanten',	'',	'',	'1,12,10',	'',	'private',	5,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1566310535,	1),
(12,	1,	1,	1,	'/mandant/firma',	'_top',	'Firma',	'Firma',	'',	'',	'1,12',	'',	'private',	1,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1611591982,	1),
(13,	12,	1,	2,	'/mandant/firma/konten',	'_top',	'Konten',	'Konten',	'',	'',	'1,12,13',	'',	'private',	2,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1611592084,	1),
(14,	12,	1,	2,	'/mandant/firma/artikel',	'_top',	'Artikel',	'Artikel',	'',	'',	'1,12,14',	'',	'public',	3,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1612269593,	1),
(15,	1,	1,	1,	'/mandant/kunden',	'_top',	'Kunden',	'Kunden',	'',	'',	'1,15',	'',	'public',	2,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1613642457,	1),
(16,	1,	1,	1,	'/mandant/lieferanten',	'_top',	'Lieferanten',	'Lieferanten',	'',	'',	'1,16',	'',	'public',	3,	1,	'DE',	'',	1,	'1',	'',	'1',	'',	1613642482,	1);

DROP TABLE IF EXISTS `lep_search`;
CREATE TABLE `lep_search` (
  `search_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  `extra` text NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_search` (`search_id`, `name`, `value`, `extra`) VALUES
(34,	'query_start',	'SELECT [TP]pages.page_id, [TP]pages.page_title,	[TP]pages.link, [TP]pages.description, [TP]pages.modified_when, [TP]pages.modified_by	FROM [TP]mod_news_posts, [TP]mod_news_groups, [TP]mod_news_comments, [TP]mod_news_settings, [TP]pages WHERE ',	'news'),
(35,	'query_body',	'\n    	[TP]pages.page_id = [TP]mod_news_posts.page_id AND [TP]mod_news_posts.title LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_posts.page_id AND [TP]mod_news_posts.content_short LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_posts.page_id AND [TP]mod_news_posts.content_long LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_comments.page_id AND [TP]mod_news_comments.title LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_comments.page_id AND [TP]mod_news_comments.comment LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.header LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.footer LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.post_header LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.post_footer LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.comments_header LIKE \'%[STRING]%\'\n    	OR [TP]pages.page_id = [TP]mod_news_settings.page_id AND [TP]mod_news_settings.comments_footer LIKE \'%[STRING]%\'',	'news'),
(27,	'cfg_search_images',	'true',	''),
(28,	'cfg_thumbs_width',	'100',	''),
(29,	'cfg_content_image',	'first',	''),
(30,	'cfg_search_library',	'lib_search',	''),
(31,	'cfg_search_droplet',	'LEPTON_SearchResults',	''),
(32,	'cfg_search_use_page_id',	'-1',	''),
(33,	'module',	'news',	'a:6:{s:7:\"page_id\";s:7:\"page_id\";s:5:\"title\";s:10:\"page_title\";s:4:\"link\";s:4:\"link\";s:11:\"description\";s:11:\"description\";s:13:\"modified_when\";s:13:\"modified_when\";s:11:\"modified_by\";s:11:\"modified_by\";}'),
(26,	'template',	'app',	''),
(25,	'cfg_show_description',	'true',	''),
(24,	'cfg_link_non_public_content',	'',	''),
(23,	'cfg_search_non_public_content',	'false',	''),
(22,	'cfg_search_description',	'true',	''),
(21,	'cfg_search_keywords',	'true',	''),
(20,	'time_limit',	'0',	''),
(19,	'max_excerpt',	'15',	''),
(18,	'module_order',	'wysiwyg',	''),
(36,	'query_end',	'',	'news');

DROP TABLE IF EXISTS `lep_sections`;
CREATE TABLE `lep_sections` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  `module` varchar(255) NOT NULL DEFAULT '',
  `block` varchar(255) NOT NULL DEFAULT '',
  `publ_start` varchar(255) NOT NULL DEFAULT '0',
  `publ_end` varchar(255) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT 'no name',
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_sections` (`section_id`, `page_id`, `position`, `module`, `block`, `publ_start`, `publ_end`, `name`) VALUES
(-1,	-120,	1,	'wysiwyg',	'',	'0',	'0',	'no name'),
(4,	3,	1,	'code2',	'1',	'0',	'0',	'no name'),
(13,	10,	1,	'app_supplier',	'1',	'0',	'0',	'no name'),
(7,	6,	1,	'app_customer',	'1',	'0',	'0',	'no name'),
(6,	5,	1,	'app_basic',	'1',	'0',	'0',	'no name'),
(9,	7,	1,	'wysiwyg',	'1',	'0',	'0',	'no name'),
(10,	8,	1,	'menu_link',	'1',	'0',	'0',	'no name'),
(11,	9,	1,	'menu_link',	'1',	'0',	'0',	'no name'),
(12,	1,	1,	'app_client',	'1',	'0',	'0',	'no name'),
(15,	12,	1,	'menu_link',	'1',	'0',	'0',	''),
(18,	14,	1,	'app_articles',	'1',	'0',	'0',	''),
(17,	13,	1,	'app_accounts',	'1',	'0',	'0',	'no name'),
(19,	15,	1,	'wysiwyg',	'1',	'0',	'0',	''),
(21,	16,	1,	'app_suppliertasks',	'1',	'0',	'0',	'no name');

DROP TABLE IF EXISTS `lep_settings`;
CREATE TABLE `lep_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_settings` (`setting_id`, `name`, `value`) VALUES
(1,	'lepton_version',	'5.4.0'),
(2,	'website_title',	'CMS-LAB  App Development'),
(3,	'website_description',	''),
(4,	'website_keywords',	''),
(5,	'website_header',	'CMS-LAB App Development'),
(6,	'website_footer',	'settings/website footer'),
(7,	'backend_title',	'CMS-LAB App Development'),
(8,	'upload_whitelist',	'jpg,jpeg,gif,gz,png,pdf,tif,zip'),
(9,	'er_level',	'-1'),
(10,	'prompt_mysql_errors',	'false'),
(11,	'default_language',	'DE'),
(12,	'app_name',	'lep6438'),
(13,	'sec_anchor',	'lep_'),
(14,	'default_timezone_string',	'Europe/Berlin'),
(15,	'default_date_format',	'd.m.Y'),
(16,	'default_time_format',	'H:i'),
(17,	'redirect_timer',	'1500'),
(18,	'leptoken_lifetime',	'1800'),
(19,	'max_attempts',	'6'),
(20,	'home_folders',	'false'),
(21,	'default_template',	'app'),
(22,	'default_theme',	'lepsem'),
(23,	'default_charset',	'utf-8'),
(24,	'link_charset',	'utf-8'),
(25,	'multiple_menus',	'true'),
(26,	'page_level_limit',	'4'),
(27,	'page_trash',	'inline'),
(28,	'homepage_redirection',	'false'),
(29,	'page_languages',	'false'),
(30,	'wysiwyg_editor',	'tinymce'),
(31,	'manage_sections',	'true'),
(32,	'section_blocks',	'true'),
(33,	'frontend_login',	'true'),
(34,	'frontend_signup',	'0'),
(35,	'search',	'public'),
(36,	'page_extension',	'.php'),
(37,	'page_spacer',	'-'),
(38,	'pages_directory',	'/app'),
(39,	'media_directory',	'/media'),
(40,	'operating_system',	'windows'),
(41,	'string_file_mode',	'0644'),
(42,	'string_dir_mode',	'0755'),
(43,	'mailer_routine',	'phpmail'),
(44,	'server_email',	'no@none.tld'),
(45,	'mailer_default_sendername',	'LEPTON Mailer'),
(46,	'mailer_smtp_host',	''),
(47,	'mailer_smtp_auth',	''),
(48,	'mailer_smtp_username',	''),
(49,	'mailer_smtp_password',	''),
(50,	'mediasettings',	''),
(52,	'tfa',	'false'),
(53,	'wysiwyg_history',	'false');

DROP TABLE IF EXISTS `lep_temp`;
CREATE TABLE `lep_temp` (
  `temp_id` int(2) NOT NULL AUTO_INCREMENT,
  `temp_browser` varchar(64) NOT NULL DEFAULT '',
  `temp_ip` varchar(64) NOT NULL DEFAULT '',
  `temp_time` int(24) NOT NULL DEFAULT '0',
  `temp_count` int(2) NOT NULL DEFAULT '0',
  `temp_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`temp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_temp` (`temp_id`, `temp_browser`, `temp_ip`, `temp_time`, `temp_count`, `temp_active`) VALUES
(49,	'85277078fbb122611191967a0b249f24614dccbe',	'4b84b15bff6ee5796152495a230e45e3d7e947d9',	1653129390,	0,	1),
(50,	'2a7edbc580926d61eadf7bc0e0e167c4',	'd080501be51b70cc452326d2aeda7ae5',	1653129642,	0,	1);

DROP TABLE IF EXISTS `lep_users`;
CREATE TABLE `lep_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `groups_id` varchar(255) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `statusflags` int(11) NOT NULL DEFAULT '6',
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `last_reset` int(11) NOT NULL DEFAULT '0',
  `pin` varchar(255) NOT NULL DEFAULT '-1',
  `pin_set` int(1) NOT NULL DEFAULT '0',
  `display_name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL,
  `timezone_string` varchar(50) NOT NULL DEFAULT 'Europe/Berlin',
  `date_format` varchar(255) NOT NULL DEFAULT '',
  `time_format` varchar(255) NOT NULL DEFAULT '',
  `language` varchar(5) NOT NULL DEFAULT 'EN',
  `home_folder` text NOT NULL,
  `login_when` int(11) NOT NULL DEFAULT '0',
  `login_ip` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `lep_users` (`user_id`, `group_id`, `groups_id`, `active`, `statusflags`, `username`, `password`, `last_reset`, `pin`, `pin_set`, `display_name`, `email`, `timezone_string`, `date_format`, `time_format`, `language`, `home_folder`, `login_when`, `login_ip`) VALUES
(1,	1,	'1',	1,	6,	'admin',	'$2y$10$skLQN/XQEYTAjR82Cr6Are3npLc85SmVndb8nEL.Ymn/AvKz1T5PW',	0,	'-1',	0,	'Administrator',	'no@none.tld',	'Europe/Berlin',	'd.m.Y',	'H:i',	'DE',	'',	1653129642,	'127.0.0.1');

-- 2022-05-21 10:42:54
