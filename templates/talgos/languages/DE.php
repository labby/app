<?php

/**
  *  @template       Talgos  Backend-Theme
 *  @version        see info.php of this template
 *  @author         LEPTON project and others for Algos theme, LEPTON project for Talgos theme
 *	@copyright      2010-2020 LEPTON Project
 *  @license        GNU General Public License
 *  @license terms  see info.php of this template
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


 
//Modul Description
$template_description 	= 'Ein erweitertes Backend-Theme für LEPTON CMS';

$THEME = array(
	'ADMIN_ONLY' 		=> 'Einstellungen nur für Administratoren',
	'BACKEND_PERMISSION' => 'Backend-Zugang',
	'COPY' 				=> 'Kopieren',
	'NO_SHOW_THUMBS' 	=> 'Thumbnails verbergen',
	'TEXT_HEADER' 		=> 'Maximale Größe für den Upload von Bildern einstellen</b><br><small><i>(gilt nur für neue Uploads)</i></small>',
	'CHANGE_LANGUAGE_NOTICE' => 'Bitte gehen Sie auf "Einstellungen", um die verwendete Seitensprache zu ändern',
	'CANNOT_DELETE'		=> 'Löschen nicht möglich - User hat statusflags 32.',	
	'NOTICE'			=> 'Achtung:',
	'PHP_INFO'			=> 'Die installierte PHP Version ist: ',
	'PHP_VERSION'		=> 'PHP-Version',
	'UPDATE' 			=> 'Eine neuere LEPTON Version ist verfügbar! Aktuelle Version: ',
	'WYSIWYG_HISTORY' 	=> 'WYSIWYG Historie',
	'SECTION_ID' 		=> 'ID',
	'SECTION_NAME' 		=> 'Name'
);
?>