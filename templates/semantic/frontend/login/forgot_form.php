<?php

/**
 *  @template       Semantic
 *  @version        see info.php of this template
 *  @author         cms-lab
 *  @copyright      2014-2021 CMS-LAB
 *  @license        http://creativecommons.org/licenses/by/3.0/
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

global $info_text, $MESSAGE, $info_color;

if($info_color == '000000') {
	$info_color = 'info';
} else {
	$info_color = 'red';
}

// Include template engine 
$oTWIG = lib_twig_box::getInstance();

// register path to make sure twig is looking in this module template folder first
$oTWIG->registerPath( __DIR__."/templates/" );

// Frontend-template?
$temp_look_for_path = LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/login/templates/";
if(file_exists(	$temp_look_for_path."forgot_form.lte")) {
	$oTWIG->registerPath( $temp_look_for_path );
}

$hash = sha1( microtime().$_SERVER['HTTP_USER_AGENT'] );
$_SESSION['wb_apf_hash'] = $hash;

$redirect_url = (isset($_SESSION['HTTP_REFERER']) ? $_SESSION['HTTP_REFERER'] : "");

unset($_SESSION['result_message']);

$data = array( 
	'info_color'		=>	$info_color,
	'info_text'		    =>	$info_text,
	'FORGOT_URL'		=>	FORGOT_URL,
	'HASH'				=>	$hash,	
	'URL'			    =>	$redirect_url
);

echo $oTWIG->render("forgot_form.lte", $data);	
