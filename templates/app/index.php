<?php

/**
 *  @template       App
 *  @version        see info.php of this template
 *  @author         cms-lab
 *	@copyright      2018-2019 cms-lab
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 */

// include class.secure.php to protect this file and the whole CMS!
if ( defined( 'LEPTON_PATH' ) )
{
    include( LEPTON_PATH . '/framework/class.secure.php' );
} 
else
{
    $oneback = "../";
    $root    = $oneback;
    $level   = 1;
    while ( ( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) ) )
    {
        $root .= $oneback;
        $level += 1;
    } 
    if ( file_exists( $root . '/framework/class.secure.php' ) )
    {
        include( $root . '/framework/class.secure.php' );
    } 
    else
    {
        trigger_error( sprintf( "[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER[ 'SCRIPT_NAME' ] ), E_USER_ERROR );
    }
}
// end include class.secure.php



// TEMPLATE CODE STARTS BELOW
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">	
		<meta name="description" content="<?php page_description(); ?>" />
		<meta name="keywords" content="<?php page_keywords(); ?>" />
		<?php get_page_headers();	?>
		<link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_DIR; ?>/css/template.css" media="screen,projection" />
		<link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_DIR; ?>/css/print.css" media="print" />
		<title><?php page_title('', '[WEBSITE_TITLE]'); ?></title>
	</head>
	<body>
		<div id="header">	
			<div id="app_header">
				<h1>Überschrift: OS-App</h1>
				<h2>Mandant: [Variable einsetzen]</h2>
			</div>
			<div id="navi_top">
				<?php show_menu2(3, SM2_ROOT, SM2_START, SM2_TRIM|SM2_PRETTY); ?>
			</div>
		</div>
		<div id="content">	
			<div id="navi_left">
				<?php show_menu2(1, SM2_ROOT, SM2_ALL, SM2_TRIM|SM2_PRETTY); ?>
			</div>
			<div id="main">
					<?php page_content(); ?>
			</div>
		</div>
		<div id="footer">
			<div id="footercontent">
				<div class="footerbox">
					Subnavigation 1
				</div>
				<div class="footerbox">
					OS-App ist ein privates, unabhängiges Projekt<br />
					Copyright © 2020 - [[year]]
				</div>
				<div class="footerbox">
					Links
				</div>
				<div class="footerbox">
					Subnavigation 2
				</div>
			</div>
		</div>
		<?php get_page_footers();	?>		
	</body>
</html>