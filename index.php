<?php

/**
 * This file is part of LEPTON Core, released under the GNU GPL
 * Please see LICENSE and COPYING files in your package for details, specially for terms and warranties.
 * 
 * NOTICE:LEPTON CMS Package has several different licenses.
 * Please see the individual license in the header of each single file or info.php of modules and templates.
 *
 * @author          Website Baker Project, LEPTON Project
 * @copyright       2004-2010 Website Baker Org. e.V.
 * @copyright       2010-2022 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see LICENSE and COPYING files in your package
 *
 */ 

$starttime = array_sum(explode(" ",microtime()));

define('DEBUG', true);
define("FRONTEND", true);

// Include config file
$config_file = dirname(__FILE__).'/config/config.php';
if(file_exists($config_file))
{
    require_once($config_file);

} else {
    
    /**
     *    File isn't there, so we try to run the installer
     *
     *    Anmerkung:  HTTP/1.1 verlangt einen absoluten URI inklusive dem Schema,
     *    Hostnamen und absoluten Pfad als Argument von Location:, manche, aber nicht alle
     *    Clients akzeptieren jedoch auch relative URIs.
     */
    $host       = $_SERVER['HTTP_HOST'];
    $uri        = rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\');
    $file       = 'install/index.php';
    $target_url = 'http://'.$host.$uri.'/'.$file;
    header('Location: '.$target_url);
    die();    // make sure that the code below will not be executed
}

// Create new frontend object
$oLEPTON = new LEPTON_frontend();

// Figure out which page to display
$oLEPTON->page_select() or die();

// Collect info about the currently viewed page
// and check permissions
$oLEPTON->get_page_details();

// Collect general website settings
$oLEPTON->get_website_settings();

// Load functions available to templates, modules and code sections
// also, set some aliases for backward compatibility
require(LEPTON_PATH.'/framework/summary.frontend_functions.php');

// redirect menu-link
$this_page_id = PAGE_ID;
$query_this_module = [];
$database->execute_query(
    "SELECT `module`, `block` FROM `".TABLE_PREFIX."sections` WHERE `page_id` = ".(int)$this_page_id." AND `module` = 'menu_link' ",
    true,
    $query_this_module,
    false
);
if( count($query_this_module) > 0)  // This is a menu_link. Get link of target-page and redirect
{
    // get target_page_id
    $query_tpid = [];
    $database->execute_query(
        "SELECT * FROM `".TABLE_PREFIX."mod_menu_link` WHERE `page_id` = ".(int)$this_page_id." ",
        true,
        $query_tpid,
        false
    );

    if( count($query_tpid) > 0 )
    {
        $target_page_id = $query_tpid['target_page_id'];
        $redirect_type = $query_tpid['redirect_type'];
        $anchor = ($query_tpid['anchor'] != '0' ? '#'.(string)$query_tpid['anchor'] : '');
        $extern = $query_tpid['extern'];
        // set redirect-type
        if($redirect_type == 301)
        {
            header('HTTP/1.1 301 Moved Permanently', TRUE, 301);
        }

        if($target_page_id == -1)
        {
            if($extern != '')
            {
                $target_url = $extern.$anchor;
                header('Location: '.$target_url);
                exit;
            }
        }
        else
        {
            // get link of target-page
            $target_page_link = $database->get_one("SELECT `link` FROM `".TABLE_PREFIX."pages` WHERE `page_id` = ".$target_page_id." ");
            if($target_page_link != null)
            {
                $target_url = LEPTON_URL.PAGES_DIRECTORY.$target_page_link.PAGE_EXTENSION.$anchor;
                header('Location: '.$target_url);
                exit;
            }
        }
    }
}

// Get pagecontent in buffer for Droplets
ob_start();
    require LEPTON_PATH.'/templates/'.TEMPLATE.'/index.php';
    $output = ob_get_clean();

// Replace all [wblinkxxx] with real, internal links
$oLEPTON->preprocess($output);

// Load Droplet engine and process
if(file_exists(LEPTON_PATH .'/modules/droplets/droplets.php'))
{
    include_once LEPTON_PATH .'/modules/droplets/droplets.php'; 
    if(function_exists('evalDroplets'))
    {
        evalDroplets($output);
    }
}

// CSRF protection - add tokens to internal links
if ($oLEPTON->is_authenticated())
{
    if (file_exists(LEPTON_PATH .'/framework/functions/function.addTokens.php'))
    {
        include_once LEPTON_PATH .'/framework/functions/function.addTokens.php';
        if (function_exists('addTokens'))
        {
            addTokens($output, $oLEPTON);
        }
    }
}

echo $output;
