<?php

/**
 *	@module			wysiwyg Admin
 *	@version		see info.php of this module
 *	@authors		Dietrich Roland Pehlke
 * @copyright       2010-2020 Dietrich Roland Pehlke
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 */

class wysiwyg_admin extends LEPTON_abstract
{
    // Own instance for this class!
    static $instance;
    
 
    public function initialize() 
    {

    }
}