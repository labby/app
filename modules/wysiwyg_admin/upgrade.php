<?php

/**
 *	@module			wysiwyg Admin
 *	@version		see info.php of this module
 *	@authors		Dietrich Roland Pehlke
 * @copyright       2010-2020 Dietrich Roland Pehlke
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


// delete obsolete file
LEPTON_handle::delete_obsolete_files('/modules/wysiwyg_admin/register_parser.php');

// -------------------------------------------
// delete obsolete files in current version
$file_names = array(
	'/modules/wysiwyg_admin/templates/modify.lte',			// replaced by tool.lte
	'/templates/lepsem/backend/wysiwyg_admin/backend.css',	// moved to module/css folder
	'/templates/lepsem/backend/wysiwyg_admin/modify.lte',	// moved to module/template folder
	'/modules/wysiwyg_admin/register_parser.php',			// no longer needed
);

LEPTON_handle::delete_obsolete_files($file_names);

$dir_names = array(
	 '/templates/lepsem/backend/wysiwyg_admin'				// no longer required
);
LEPTON_handle::delete_obsolete_directories($dir_names);

?>
