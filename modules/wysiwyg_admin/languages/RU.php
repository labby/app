<?php

/**
 *	@module			wysiwyg Admin
 *	@version		please see info.php of this module
 *	@author			Dietrich Roland Pehlke
 *	@copyright		2010-2020 Dietrich Roland Pehlke
 *	@link			https://lepton-cms.org
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:25, 25-06-2020
 * translated from..: EN
 * translated to....: RU
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_WYSIWYG_ADMIN	= [
    "HEIGHT"            => "высота редактора",
    "LEGEND1"           => "в '%'. Не оставляйте эти поля пустыми!",
    "LEGEND2"           => "в 'px'. Не оставляйте эти поля пустыми!",
    "PREVIEW"           => "Предварительный просмотр",
    "SKINS"             => "Выберите кожу",
    "TOOL"              => "Панель инструментов редактора",
    "WIDTH"             => "ширина редакции",
    "SAVE_OK"           => "Настройки сохранены.",
    "SAVE_FAILED"       => "Ошибка во время save.<br />Попробуйте еще раз.<br /><br />Если ошибка повторится, пожалуйста, сообщите об ошибке <a href='https://forum.lepton-cms.org' target='_blank'>[здесь]</a>.",
    "SAVE_FAIL_DRIVER"  => "ВНИМАНИЕ: использование устаревших драйверов!<br />Пожалуйста, добавьте class.editorinfo.php в текущий wysiwyg-редактор!"
];
