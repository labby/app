<?php

/**
 *	@module			wysiwyg Admin
 *	@version		please see info.php of this module
 *	@author			Dietrich Roland Pehlke
 *	@copyright		2010-2020 Dietrich Roland Pehlke
 *	@link			https://lepton-cms.org
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$MOD_WYSIWYG_ADMIN = Array(
	'HEIGHT'	=> 'Editor height',
	'LEGEND1'	=> "in '%'. Don't leave these fields blank!",
	'LEGEND2'	=> "in 'px'. Don't leave these fields blank!",
	'PREVIEW'	=> 'Preview',
	'SKINS'		=> 'Select skin',
	'TOOL'		=> 'Editor toolbar',
	'WIDTH'		=> 'Editor width',
	"SAVE_OK"		=> "Settings are saved.",
	"SAVE_FAILED"	=> "An error occured during save.<br />"
						."Please try again.<br />"
						."<br />"
						."If error occurs again, please report the issue <a href='https://forum.lepton-cms.org' target='_blank'>[here]</a>.",
	"SAVE_FAIL_DRIVER"	=> "WARNING: use of obsolete drivers!<br />"
						."Please add a class.editorinfo.php to the current wysiwyg-editor!"
);

?>
