<?php

/**
 *	@module			wysiwyg Admin
 *	@version		please see info.php of this module
 *	@author			Dietrich Roland Pehlke
 *	@copyright		2010-2020 Dietrich Roland Pehlke
 *	@link			https://lepton-cms.org
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:26, 25-06-2020
 * translated from..: EN
 * translated to....: PL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_WYSIWYG_ADMIN	= array(
	"HEIGHT"				=> "Wysokość redaktora",
	"LEGEND1"				=> "w '%'. Nie zostawiaj tych pól pustych!",
	"LEGEND2"				=> "w 'px'. Nie zostawiaj tych pól pustych!",
	"PREVIEW"				=> "w 'Podgląd'.",
	"SKINS"					=> "Wybierz skórę",
	"TOOL"					=> "Pasek narzędzi edytora",
	"WIDTH"					=> "Szerokość edytora",
	"SAVE_OK"				=> "Ustawienia są zapisywane.",
	"SAVE_FAILED"			=> "Wystąpił błąd podczas save.<br />Proszę spróbować ponownie.<br /><br />Jeśli błąd wystąpi ponownie, proszę zgłosić problem <a href='https://forum.lepton-cms.org' target='_blank'>[tutaj]</a>.",
	"SAVE_FAIL_DRIVER"		=> "OSTRZEŻENIE: użycie przestarzałych sterowników!<br />Proszę dodać class.editorinfo.php do obecnego wysiwyg-editor!"
);

?>
