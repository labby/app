<?php

/**
 *	@module			wysiwyg Admin
 *	@version		please see info.php of this module
 *	@author			Dietrich Roland Pehlke
 *	@copyright		2010-2020 Dietrich Roland Pehlke
 *	@link			https://lepton-cms.org
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:28, 25-06-2020
 * translated from..: EN
 * translated to....: FR
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_WYSIWYG_ADMIN	= array(
	"HEIGHT"				=> "Hauteur de l'éditeur",
	"LEGEND1"				=> "en '%'. Ne laissez pas ces champs vides !",
	"LEGEND2"				=> "dans 'px'. Ne laissez pas ces champs vides !",
	"PREVIEW"				=> "Prévisualisation",
	"SKINS"					=> "Sélectionner la peau",
	"TOOL"					=> "Barre d'outils de l'éditeur",
	"WIDTH"					=> "Largeur de l'éditeur",
	"SAVE_OK"				=> "Les réglages sont enregistrés.",
	"SAVE_FAILED"			=> "Une erreur s'est produite lors de la sauvegarde.<br />Veuillez réessayer.<br /><br />Si l'erreur se reproduit, veuillez signaler le problème <a href='https://forum.lepton-cms.org' target='_blank'>[ici]</a>.",
	"SAVE_FAIL_DRIVER"		=> "ATTENTION : utilisation de pilotes obsolètes ! <br />Veuillez ajouter un fichier class.editorinfo.php au wysiwyg-editor actuel !"
);

?>
