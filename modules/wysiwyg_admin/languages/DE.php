<?php

/**
 *	@module			wysiwyg Admin
 *	@version		please see info.php of this module
 *	@author			Dietrich Roland Pehlke
 *	@copyright		2010-2020 Dietrich Roland Pehlke
 *	@link			https://lepton-cms.org
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$MOD_WYSIWYG_ADMIN = Array(
	'HEIGHT'	=> 'Editor H&ouml;he',
	'LEGEND1'	=> "in '%'. Dieses Feld darf nicht leer sein!",
	'LEGEND2'	=> "in 'px'. Dieses Feld darf nicht leer sein!",
	'PREVIEW'	=> 'Vorschau',
	'SKINS'		=> 'W&auml;hle skin',
	'TOOL'		=> 'Editor Toolbar',
	'WIDTH'		=> 'Editor Breite',
	"SAVE_OK"		=> "Einstellungen wurden gespeichert.",
	"SAVE_FAILED"	=> "Beim speichern der Einstellungen gab es ein Problem.<br />"
						."Bitte nochmals versuchen.<br />"
						."<br />"
						."Im Wiederholungsfall den Fehler bitte <a href='https://forum.lepton-cms.org' target='_blank'>[hier]</a> melden.",
	"SAVE_FAIL_DRIVER"	=> "ACHTUNG: Es wird ein veralteter Treiber verwendet!<br />"
						."Bitte aktualisieren Sie Ihre LEPTON CMS Version."
);

?>
