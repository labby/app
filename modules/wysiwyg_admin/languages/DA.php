<?php

/**
 *	@module			wysiwyg Admin
 *	@version		please see info.php of this module
 *	@author			Dietrich Roland Pehlke
 *	@copyright		2010-2020 Dietrich Roland Pehlke
 *	@link			https://lepton-cms.org
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:29, 25-06-2020
 * translated from..: EN
 * translated to....: DA
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_WYSIWYG_ADMIN	= array(
	"HEIGHT"				=> "Editorhøjde",
	"LEGEND1"				=> "i '%'. Lad ikke disse felter være tomme!",
	"LEGEND2"				=> "i 'px'. Lad ikke disse felter være tomme!",
	"PREVIEW"				=> "Eksempel",
	"SKINS"					=> "Vælg hud",
	"TOOL"					=> "Editor værktøjslinje",
	"WIDTH"					=> "Editorbredde",
	"SAVE_OK"				=> "Indstillinger gemmes.",
	"SAVE_FAILED"			=> "Der opstod en fejl under gemningen.<br />Prøv igen.<br /><br />Hvis der opstår en fejl igen, skal du rapportere problemet <a href='https://forum.lepton-cms.org' target = '_blank'>[her]</a>.",
	"SAVE_FAIL_DRIVER"		=> "ADVARSEL: Brug af forældede drivere!<br />Tilføj en class.editorinfo.php til den nuværende wysiwyg-editor!"
);

?>
