<?php

/**
 *	@module			wysiwyg Admin
 *	@version		please see info.php of this module
 *	@author			Dietrich Roland Pehlke
 *	@copyright		2010-2020 Dietrich Roland Pehlke
 *	@link			https://lepton-cms.org
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		please see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:27, 25-06-2020
 * translated from..: EN
 * translated to....: NL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_WYSIWYG_ADMIN	= array(
	"HEIGHT"				=> "Hoogte van de redacteur",
	"LEGEND1"				=> "in '%'. Laat deze velden niet leeg!",
	"LEGEND2"				=> "in 'px'. Laat deze velden niet leeg!",
	"PREVIEW"				=> "Voorbeeld",
	"SKINS"					=> "Selecteer huid",
	"TOOL"					=> "Redacteurswerkbalk",
	"WIDTH"					=> "Redacteursbreedte",
	"SAVE_OK"				=> "De instellingen worden opgeslagen.",
	"SAVE_FAILED"			=> "Er is een fout opgetreden tijdens het opslaan.<br />Please probeer het opnieuw.<br /><br />Als er weer een fout optreedt, meld dan het probleem <a href='https://forum.lepton-cms.org' target='_blank'>[hier]</a>.",
	"SAVE_FAIL_DRIVER"		=> "WAARSCHUWING: gebruik van verouderde drivers! <br />BR#Voeg alsjeblieft een class.editorinfo.php toe aan de huidige wysiwyg-editor!"
);

?>
