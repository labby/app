<?php

/**
 * @module          Reset PIN
 * @author          cms-lab
 * @copyright       2019-2021 cms-lab
 * @link            http://www.cms-lab.com
 * @license         custom license: http://cms-lab.com/_documentation/reset-pin/license.php
 * @license_terms   please see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

// get instance of functions file
$oRESET_PIN = reset_pin::getInstance();

$statusMessage = "";

if (( true === isset( $_POST['username'] ))
 || ( true === isset( $_POST['display_name'] )))
{
	if (( true === isset($_SESSION['reset_pin_hash']) 
	 && ( $_SESSION['reset_pin_hash'] == $_POST['hash']) ))
	{
		unset($_SESSION['reset_pin_hash']);
		unset($_POST['hash']);

		$statusMessage = $oRESET_PIN->reset_value();
	} else {
		$statusMessage = array( "STATUS" => false, "MESSAGE_ID" => "SAVE_FAILED" );
	}
}

//  [0] build hash
$sHash = $oRESET_PIN->generateHash();
$_SESSION['reset_pin_hash'] = $sHash;

//  [1] set template interface values
$templateValues = array(
	'oRESET_PIN'	=> $oRESET_PIN,
	"hash"			=> $sHash,
	'readme_link'	=> 'http://cms-lab.com/_documentation/reset-pin/readme.php',
	'leptoken'		=> get_leptoken()
);
if ( true === is_array( $statusMessage ))
	{ $templateValues["message"] = $statusMessage; }

/**
 *	get the template-engine.
 */
$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule('reset_pin');

echo $oTwig->render(
	"@reset_pin/tool.lte",	//	template-filename
	$templateValues			//	template-data
);

?>
