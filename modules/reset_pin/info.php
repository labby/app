<?php

/**
 * @module          Reset PIN
 * @author          cms-lab
 * @copyright       2019-2021 cms-lab
 * @link            http://www.cms-lab.com
 * @license         custom license: http://cms-lab.com/_documentation/reset-pin/license.php
 * @license_terms   please see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

$module_directory     = "reset_pin";
$module_name          = "Reset PIN";
$module_function      = "tool";
$module_version       = "0.2.0";
$module_platform      = "5.x";
$module_author        = "<a href='https://cms-lab.com' target='_blank'>CMS-LAB</a>";
$module_license       = "<a href='https://cms-lab.com/_documentation/reset-pin/license.php' class='admintools_link' target='_blank'>Custom license</a>";
$module_license_terms = "please see license";
$module_description   = "Tool to reset PIN if <a href='https://doc.lepton-cms.org/docu/english/home/core/features/two-factor-authentication.php' class='admintools_link' target='_blank'>TFA</a> failed";
$module_guid		  = "767281a0-096a-40dc-8987-bc132d76578b";

?>
