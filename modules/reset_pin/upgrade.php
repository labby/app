<?php

/**
 * @module          Reset PIN
 * @author          cms-lab
 * @copyright       2019-2021 cms-lab
 * @link            http://www.cms-lab.com
 * @license         custom license: http://cms-lab.com/_documentation/reset-pin/license.php
 * @license_terms   please see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

// -------------------------------------------
// delete obsolete files in current version
$file_names = array(
	'/modules/reset_pin/templates/info.lte',			// replaced by generic showmodinfo
	'/templates/talgos/backend/reset_pin/info.lte'		// replaced by generic showmodinfo
);

LEPTON_handle::delete_obsolete_files($file_names);

?>
