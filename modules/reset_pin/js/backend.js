/**
 * @module          Reset PIN
 * @author          cms-lab
 * @copyright       2019-2021 cms-lab
 * @link            http://www.cms-lab.com
 * @license         custom license: http://cms-lab.com/_documentation/reset-pin/license.php
 * @license_terms   please see license
 *
 */

$('.ui.dropdown')
  .dropdown()
;


