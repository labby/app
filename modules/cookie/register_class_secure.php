<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2017-2022 cms-lab
 * @link            http://www.cms-lab.com
 * @license         custom license: http://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: http://cms-lab.com/_documentation/cookie/license.php
 *
 */
 

$files_to_register = array();

LEPTON_secure::getInstance()->accessFiles( $files_to_register );

?>