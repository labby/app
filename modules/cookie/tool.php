<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2017-2022 cms-lab
 * @link            http://www.cms-lab.com
 * @license         custom license: http://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: http://cms-lab.com/_documentation/cookie/license.php
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php


// get instance of functions file
$oCOOKIE = cookie::getInstance();

$statusMessage = "";

if ( true === isset($_POST['job']) )
{
	if (( true === isset($_SESSION['cookie_hash']) 
	 && ( $_SESSION['cookie_hash'] == $_POST['hash']) ))
	{
		unset($_SESSION['cookie_hash']);
		unset($_POST['hash']);
	
		switch($_POST['job'])
		{
			case 'save':
				$statusMessage = $oCOOKIE->save_settings();
				break;

			default:
				// nothing
		}
	} else {
		$statusMessage = array( "STATUS" => false, "MESSAGE_ID" => "SAVE_FAILED" );
	}
}

//  [0] build hash
$sHash = $oCOOKIE->generateHash();
$_SESSION['cookie_hash'] = $sHash;

//  [1] set template interface values
$templateValues = array(
	'oCOOKIE'		=> $oCOOKIE,
	'leptoken'		=> get_leptoken(),
	"hash"			=> $sHash,
	'readme_link'	=> "http://cms-lab.com/_documentation/cookie/readme.php",
	'example_link'	=> "https://cookieconsent.insites.com/demos/"
);
if ( true === is_array( $statusMessage ))
	{ $templateValues["message"] = $statusMessage; }

//  [2] get Twig-Instance
$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule('cookie');

echo $oTwig->render(
	"@cookie/tool.lte",		//	template-filename
	$templateValues			//	template-data
);

?>
