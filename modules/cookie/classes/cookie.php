<?php

/**
 * @module          Cookie
 * @author          cms-lab
 * @copyright       2017-2022 cms-lab
 * @link            http://www.cms-lab.com
 * @license         custom license: http://cms-lab.com/_documentation/cookie/license.php
 * @license_terms   see: http://cms-lab.com/_documentation/cookie/license.php
 *
 */

class cookie extends LEPTON_abstract
{
	public $cookie_settings = array();
	public $database = 0;
	public $admin = 0;
	public $action = LEPTON_URL.'/modules/cookie/';
	public $cookie_js = LEPTON_URL.'/modules/cookie/js/cookieconsent.min.js';
	public $cookie_css = LEPTON_URL.'/modules/cookie/css/cookieconsent.min.css';

	public static $instance;

	public function initialize()
	{
		$this->database = LEPTON_database::getInstance();
		$this->init_tool();
	}

	public function init_tool()
	{
		// Get current settings form the db as array
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_cookie",
			true,
			$this->cookie_settings,
			false
		);
	}

	// function called from backend
	public function save_settings()
	{
		$database = LEPTON_database::getInstance();

		//	save elements
		$oREQUEST = LEPTON_request::getInstance();

		$all_fields = array (
			'cookie_id'	=> array ('type' => 'integer+', 'default' => 0),
			'position'	=> array ('type' => 'string_clean', 'default' => ""),
			'layout' 	=> array ('type' => 'string_clean', 'default' => ""),
			'pop_bg'	=> array ('type' => 'string_clean', 'default' => ""),
			'pop_text'	=> array ('type' => 'string_clean', 'default' => ""),
			'but_bg'	=> array ('type' => 'string_clean', 'default' => ""),
			'but_text'	=> array ('type' => 'string_clean', 'default' => ""),
			'but_border'=> array ('type' => 'string_clean', 'default' => ""),
			'type'		=> array ('type' => 'string_clean', 'default' => ""),
			'href'		=> array ('type' => 'string_clean', 'default' => ""),
			'message'	=> array ('type' => 'string_clean', 'default' => ""),
			'dismiss'	=> array ('type' => 'string_clean', 'default' => ""),
			'allow'		=> array ('type' => 'string_clean', 'default' => ""),
			'deny'		=> array ('type' => 'string_clean', 'default' => ""),
			'link'		=> array ('type' => 'string_clean', 'default' => ""),
			'overwrite' => array ('type' => 'integer', 'default' => 0)
		);
		$all_values = $oREQUEST->testPostValues($all_fields);

		$cookie_id = $all_values[ "cookie_id" ];
		if ( $cookie_id > 0 )
		{
			unset( $all_values[ "cookie_id" ] );

			$table = TABLE_PREFIX."mod_cookie";
			$database->build_and_execute(
				'UPDATE',
				$table,
				$all_values,
				"cookie_id = ".$cookie_id
			);

			// reload currently saved data
			$this->init_tool();

			// return success
			return array( "STATUS" => true, "MESSAGE_ID" => "SAVE_OK" );
		}
		else
		{
			return array( "STATUS" => false, "MESSAGE_ID" => "SAVE_FAILED" );
		}
	}

	// called from ... ???
	public function build_js()
	{
		// data for twig template engine
		$data = array(
			'oCOOKIE'		=> $this,
			'js_class'		=>'{{classes}}',
			'readme_link'	=> "http://cms-lab.com/_documentation/cookie/readme.php",
			'example_link'	=> "https://cookieconsent.insites.com/demos/"
		);

		/**
		 *	get the template-engine.
		 */
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('cookie');

		return $oTwig->render(
			"@cookie/output.lte",	//	template-filename
			$data						//	template-data
		);

	}

	/** =========================================================================
	 *
	 * Show an info popup
	 *
	 * @access  public
	 * @param   $modvalues  As optional array containing module specialized values
	 * @param   $bPrompt    True for direct output via echo, false for returning the generated source.
	 * @return  mixed       Depending on the $bPrompt param: boolean or string.
	 */
	public function showmodinfo( $modvalues = null, $bPrompt = true )
	{
		// prepare array with module specific values
		$modvalues = array(
			"IMAGE_URL"		=> "http://cms-lab.com/_documentation/media/cookie/cookie.jpg",
			"BUTTONS"		=> array(
				 "README"		=> array( "AVAILABLE"	=> true,
										"URL"			=> "http://cms-lab.com/_documentation/cookie/readme.php"
				)
			)
		);

		// show module info
		$sSource = parent::showmodinfo( $modvalues );

		return $sSource;
	}

} // end of class
?>
