<?php

/**
 *	@module			Cookie
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2017-2022 cms-lab
 *	@link			http://www.cms-lab.com
 *	@license		custom license: http://cms-lab.com/_documentation/cookie/license.php
 *	@license_terms	see: http://cms-lab.com/_documentation/cookie/license.php
 *	@platform		see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 14:24, 25-06-2020
 * translated from..: EN
 * translated to....: NL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_COOKIE	= array(
	"SAVE_FAILED"			=> "Cookie-instellingen werden niet opgeslagen!",
	"SAVE_OK"				=> "Cookie-instellingen opgeslagen",
	"allow"					=> "Ik ga akkoord",
	"allow_label"			=> "Knop Toestaan",
	"banner_background"		=> "Bannerkleur",
	"banner_text"			=> "Kleur van de bannertekst",
	"button_background"		=> "Knopkleur",
	"button_border"			=> "Knoopjesgrenskleur",
	"button_text"			=> "Knop Tekst-Kleur",
	"deny"					=> "Ik ontken!",
	"deny_label"			=> "Knop ontkennen",
	"dismiss"				=> "Ik accepteer!",
	"dismiss_label"			=> "Knop Ontslag",
	"examples"				=> "Voorbeelden",
	"info"					=> "Addon-informatie",
	"layout"				=> "Indeling",
	"learn_more"			=> "Meer informatie",
	"learn_more_label"		=> "Meer informatie over de link",
	"message"				=> "Deze website maakt gebruik van cookies om ervoor te zorgen dat u de beste ervaring op onze website krijgt.",
	"message_label"			=> "Informatie",
	"overwrite"				=> "Overschrijven van taalbestanden (alleen sites in één taal)",
	"policy_link"			=> "Beleidslink",
	"policy_name"			=> "Beleid",
	"position"				=> "Positie",
	"type"					=> "Type",
	"type_text1"			=> "Vertel gebruikers dat we cookies gebruiken",
	"type_text2"			=> "Laat gebruikers zich afmelden voor cookies (Advanced)",
	"type_text3"			=> "Vraag gebruikers om te kiezen voor cookies (Geavanceerd)",
	"type_text_message1"	=> "Link voor gedetailleerde informatie",
	"type_text_message2"	=> "In het geval van 'geavanceerde opties' worden cookies ingesteld die verwijzen naar de actie van de gebruiker!"
);

?>
