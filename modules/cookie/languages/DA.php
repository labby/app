<?php

/**
 *	@module			Cookie
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2017-2022 cms-lab
 *	@link			http://www.cms-lab.com
 *	@license		custom license: http://cms-lab.com/_documentation/cookie/license.php
 *	@license_terms	see: http://cms-lab.com/_documentation/cookie/license.php
 *	@platform		see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 14:18, 25-06-2020
 * translated from..: EN
 * translated to....: DA
 * translated using.: translate.google.com
 * ==============================================
 */

$MOD_COOKIE	= array(
	"SAVE_FAILED"			=> "Cookie-indstillinger blev ikke gemt!",
	"SAVE_OK"				=> "Cookieindstillinger gemt",
	"allow"					=> "Jeg er enig",
	"allow_label"			=> "Knap Tillad",
	"banner_background"		=> "Bannerfarve",
	"banner_text"			=> "Banner-tekstfarve",
	"button_background"		=> "Knapfarve",
	"button_border"			=> "Knap kantfarve",
	"button_text"			=> "Tekstfarve på knap",
	"deny"					=> "Jeg benægter!",
	"deny_label"			=> "Knap nægtes",
	"dismiss"				=> "Jeg accepterer!",
	"dismiss_label"			=> "Knap afvises",
	"examples"				=> "Eksempler",
	"info"					=> "Addon Info",
	"layout"				=> "Layout",
	"learn_more"			=> "Lær mere",
	"learn_more_label"		=> "Lær mere link",
	"message"				=> "Dette websted bruger cookies for at sikre, at du får den bedste oplevelse på vores hjemmeside.",
	"message_label"			=> "Oplysninger",
	"overwrite"				=> "Overskriv sprogfiler (kun websteder med enkelt sprog)",
	"policy_link"			=> "Politiklink",
	"policy_name"			=> "Politik",
	"position"				=> "Position",
	"type"					=> "Type",
	"type_text1"			=> "Bare fortæl brugerne, at vi bruger cookies",
	"type_text2"			=> "Lad brugere fravælge cookies (Avanceret)",
	"type_text3"			=> "Bed brugere om at tilmelde dig cookies (Avanceret)",
	"type_text_message1"	=> "Link til detaljerede oplysninger</a>",
	"type_text_message2"	=> "I tilfælde af 'avancerede indstillinger' indstilles cookies under henvisning til brugerhandling!"
);

?>
