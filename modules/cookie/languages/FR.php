<?php

/**
 *	@module			Cookie
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2017-2022 cms-lab
 *	@link			http://www.cms-lab.com
 *	@license		custom license: http://cms-lab.com/_documentation/cookie/license.php
 *	@license_terms	see: http://cms-lab.com/_documentation/cookie/license.php
 *	@platform		see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 14:22, 25-06-2020
 * translated from..: EN
 * translated to....: FR
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_COOKIE	= array(
	"SAVE_FAILED"			=> "Les paramètres des cookies n'ont pas été sauvegardés !",
	"SAVE_OK"				=> "Paramètres des cookies sauvegardés",
	"allow"					=> "Je suis d'accord",
	"allow_label"			=> "Bouton Autoriser",
	"banner_background"		=> "Couleur de la bannière",
	"banner_text"			=> "Couleur du texte de la bannière",
	"button_background"		=> "Couleur des boutons",
	"button_border"			=> "Bouton Couleur de la bordure",
	"button_text"			=> "Bouton Couleur du texte",
	"deny"					=> "Je nie !",
	"deny_label"			=> "Bouton Nier",
	"dismiss"				=> "J'accepte !",
	"dismiss_label"			=> "Bouton Rejeté",
	"examples"				=> "Exemples",
	"info"					=> "Addon Info",
	"layout"				=> "Mise en page",
	"learn_more"			=> "En savoir plus",
	"learn_more_label"		=> "Pour en savoir plus, cliquez sur le lien",
	"message"				=> "Ce site web utilise des cookies afin de vous garantir la meilleure expérience possible sur notre site web.",
	"message_label"			=> "Informations",
	"overwrite"				=> "Écraser les fichiers de langue (uniquement les sites en langue unique)",
	"policy_link"			=> "Lien politique",
	"policy_name"			=> "Politique",
	"position"				=> "Position",
	"type"					=> "Type",
	"type_text1"			=> "Dites simplement aux utilisateurs que nous utilisons des cookies",
	"type_text2"			=> "Permettre aux utilisateurs de refuser les cookies (Avancé)",
	"type_text3"			=> "Demander aux utilisateurs d'accepter les cookies (Avancé)",
	"type_text_message1"	=> "Lien pour des informations détaillées",
	"type_text_message2"	=> "Dans le cas des 'options avancées', des cookies sont définis en référence à l'action de l'utilisateur !"
);

?>
