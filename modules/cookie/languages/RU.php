<?php

/**
 *	@module			Cookie
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2017-2022 cms-lab
 *	@link			http://www.cms-lab.com
 *	@license		custom license: http://cms-lab.com/_documentation/cookie/license.php
 *	@license_terms	see: http://cms-lab.com/_documentation/cookie/license.php
 *	@platform		see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 14:26, 25-06-2020
 * translated from..: EN
 * translated to....: RU
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_COOKIE	= array(
	"SAVE_FAILED"			=> "Настройки cookie-файлов не были сохранены!",
	"SAVE_OK"				=> "Настройки cookie-файлов сохранены",
	"allow"					=> "Я согласен",
	"allow_label"			=> "Кнопка 'Разрешить",
	"banner_background"		=> "Цвет баннера",
	"banner_text"			=> "Цвет текста баннера",
	"button_background"		=> "Цвет кнопки",
	"button_border"			=> "Кнопка Граница-Цвет",
	"button_text"			=> "Кнопка 'Цвет текста",
	"deny"					=> "Я отрицаю!",
	"deny_label"			=> "Кнопка отрицания",
	"dismiss"				=> "Я принимаю!",
	"dismiss_label"			=> "Кнопка 'Свободен",
	"examples"				=> "Примеры",
	"info"					=> "Аддон Информация",
	"layout"				=> "Макет",
	"learn_more"			=> "Узнать больше",
	"learn_more_label"		=> "Узнать больше ссылка",
	"message"				=> "Данный веб-сайт использует куки-файлы, чтобы обеспечить вам наилучшее впечатление от работы на нашем веб-сайте.",
	"message_label"			=> "Информация",
	"overwrite"				=> "Перезаписывать языковые файлы (только сайты на одном языке).",
	"policy_link"			=> "Связь с политикой",
	"policy_name"			=> "Политика",
	"position"				=> "Должность",
	"type"					=> "Вид",
	"type_text1"			=> "Просто скажите пользователям, что мы используем куки.",
	"type_text2"			=> "Позволить пользователям отказаться от использования cookie-файлов (Дополнительно).",
	"type_text3"			=> "Попросить пользователей согласиться на использование cookie-файлов (Дополнительно).",
	"type_text_message1"	=> "Ссылка для подробной информации",
	"type_text_message2"	=> "В случае установки 'дополнительных опций' куки-файлы относятся к действиям пользователя!"
);

?>
