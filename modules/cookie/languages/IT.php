<?php

/**
 *	@module			Cookie
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2017-2022 cms-lab
 *	@link			http://www.cms-lab.com
 *	@license		custom license: http://cms-lab.com/_documentation/cookie/license.php
 *	@license_terms	see: http://cms-lab.com/_documentation/cookie/license.php
 *	@platform		see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 14:23, 25-06-2020
 * translated from..: EN
 * translated to....: IT
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_COOKIE	= array(
	"SAVE_FAILED"			=> "Le impostazioni dei cookie non sono state salvate!",
	"SAVE_OK"				=> "Impostazioni dei cookie salvate",
	"allow"					=> "Sono d'accordo",
	"allow_label"			=> "Pulsante Allow",
	"banner_background"		=> "Colore del banner",
	"banner_text"			=> "Colore del testo del banner",
	"button_background"		=> "Colore del pulsante",
	"button_border"			=> "Bordo-Colore del pulsante",
	"button_text"			=> "Pulsante Testo-Colore",
	"deny"					=> "Io nego!",
	"deny_label"			=> "Pulsante Negare",
	"dismiss"				=> "Accetto!",
	"dismiss_label"			=> "Pulsante Licenziamento",
	"examples"				=> "Esempi",
	"info"					=> "Addon Info",
	"layout"				=> "Layout",
	"learn_more"			=> "Per saperne di più",
	"learn_more_label"		=> "Per saperne di più link",
	"message"				=> "Questo sito web utilizza i cookie per garantire la migliore esperienza sul nostro sito web.",
	"message_label"			=> "Informazioni",
	"overwrite"				=> "Sovrascrivere i file di lingua (solo siti in una sola lingua)",
	"policy_link"			=> "Collegamento alla politica",
	"policy_name"			=> "Politica",
	"position"				=> "Posizione",
	"type"					=> "Tipo",
	"type_text1"			=> "Basta dire agli utenti che usiamo i cookie",
	"type_text2"			=> "Permettete agli utenti di rinunciare ai cookie (Avanzato)",
	"type_text3"			=> "Chiedere agli utenti di scegliere i cookie (Avanzato)",
	"type_text_message1"	=> "Link per informazioni dettagliate",
	"type_text_message2"	=> "In caso di 'opzioni avanzate' i cookie sono impostati in riferimento all'azione dell'utente!"
);

?>
