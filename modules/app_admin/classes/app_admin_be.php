<?php

/**
 * @module          App-Admin
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
trait app_admin_be		// all standard vars for all modules
{

//	public $admin = 0;
	public $database = 0;

	public $size = 'medium';		// backend button size		
	public $addon_color = 'olive';
	
	public $action_url = '';	

	public $support_link = "<a href=\"#\">NO Live-Support / FAQ</a>";
	public $image_url = "https://cms-lab.com/_documentation/media/app/app.jpg";
	public $readme_link = "<a class='ui basic button' href='https://os-app.org/free/readme.php' target='_blank'><i class='blue book reader icon'></i>Readme</a>";	

public function get_all_vars() 
	{	
		$this->database = LEPTON_database::getInstance();
//		$this->admin = LEPTON_admin::getInstance();
		
	}	
 } // end class
