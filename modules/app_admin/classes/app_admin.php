<?php

/**
 * @module          App-Admin
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_admin extends LEPTON_abstract
{

	use app_admin_be;
		
	public $months  = array();
	public $states  = array();
	public $logs = 0;
	public $checked_entries = 0;

	public static $instance;

public function initialize() 
	{	
		// all vars from class app_admin_be
		$this->get_all_vars();
		
		$this->action_url = ADMIN_URL . '/admintools/tool.php?tool=app_admin';	
		$this->logs = LEPTON_logging::getInstance();
		$this->addon_color = 'blue';
		$this->init_tool();
		
	}

public function init_tool( $sToolname = '' )
	{
		//get months
		$this->months = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_month " ,
			true,
			$this->months,
			true
		);		

		//get states
		$this->states = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_state " ,
			true,
			$this->states,
			true
		);
		
		//get checked entries
		$this->checked_entries = $this->logs->check_entry();
	}
	
public function display($id) 
	{

		if ( $id == 'month') 
		{		
			// data for twig template engine	
			$data = array(
				'oABE'		=> $this,						
				'leptoken'	=> get_leptoken()		

			);

			//	get the template-engine
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_admin');
					
			echo $oTwig->render( 
				"@app_admin/display_months.lte",	//	template-filename
				$data						//	template-data
			);	
		}

		if ( $id == 'states') 
		{		
			// data for twig template engine	
			$data = array(
				'oABE'		=> $this,		
				'readme_link'	=> "https://os-app.org/free/readme.php",				
				'leptoken'	=> get_leptoken()		

			);

			//	get the template-engine
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_admin');
					
			echo $oTwig->render( 
				"@app_admin/display_states.lte",	//	template-filename
				$data						//	template-data
			);	
		}

		if ( $id == 'logs') 
		{			
			// data for twig template engine	
			$data = array(
				'oABE'		=> $this,		
				'readme_link'	=> "https://os-app.org/free/readme.php",				
				'leptoken'	=> get_leptoken()		

			);

			//	get the template-engine
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_admin');
					
			echo $oTwig->render( 
				"@app_admin/display_log.lte",	//	template-filename
				$data						//	template-data
			);			
		}		
	}	

public function show_detail($id = 0) 
	{
		if($_POST['show_detail'] != 0)
		{
			$id = intval($_POST['show_detail']);
			
			$current_entry = $this->logs->display_secure_entry( $id, $id );
		
			// data for twig template engine	
			$data = array(
				'oABE'			=> $this,
				'leptoken'		=> get_leptoken(),
				'current_entry'	=> $current_entry
				);

			/**	
			 *	get the template-engine.
			 */
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_admin');
				
			echo $oTwig->render( 
				"@app_admin/show_detail.lte",	//	template-filename
				$data						//	template-data
			);
		}
	}		
	

public function save_comment($id = 0) 
	{
		if($_POST['save_comment'] == 99)
		{
			$id = intval($_POST['id']);

			$result = $this->database->simple_query("UPDATE ".$this->logs->log_table." SET comment = '".$_POST['comment']."' WHERE id = ".$id." ");
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}
			else
			{
				// write log entry
				$this->logs->insert_secure_entry( 'UPDATE',$this->language['data_id'].$id.' '.$this->language['data_table'].$this->logs->log_table );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				$_POST = array();
				$_POST['show_info'] = 'logs';
				$_POST['leptoken'] = get_leptoken();
				header("Refresh:0; url=".$this->action_url."&leptoken=".$_POST['leptoken']);	
				exit(0);
			}

			
		}
	}	
	
public function show_info() 
	{
		// data for twig template engine	
		$data = array(
			'oABE'			=> $this
			);

		/**	
		 *	get the template-engine.
		 */
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('app_admin');
			
		echo $oTwig->render( 
			"@app_admin/info.lte",	//	template-filename
			$data						//	template-data
		);		
		
	}	
 } // end class
