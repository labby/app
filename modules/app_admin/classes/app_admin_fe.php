<?php

/**
 * @module          App-Admin
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
trait app_admin_fe		// all standard frontend vars for all app-modules
{

	public $page_id = 0;
	public $section_id = 0;
	public $database = 0;
	public $timestamp = '';
	public $log_entry = 0;

	public $size = 'large';		// frontend button size		
	public $addon_color = 'olive';
	
	public $client_url = '';
	public $client_id = 0;	
	public $basic_settings = array();	
	public $random_value = 0;	

	public $fe_action = '';
	public $fe_help = '';
	public $logout = LEPTON_URL.'/account/logout.php';	
	public $support_link = "<a href=\"#\">NO Live-Support / FAQ</a>";
	public $readme_link =  "<a href=\"https://os-app.org/free/readme.php \" class=\"info\"target=\"_blank\">Readme</a>";	

public function get_all_vars() 
	{	
		$this->fe_action = LEPTON_URL.PAGES_DIRECTORY.LEPTON_frontend::getInstance()->page['link'].PAGE_EXTENSION;
		$this->database = LEPTON_database::getInstance();
		$this->log_entry = LEPTON_logging::getInstance();
		$this->timestamp = date('Y-m-d H:i:s',time());
		$this->client_id = $_SESSION['current_client'] ?? -1;
		$page = $this->database->get_one("SELECT page_id FROM ".TABLE_PREFIX."sections WHERE module = 'app_client' ");
		$page_link = $this->database->get_one("SELECT link FROM ".TABLE_PREFIX."pages WHERE page_id = ".$page." ");
		$this->client_url = LEPTON_URL.PAGES_DIRECTORY.$page_link.PAGE_EXTENSION;
		$this->random_value = $this->database->get_one("SELECT random FROM ".TABLE_PREFIX."mod_app_basic WHERE client_id = ".$this->client_id." ");
		
		//get array of basic settings
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_basic_settings WHERE client_id = ".$this->client_id." AND active = 1",
			true,
			$this->basic_settings,
			false
		);		
	}	
 } // end class
