<?php

/**
 * @module          App-Admin
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$MOD_APP_ADMIN = array(
	// common marker
	'action'	=> "Aktion",
	'active'	=> "Aktiv",
	'active_no'	=> "Nicht Aktiv",
	'additional_values'	=> 'Sonstige Informationen',
	'articles'	=> 'Artikel',	
	'capital'	=> "Hauptstadt",
	'client'	=> "Mandant",
	'chose'		=> 'Bitte auswählen',	
	'customer'	=> "Kunde",
	'eu_trade'	=> "EU-Buchung",
	'header_0'	=> "Tabelle Monate",
	'header_1'	=> "Tabelle Bundesländer",
	'header_2'	=> "Logeinträge",
	'header_3'	=> "Anzahl der fehlerhaften Logeinträge",
	'id'		=> 'Id',
	'info_1'	=> "Bitte geben Sie die Daten ein, Pflichtfelder sind mit einem * gekennzeichnet.",	
	'info_2'	=> "Der aktuelle Mandant ist: ",
	'info_3'	=> "Bitte geben Sie die Daten ein, Pflichtfelder sind mit einem * gekennzeichnet.",
	'info_4'	=> "Nach dem Speichern können Sie den Mandanten unter 'Stammdaten' ändern und ergänzen.",
	'info_5'	=> "Bitte wählen Sie hierfür zuerst den zu bearbeitenden Mandanten aus!",	
	'info_6'	=> "Bitte wählen Sie einen Mandanten aus!",	
	'months'	=> "Monate",
	'name'		=> 'Bezeichnung',
	'no_client'	=> "Kein Mandanten-Id vorhanden, bitte fragen Sie den Administrator!",	
	'show_info'	=> "Addon Info",
	'show_details'	=> "Details anzeigen",
	'states'	=> "Bundesländer",
	'search_head'	=> "Suche",
	'search_text'	=> "Suche über alle Felder",	
	'summary'	=> "Übersicht",
	'supplier'	=> "Lieferant",
	'to_delete_info'=> "Dieser Vorgang kann NICHT rückgängig gemacht werden!",
	'to_delete'		=> "wirklich löschen?",	
	'vat_key'	=> "Mwst-Schlüssel",
	'want_delete'	=> "Wollen Sie den Datensatz ",	
	
	//accounts
	'header_accounts_1'	=> "FiBu-Kontenliste",
	'account_active'	=> 'Konto ist aktiv',
	'account_amount'	=> 'Anfangssaldo',
	'account_no'		=> "Kontonummer",
	'account_name'		=> "Name",
	'account_details'	=> "Konto editieren",
	'account_description'	=> "Beschreibung",
	'account_duplicate'	=> "Konto kopieren",
	'account_start'		=> "am",
	'account_type'		=> "Konto-Typ",
	'account_use'		=> "Dieses Konto verwenden für:",
	'form_relation_1'	=> "Zuordnung des Kontos im Formular Einnahmen/Überschuss Rechnung (EÜR)",
	'form_relation_2'	=> "Zuordnung Zusammenfassende Meldung",
	'form_relation_3'	=> "Zuordnung des Kontos im Formular Umsatzsteueranmeldung",	
	'info_account'		=> 'Bitte achten Sie genau auf die Zuordnungen des Konto!',
	'info_has_start'	=> '<b>Achtung:</b> Diese Daten können nach Speicherung nicht mehr geändert werden.',
	'owner'				=> 'Kontoinhaber',
	'saldo_header'		=> 'Anfangsbestand eintragen',



	//articles
	'account_in'	=> 'Aufwandskonto',
	'account_out'	=> 'Erlöskonto',
	'article_copy'		=> "Artikel kopieren",
	'article_details'	=> "Artikel bearbeiten",
	'article_group'		=> "Artikelgruppe",
	'article_in'		=> "Einkauf",
	'article_name'		=> 'Artikelname',
	'article_no'		=> 'Artikelnummer',
	'article_out'		=> "Verkauf",
	'article_type'		=> 'Artikeltyp',
	'article_unit'		=> 'Einheit',
	'basic_data'		=> 'Stammdaten',
	'group_copy'		=> "Gruppe kopieren",
	'group_details'		=> "Gruppe bearbeiten",
	'in_brutto'			=> "EK brutto",
	'in_description'	=> "Text Einkaufsbuchungen",
	'in_netto'			=> "EK netto",
	'out_brutto'		=> "VK brutto",
	'out_description'	=> "Text Verkaufsbuchungen",
	'out_netto'			=> "VK netto",	
	'type_copy'			=> "Typ kopieren",
	'type_details'		=> "Typ bearbeiten",


	// basic
	'account_no'	=> "Konto-Nr.",
	'client_values'	=> 'Startwerte',
	'eu_trade'		=> "EU-Warenbewegung",	
	'fibu_customer'	=> "Startwert Kunde Fibu",
	'fibu_invoice'	=> "Startwert Rechnungsnummer",
	'fibu_skr'		=> "Sachkontenrahmen",
	'fibu_skr03'	=> "Skr 03",
	'fibu_skr04'	=> "Skr 04",
	'fibu_supplier'	=> "Startwert Lieferant Fibu",
	'first_month'	=> 'Erster Monat des Geschäftsjahres',	
	
	//client
	'add_client'		=> "Mandant hinzufügen",	
	'chose_client'		=> 'Mandantenwahl',
		
	
	// company, contact and bank_data
	'bank_data'			=> 'Bankdaten',
	'bank_name'			=> 'Name der Bank',
	'basic_data'		=> 'Stammdaten',
	'bic'				=> 'BIC-Code',	
	'city'				=> 'Ort',
	'company'			=> "Firmenname",
	'company_short'		=> "Kurzbezeichnung",
	'company_type'		=> "Art des Unternehmens",
	'contact_data'		=> 'Kontaktdaten',
	'country'			=> "Land",
	'delivery_address'	=> 'Lieferanschrift',	
	'email'				=> "Email",
	'extra_1'			=> "Zusatz",
	'iban'				=> 'IBAN-Nr.',
	'internet'			=> 'Internet',
	'invoice_type'		=> 'Rechnung-Vorlage',
	'fax'				=> 'Fax',
	'fon'				=> 'Telefon',
	'fon_1'				=> 'Telefon-Vorwahl',
	'fon_2'				=> 'Telefon-Rufnnumer',
	'legal_form'		=> 'Rechtsform des Unternehmens',
	'no'				=> 'Hausnummer',
	'person_name_1'		=> 'Vorname',
	'person_name_2'		=> 'Nachname',
	'person_salutation'	=> 'Anrede',
	'plz'				=> 'PLZ',
	'send_invoice'		=> 'Rechnung per Mail',
	'sepa'				=> 'Sepa-Lastschrift',
	'state'				=> 'Bundesland',
	'street'			=> 'Straße',
	'tax_number'		=> 'FA-Steuernummer',
	'terms_payment'		=> 'Zahlungsbedingungen',
	'terms_price'		=> 'Preise',
	'terms_price_0'		=> 'Brutto',
	'terms_price_1'		=> 'Netto',
	'vat_id'			=> 'Ust-ID',	
	'zip'				=> 'Postleitzahl',	

	// customer
	'add_customer'		=> 'Kunden hinzufügen',
	'customer_bank'		=> 'Bankdaten ändern',
	'customer_contact'	=> 'Kontaktdaten ändern',
	'customer_delete'	=> 'Kunde löschen',
	'customer_delivery'	=> 'Lieferanschrift ändern',
	'customer_details'	=> 'Details ändern',
	'customer_no'		=> 'Kd-Nr.',
	'customer_short'	=> 'Kurzbezeichnung',
	'person_customer'	=> 'Ansprechpartner',


	// supplier
	'add_supplier'		=> 'Lieferant hinzufügen',
	'person_supplier'	=> 'Ansprechpartner',	
	'supplier'			=> 'Lieferant',
	'supplier_bank'		=> 'Bankdaten ändern',
	'supplier_contact'	=> 'Kontaktdaten ändern',
	'supplier_delete'	=> 'Lieferant löschen',
	'supplier_details'	=> 'Details ändern',	
	'supplier_no'		=> 'Lieferanten-Nr.',
	'supplier_short'	=> 'Kurzbezeichnung',

	// tasks
	'add_creditnote_short'	=> 'Neue Rechnungskorrektur erfassen',
	'add_creditnote_article'=> 'Neue Rechnungskorrektur mit Artikeln erfassen',	
	'add_invoice_short'		=> 'Neue Rechnung erfassen',
	'add_invoice_article'	=> 'Neue Rechnung mit Artikeln erfassen',
	'add_offer'			=> 'Neues Angebot',
	'display_docs'		=> 'Dokumente Anzeigen',
	'payable'			=> 'Zahlbar am',
	'purpose'			=> 'Verwendungszweck',
	'record_amount'		=> 'Betrag',
	'record_date'		=> 'Beleg-Datum',
	'record_no'			=> 'Beleg-Nummer',
	
	//log entries
	'account_new'		=> "Konto wurde angelegt oder upgedated",
	'added_client'		=> "Ein neuer Mandant wurde hinzugefügt",
	'comment'			=> "Anmerkung",
	'customer_deleted'	=> 'Der Kunde wurde deaktiviert',	
	'customer_new'		=> "Neuer Kunde",	
	'date_check'		=> "Check-Datum",
	'data_id'			=> 'Datensatz Id: ',
	'data_table'		=> 'in der Tabelle: ',
	'date_log'			=> "Log-Datum",	
	'from_user_id'		=> "Nutzer-Id ",
	'log_error1'		=> '->FEHLER: Erstellung folgender Datei ist fehlgeschlagen: ',
	'log_error2'		=> '->Folgende Datei wurde erstellt: ',
	'log_error3'		=> '->Folgende Datei wurde übertragen: ',
	'log_error4'		=> '->FEHLER: folgende Datei wurde NICHT übertragen: ',	
	'log_text'			=> "Log-Beschreibung",
	'save_ok'			=> 'Daten wurden erfolgreich gespeichert!',
	'set_session'		=> 'Folgender Mandant wurde ausgewählt:',
	'supplier_deleted'	=> 'Der Lieferant wurde deaktiviert',	
	'supplier_new'		=> "Neuer Lieferant",	
	'user_id'			=> "Nutzer-Id",
	'username'			=> "Nutzer-Name",

		
	// help page
	'attention'					=> 'Bitte beachten',
	'contract_help_header'		=> 'Details zum Auftragsformular',
	'contract_help_info1'		=> 'Alle mit einem Stern gekennzeichneten Felder sind Pflichfelder.',
	'contract_help_info2'		=> 'Sie werden zwingend für die Auftragsbearbeitung benötigt!',	
	'contract_help_info3'		=> 'Bitte prüfen Sie die Eingaben unbedingt auf Richtigkeit!',
	'help_contract_redial'		=> 'Wahlwiederholung',
	'help_test_string_fax'		=> 'Nummer, an die ein Testfax gesendet werden soll.',	
	'help_test_string_email'	=> 'Adresse, an die eine Testmail gesendet werden soll.',
	'help_fax_sender'			=> 'Absenderkennung des Faxes.',
	'help_contract_pages'		=> 'Anzahl der zu versendenden Seiten',
	'help_contract_personal'	=> 'Soll eine Personalisierung vorgenommen werden?',
	'help_email_tracking'		=> 'Soll ein Tracking vorgenommen werden.',
	'help_contract_duplicates'	=> 'Empfänger-Adressen auf Duplikate prüfen',
	'help_contract_international'		=> 'Intarnational versenden?',
	'help_contract_file'		=> 'Format der Vorlage',
	'help_email_attachment'		=> 'Soll eine Anlage mitversendet werden?',
	'help_email_extra_1'		=> 'A/B Test',
	'help_email_extra_2'		=> 'Nicht-Öffner',
	'help_contract_date'		=> 'Ausführungsdatum',	
	'help_upload_name'			=> 'Eine beliebige Bezeichnug',
	'help_email_reply'			=> 'Antwort Adresse',
	'help_contract_time'		=> 'Ausführungsuhrzeit',
	'help_contract_recipients'	=> 'Anzahl der Empfänger.',
	'help_email_sender'			=> 'Absende Mail-Adresse',
	'help_email_sender_name'	=> 'Absender Name',
	'help_email_subject'		=> 'Betreffzeile der Mail.',
	'help_notice'				=> 'Ihre persönlichen Anmerkungen zum Auftrag.',

	//backend
	'be_info_1'		=> 'Hier gibt es nichts zu sehen, ausser einige Addon Informationen.',
	'be_info_2'		=> 'Änderungen können nur im FE gemacht werden.',	
);

?>