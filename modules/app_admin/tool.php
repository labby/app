<?php

/**
 * @module          App-Admin
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php
/*
$debug = true;

if (true === $debug) {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}
*/
if(isset ($_GET['tool'])) {
	$toolname = $_GET['tool'];
} else {
	die('[1]');
}

// get instance of functions file
$oABE = app_admin::getInstance();
	
//die(LEPTON_tools::display($_POST, 'pre','ui blue message'));	
if(isset ($_GET['tool']) && (empty($_POST)) ) {
//	$oABE->display('month');	
	$oABE->display('logs');
} elseif(isset ($_POST['show_info']) && ($_POST['show_info']== 'month') ) {
	$oABE->display('month');	
} elseif(isset ($_POST['show_info']) && ($_POST['show_info']== 'states') ) {
	$oABE->display('states');
} elseif(isset ($_POST['show_info']) && ($_POST['show_info']== 'logs') ) {
	$oABE->display('logs');
} elseif(isset ($_POST['show_detail']) && is_numeric($_POST['show_detail']) ) {
	$oABE->show_detail($_POST['show_detail']);
} elseif(isset ($_POST['save_comment']) && ($_POST['save_comment']== '99') ) {
	$oABE->save_comment('99');
} elseif(isset ($_POST['show_info']) && ($_POST['show_info']== 'show') ) {
	$oABE->show_info();	
} else {
	die('something went wrong');
}

?>