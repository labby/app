<?php

/**
 * @module          App-Admin
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `month` varchar(64) NOT NULL DEFAULT '',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_admin_month", $table_fields);


$field_values="
	(1,	'Januar'),
	(2,	'Februar'),
	(3,	'März'),
	(4,	'April'),
	(5,	'Mai'),
	(6,	'Juni'),
	(7,	'Juli'),
	(8,	'August'),
	(9,	'September'),
	(10,'Oktober'),
	(11,'November'),
	(12,'Dezember')
";
LEPTON_handle::insert_values("mod_app_admin_month", $field_values);

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `state` varchar(128) NOT NULL DEFAULT '',
	  `capital` varchar(128) NOT NULL DEFAULT '',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_admin_state", $table_fields);


$field_values="
	(1,	'Baden-Württemberg','Stuttgart'),
	(2,	'Bayern','München'),
	(3,	'Berlin','Berlin'),
	(4,	'Brandenburg','Potsdam'),
	(5,	'Bremen','Bremen'),
	(6,	'Hamburg','Hamburg'),
	(7,	'Hessen','Wiesbaden'),
	(8,	'Mecklenburg-Vorpommern','Schwerin'),
	(9,	'Niedersachsen','Hannover'),
	(10,'Nordrhein-Westfalen','Düsseldorf'),
	(11,'Rheinland-Pfalz','Mainz'),
	(12,'Saarland','Saarbrücken'),
	(13,'Sachsen','Dresden'),
	(14,'Sachsen-Anhalt','Magdeburg'),
	(15,'Schleswig-Holstein	','Kiel'),
	(16,'Thüringen','Erfurt')
";
LEPTON_handle::insert_values("mod_app_admin_state", $field_values);

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `days` int(11) NOT NULL DEFAULT '0',
	  `percent` int(11) NOT NULL DEFAULT '0',
	  `text` varchar(256) NOT NULL DEFAULT '',
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_admin_payment_terms", $table_fields);


$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `text` varchar(256) NOT NULL DEFAULT '',
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_admin_account_type", $table_fields);

$field_values="
	(1,	'Finanzkonten',1),
	(2,	'Kreditkartenkonten',1),
	(3,	'Vermögen',1),
	(4,	'Schulden',1),
	(5,	'Einnahmen',1),
	(6,	'Ausgaben',1),
	(7,	'Kapital',1)
";
LEPTON_handle::insert_values("mod_app_admin_account_type", $field_values);


$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `text` varchar(256) NOT NULL DEFAULT '',
	  `account_type` int(11) NOT NULL DEFAULT '1',
	  `extra` varchar(256) NOT NULL DEFAULT '',	  
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_admin_account_use", $table_fields);


$field_values="
	(NULL,'Sachanlagen',3,'',1),
	(NULL,'offene Kundenrechnungen',3,'',1),
	(NULL,'Vorräte',3,'',1),
	(NULL,'betriebseigene Wertpapiere',3,'',1),
	(NULL,'Forderungen',3,'',1),
	(NULL,'sonstige Vermögensgegenstände',3,'',1),
	(NULL,'Schecks, Kassenbestand, Bankguthaben',3,'',1),
	(NULL,'Aufwendungen Ingangsetzung Geschäftsbetrieb',3,'',1),
	(NULL,'Ausstehende Einlagen auf das gezeichnete Kapital',3,'',1),
	(NULL,'Immaterielle Vermögensgegenstände',3,'',1),
	(NULL,'Finanzanlagen',3,'',1),
	(NULL,'Aktive Rechnungsabgrenzungsposten',3,'',1),
	(NULL,'aktive latente Steuern',3,'',1),
	(NULL,'Verbindlichkeiten gegenüber Kreditinstituten',4,'',1),
	(NULL,'Offene Lieferantenrechnungen',4,'',1),
	(NULL,'Sonstige Schulden',4,'',1),
	(NULL,'Kurzfristige betiebsbezogene Kredite',4,'',1),
	(NULL,'Gewinnrücklagen',4,'',1),
	(NULL,'Rückstellungen für Pensionen',4,'',1),
	(NULL,'Steuerrückstellungen',4,'',1),
	(NULL,'Sonstige Rückstellungen',4,'',1),
	(NULL,'Passive Rechnungsabgrenzungsposten',4,'',1),
	(NULL,'Langfristige Verbindlichkeiten',4,'',1),
	(NULL,'Passive latente Steuern',4,'',1),
	(NULL,'Anzahlungen',4,'',1),
	(NULL,'Umsatzerlöse',5,'',1),
	(NULL,'sonstige Erlöse',5,'',1),
	(NULL,'andere aktivierte Eigenleistungen',5,'',1),
	(NULL,'Eigenverbrauch',5,'',1),
	(NULL,'Zinserträge',5,'',1),
	(NULL,'außerordentliche Erträge',5,'',1),
	(NULL,'Erträge aus anderen Wertpapieren und Ausleihen',5,'',1),
	(NULL,'Erträge aus Beteiligungen',5,'',1),
	(NULL,'Sonstiger Eigenverbrauch',5,'',1),	
	(NULL,'Zinsen',6,'',1),
	(NULL,'Versicherungen und Beiträge',6,'',1),
	(NULL,'Abschreibungen',6,'',1),
	(NULL,'Personalkosten',6,'',1),
	(NULL,'Telefon und Porto',6,'',1),
	(NULL,'KFZ-Kosten',6,'',1),
	(NULL,'Raumkosten',6,'',1),
	(NULL,'Reise und Bewirtung',6,'',1),
	(NULL,'Betriebliche Stuern',6,'',1),
	(NULL,'Sonstige Ausgaben',6,'',1),
	(NULL,'Kosten für Waren',6,'',1),
	(NULL,'Aufwendungen für Leistungen',6,'',1),
	(NULL,'Steuern vpm Einkommen und Ertrag',6,'',1),
	(NULL,'Außerordentliche Ausgaben',6,'',1),
	(NULL,'Abschreibungen auf Finanzanlagen und Wertpapiere',6,'',1),
	(NULL,'Abschreibungen auf Vermögensgegenstände des Umlaufvermögens',6,'',1),
	(NULL,'Abgaben zur Sozialversicherung',6,'',1),
	(NULL,'Bestandsveränderungen an fertigen und unfertigen Erzeugnissen',6,'',1),
	(NULL,'Private Entnahme von Geld und Gütern',7,'',1),
	(NULL,'Private Einlage von Geld und Gütern',7,'',1),
	(NULL,'Gezeichnetes Kapital',7,'',1),
	(NULL,'Kapital zum Geschäftsjahresbeginn',7,'',1),
	(NULL,'Kapitalrücklagen',7,'',1),
	(NULL,'Gewinnrücklagen',7,'',1),
	(NULL,'Sonstige Verbindlichkeiten',7,'',1),
	(NULL,'Ausstehende Einlagen auf das gezeichnete Kapital',7,'',1),
	(NULL,'Gewinnvortrag/Verlustvortrag',7,'',1)
";
LEPTON_handle::insert_values("mod_app_admin_account_use", $field_values);


$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `key` varchar(32) NOT NULL DEFAULT '',
	  `percent` int(11) NOT NULL DEFAULT -1,
	  `text` varchar(32) NOT NULL DEFAULT '',
	  `vat_eu` int(11) NOT NULL DEFAULT 0,
	  `relate_1` int(11) NOT NULL DEFAULT -1,
	  `relate_2` int(11) NOT NULL DEFAULT -1,
	  `relate_3` int(11) NOT NULL DEFAULT -1,
	  `relate_4` int(11) NOT NULL DEFAULT -1,
	  `active` int(11) NOT NULL DEFAULT 1,
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_admin_vat_key", $table_fields);

$field_values="
	(NULL,'EUB',0,'EU steuerbefreit',1,41,91,791,-1,1),
	(NULL,'19%',19,'Inland 19%',0,81,66,320,-1,1),
	(NULL,'7%',7,'Inland 7%',0,86,66,320,-1,1)

";
LEPTON_handle::insert_values("mod_app_admin_vat_key", $field_values);


$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `type` int(11) NOT NULL DEFAULT -1,
	  `text` varchar(256) NOT NULL DEFAULT '',
	  `form_line` int(11) NOT NULL DEFAULT -1,
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_admin_vat_relation", $table_fields);

$field_values="
	(NULL,-1,'Kein',0,1),
	(NULL,1,'Steuerpflichtige Umsätze zum Steuersatzt von 7%',86,1),
	(NULL,2,'Vorsteuerbeträge von Rechnungen anderer Unternehmer',66,1),
	(NULL,0,'Vorsteuerbeträge aus Rechnungen von anderen Unternehmen',320,1)

";
LEPTON_handle::insert_values("mod_app_admin_vat_relation", $field_values);

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `type` int(11) NOT NULL DEFAULT -1,
	  `text` varchar(256) NOT NULL DEFAULT '',
	  `form_line` int(11) NOT NULL DEFAULT -1,
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_admin_form_relation", $table_fields);

$field_values="
	(NULL,-1,'Nicht Relevant',0,1),
	(NULL,1,'Steuern, Versicherungen und Maut',145,1),
	(NULL,1,'Abziehbare Bewirtungskosten',175,1),
	(NULL,1,'Aufwendungen für Telekommunikation',210,1)

";
LEPTON_handle::insert_values("mod_app_admin_form_relation", $field_values);
?>