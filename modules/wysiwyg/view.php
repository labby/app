<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

if( (true === wysiwyg_preview::$iUsePreview) && ($section_id == wysiwyg_preview::$iPreviewID) )
{
    $content = wysiwyg_preview::$sContent;
} 
else 
{
    // Get content
    $temp = $database->get_one("SELECT `content` FROM `".TABLE_PREFIX."mod_wysiwyg` WHERE `section_id` = '".$section_id."'");
	$content = htmlspecialchars_decode($temp);
}

/**
 *  htmlpurifier
 *  see: http://htmlpurifier.org/docs
 */
$oPURIFIER = lib_lepton::getToolInstance("htmlpurifier");

$content = $oPURIFIER->purify($content);

echo $content;
