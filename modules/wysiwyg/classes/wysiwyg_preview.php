<?php
/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */
 
 
class wysiwyg_preview
{
    static $instance;
    
    const HISTORY_TABLE = "mod_wysiwyg_history";
	const WYSIWYG_TABLE = "mod_wysiwyg";
    
    static $iUsePreview = false;
    
    static $iPreviewID = 0;
    
    static $sContent = "";
    
    static function getPreviewData( $iID = 0 )
    {
        $database = LEPTON_database::getInstance();
        
        $aPreviewData = [];
        $database->execute_query(
            "SELECT h.*, s.page_id, p.link 
                FROM `".TABLE_PREFIX.SELF::HISTORY_TABLE."` as h
                JOIN 
                    `".TABLE_PREFIX."mod_wysiwyg` as s,
                    `".TABLE_PREFIX."pages` as p 
                WHERE `id`=".$iID."
                AND s.section_id = h.section_id
                AND p.page_id = s.page_id
            ",
            true,
            $aPreviewData,
            false
        );
        
        if( 0 < count($aPreviewData))
        {
            self::$iUsePreview = true;
            self::$iPreviewID = $iID; 
            self::$sContent = htmlspecialchars_decode($aPreviewData["content"]);
        }
        
        return $aPreviewData;
    }
	
    static function getPreviewWC( $iSId = 0 )
    {
        $database = LEPTON_database::getInstance();
        
        $aPreviewData = [];
        $database->execute_query(
            "SELECT * FROM `".TABLE_PREFIX.SELF::WYSIWYG_TABLE."` WHERE `section_id`=".$iSId,
            true,
            $aPreviewData,
            false
        );
        
        if( 0 < count($aPreviewData))
        {
            self::$iUsePreview = true;
            self::$iPreviewID = $iSId; 
            self::$sContent = htmlspecialchars_decode($aPreviewData["working_content"]);
        }
        
        return $aPreviewData;
    }	

}