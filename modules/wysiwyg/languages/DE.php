<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// Deutsche Modulbeschreibung
$module_description = 'Dieses Modul ermöglicht die Bearbeitung von Seiteninhalten mit Hilfe eines grafischen Editors';

$MOD_WYSIWYG = Array(
	'action'	=> 'Aktion',
	'autosave_always'	=> 'Änderungen veröffentlichen und bisherigen Inhalt in der Historie speichern (max. '.MAX_WYSIWYG_HISTORY.' möglich)',
	'button_copy'	=> 'Entwürfe',
	'button_history'=> 'Historie',
	'comment'		=> 'Kommentar',
	'date'			=> 'Datum',
	'docs'			=> 'Dokumentation',
	'header1'		=> 'Nr',
	'publish_changes'=> 'Änderungen veröffentlichen und bisherigen Inhalt ersetzen (Standard)',
	'pushed_by'		=> 'Zur Historie zugefügt von',
	'saved_by'		=> 'Zuletzt gespeichert von',
	'save_ok'		=> 'Daten erfolgreich gespeichert!',
	'use_workingcopy'=> 'Änderungen als Entwurf speichern (nur ein Entwurf pro Abschnitt möglich)',
	'version_delete'=> 'Löschen',
	'version_restore'=> 'Wiederherstellen',
	'version_view'	=> 'Vorschau',
	'want_really'	=> "wirklich löschen",	
	'want_delete'	=> "Wollen Sie die Version vom ",
	'want_delete_wc'=> "Wollen Sie den Entwurf von Sektion ",
	'wcopy_by'		=> 'Als Entwurf gespeichert von',	
	'wc_delete'		=> 'Löschen',
	'wc_restore'	=> 'Wiederherstellen',
	'wc_preview'	=> 'Vorschau',
	'what_to_do'	=> 'Bitte wählen'
);
