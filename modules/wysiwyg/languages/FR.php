<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

//Module Description
$module_description = 'Ce module permet de modifier le contenu de la page &agrave; l&apos;aide d&apos;un &eacute;diteur graphique';

$MOD_WYSIWYG = Array(
	'action'	=> 'Action',
	'autosave_always'	=> 'Publish changes and keep old content (max '.MAX_WYSIWYG_HISTORY.')',
	'button_copy'	=> 'Working Copy',
	'button_history'=> 'Manage History',
	'comment'		=> 'Comment',
	'date'			=> 'Date',
	'docs'			=> 'Documentation',
	'header1'		=> 'No',
	'publish_changes'=> 'Publish changes and replace old content',
	'pushed_by'=> 'Pushed to history by',
	'saved_by'=> 'Saved by',
	'save_ok'		=> 'Data saved successfull!',
	'use_workingcopy'=> 'Save as working copy',
	'version_delete'=> 'Delete Version',
	'version_restore'=> 'Restore Version',
	'version_view'=> 'Preview Version',
	'want_really'	=> "really",	
	'want_delete'	=> "Do you want to delete version from ",
	'want_delete_wc'=> "Do you want to delete Working Copy of section ",
	'wcopy_by'		=> 'Pushed to Working Copy by',	
	'wc_delete'		=> 'Delete Working Copy',
	'wc_preview'	=> 'Preview Working Copy',	
	'wc_restore'	=> 'Restore Working Copy',	
	'what_to_do'	=> 'Please choose'
);