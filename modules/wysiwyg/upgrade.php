<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


$files = array(
	'/modules/wysiwyg/register_parser.php'
);
LEPTON_handle::delete_obsolete_files ($files);

// add new table for history if not exists
$aAllTables = $database->list_tables( TABLE_PREFIX );

$table = 'mod_wysiwyg_history';
if(!in_array( $table, $aAllTables))
{
	$table_fields="
        `id`            INT NOT NULL AUTO_INCREMENT,
        `section_id`    INT NOT NULL DEFAULT 0,
        `version`       INT NOT NULL DEFAULT 0,
        `autosaved`     INT NOT NULL DEFAULT 0,
        `date`          INT NOT NULL DEFAULT 0,
        `user_id`       INT NOT NULL DEFAULT -1 COMMENT 'User who last saved this content',
        `user_id_hist`  INT NOT NULL DEFAULT -1 COMMENT 'User who pushed this content to history',
        `comment`       TEXT NOT NULL,
        `content`       LONGTEXT NOT NULL,
		`text`      	LONGTEXT NOT NULL,
        PRIMARY KEY (`id`),
        INDEX (`section_id`)
	";
	LEPTON_handle::install_table($table, $table_fields);
}

// extend existing table mod_wysiwyg
$table = 'mod_wysiwyg';

$fields = array();
$database->describe_table(
    TABLE_PREFIX.$table,
    $fields,
    LEPTON_database::DESCRIBE_ONLY_NAMES
);

if(!defined("MAX_WYSIWYG_HISTORY"))
{
    define("MAX_WYSIWYG_HISTORY", 6);
}

$aUpdateFields = [
    "last_version" => "INT NOT NULL DEFAULT '0' AFTER `page_id`",
    "hist_autosave" => "TINYINT NOT NULL DEFAULT 0 AFTER `last_version`",
    "max_history"   => "INT NOT NULL DEFAULT ".MAX_WYSIWYG_HISTORY." AFTER `hist_autosave`",
    "use_workingcopy" => "TINYINT NOT NULL DEFAULT 0 AFTER `max_history`",
    "user_id"           => "INT NOT NULL DEFAULT -1 AFTER `use_workingcopy`",
    "user_id_working"   => "INT NOT NULL DEFAULT -1 AFTER `user_id`",
    "content_comment"   => "TEXT NOT NULL AFTER `content`",
    "working_content"   => "LONGTEXT NOT NULL",
    "working_content_comment" => "TEXT NOT NULL"
];

foreach( $aUpdateFields as $sFieldname => $sDescription)
{
    if (!in_array( $sFieldname, $fields)) 
    {
        $database->add_column(
            $table,
            $sFieldname,
            $sDescription
        ); 
    }
}

// Delete obsolete language file
$file_names = array(
	'/modules/wysiwyg/languages/DA.php',
	'/modules/wysiwyg/languages/NO.php',
	'/modules/wysiwyg/languages/RU.php'
);
LEPTON_handle::delete_obsolete_files( $file_names );

