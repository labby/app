<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          wysiwyg
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

/** 
 *  PreView
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

if (isset($_GET['pid']))
{
    $iDisplayID = intval($_GET['pid']);
    if($iDisplayID < 1)
    {
        die( "[2] Nothing to display!" );
    }
	
	$database = LEPTON_database::getInstance();

	$aAllPreviewData =  wysiwyg_preview::getPreviewData( $iDisplayID );

	$page_id = $aAllPreviewData["page_id"];

	require LEPTON_PATH."/index.php";
	
}

if (isset($_GET['sid']))
{
    $iDisplayID = intval($_GET['sid']);
    if($iDisplayID < 1)
    {
        die( "[2] Nothing to display!" );
    }
	$database = LEPTON_database::getInstance();

	$aAllPreviewData =  wysiwyg_preview::getPreviewWC( $iDisplayID );

	$page_id = $aAllPreviewData["page_id"];

	require LEPTON_PATH."/index.php";	
}

