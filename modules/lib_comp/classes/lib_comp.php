<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_comp
 * @author          LEPTON Project, various
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

/**
 *  The LEPTON class for backward compatibility in addons
 *
 */
class lib_comp
{ 
    static $bXRunInit = false;
    
    const JSCALENDAR_URL	= LEPTON_URL."/modules/lib_comp/dist/jscalendar";
    const JSCALENDAR_PATH	= LEPTON_PATH."/modules/lib_comp/dist/jscalendar";
    const JQUERY_URL		= LEPTON_URL."/modules/lib_comp/dist/jquery_ext";
    const JQUERY_PATH		= LEPTON_PATH."/modules/lib_comp/dist/jquery_ext";
    
    /**
     *  Static Method, called by external modules (e.g. WB/WBCE)
     *
     *  @access static
     *  @param  string  $sModuleName    Optional param with the name of the module witch is calling this method.
     *  @return nothing
     *
     */
    static function init( $sModuleName = "" )
    {
        // echo "call by ".$sModuleName." ";
        self::$bXRunInit = true;
        
        // 1 Old Template engine in WB/WBCE
        require_once dirname(__DIR__)."/dist/phplib/template.inc";
		
        // 2 Fallback old WB/WBCE addons
        if(!defined('WB_PATH'))
		{
			define('WB_URL', LEPTON_URL);
			define('WB_PATH', LEPTON_PATH);			
		}			
    }

    static function reset( $sModuleName = "" )
    {
        // echo "call by ".$sModuleName." ";
        self::$bXRunInit = false;
    }

    static function initJsCalendar()
    {
        require_once self::JSCALENDAR_PATH."/jscalendar-functions.php";
    }
    
    static function JsCalendarsSetup()
    {
        require self::JSCALENDAR_PATH."/wb-setup.php";
    }
}

final class lib_comp_query
{    
    public $oExecutedStatement = NULL;
    
    public function __construct( &$oStatement = NULL )
    {
        $this->oExecutedStatement = $oStatement; 
    }
    
    //  Old WB/WBCE!
    public function numRows()
    {
        return $this->oExecutedStatement->rowCount();
    }
            
    public function fetchRow()
    {
        return $this->oExecutedStatement->fetch( PDO::FETCH_BOTH );
    }

    //  PDO :: https://www.php.net/manual/de/class.pdostatement.php
    public function rowCount()
    {
        return $this->oExecutedStatement->rowCount();
    }        
    
    public function fetch()
    {
        return $this->oExecutedStatement->fetch();
    }
            
    public function fetchAll()
    {
        return $this->oExecutedStatement->fetchAll();
    }
            
    public function fetchColumn()
    {
        return $this->oExecutedStatement->fetchColumn();
    }
            
    public function fetchObject()
    {
        return $this->oExecutedStatement->fetchObject();
    }
            
    public function errorCode()
    {
        return $this->oExecutedStatement->errorCode();
    }
            
    public function errorInfo()
    {
        return $this->oExecutedStatement->errorInfo();
    }  
    
    public function __call( $sName, $aParams)
    {
        switch( $sName )
        {

            default:
                // echo "call ".$sName." ";
                die( "Method is not supported by this subclass! ".$sName );
        }
    }
}