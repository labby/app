<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 * @module          lib_lepton
 * @author          LEPTON Project
 * @copyright       2010-2021 LEPTON Project
 * @link            https://lepton-cms.org
 * @license         http://www.gnu.org/licenses/gpl.html
 * @license_terms   please see info.php of this module
 *
 */

class lib_lepton extends LEPTON_abstract
{
    public static $instance;
    
    public function initialize()
    {

    }

    /**
     * For (auto-)loading instances from sub-folders (tools) inside
     * the lib_lepton, e.g. 'pclzip', 'datetools', etc.
     * @code{.php}
     *    $oUpload = lib_lepton::getToolInstance("upload", $_FILES[ $fileid ]);
     * @endcode

     * @param   string  A valid "toolname" of one of the sub-modules.
     * @param   mixed   An optional valid string, array, or object-instance.
     * @param   mixed   An optional valid string, array, or object-instance.
     *
     * @return mixed    A valid instance (object-reference) or NULL if failed or no name match.
     * 
     */
    static public function getToolInstance($sToolName = "", $sParam2="", $sParam3="" )
    {
        $returnValue = NULL;

        switch(strtolower($sToolName))
        {
            //  [1]
            case "datetools":
                require_once __dir__."/../datetools/lib_lepton_datetools.php";
                $returnValue = 	lib_lepton_datetools::getInstance( $sParam2 );	
                break;

            //  [2a] release 2.8.2   -> no original source available
            case "pclzip":
                require_once __dir__."/../pclzip/pclzip.lib.php";
                $returnValue = new PclZip( $sParam2 );
                break;

            //  [2b] release 4.0.0 , as an alternative to outdated 2a, for details: https://github.com/Ne-Lexa/php-zip
            case "php-zip":
                // 2b.1 LEPTON_autoloader
                require_once __dir__."/../php-zip/PhpZip_loader.php";
                PhpZip_loader::getInstance()->register();

                // 2b.2 get file and instance
                require_once __dir__."/../php-zip/src/ZipFile.php";
                $returnValue = new PhpZip\ZipFile();
                break;

            //  [3] release 2.0.9 plus additional commits up to 2021-09-25, for details please see https://github.com/verot/class.upload.php
            case "upload": 
                require_once __dir__."/../upload/class.upload.php";
                if($sParam3 == "")
                {
                    $sParam3 = strtolower(LANGUAGE)."_".LANGUAGE;
                }
                $returnValue = new Verot\Upload\Upload( $sParam2, $sParam3 );
                break;

            //  [4] release 2.9 plus additional commits up to 2021-09-25, for details please see https://github.com/ifsnop/mysqldump-php
            case "mysqldump":
                require_once dirname(__dir__)."/mysqldump/lib_lepton_mysqldump.php";
                $returnValue = lib_lepton_mysqldump::getInstance( $sParam2 );
                break;

            //  [5] release 4.13.0, for details please see http://htmlpurifier.org
            case "htmlpurifier":
                require_once dirname(__dir__)."/htmlpurifier/HTMLPurifier.auto.php";
                $config = HTMLPurifier_Config::createDefault();

                //  [5.1] For YouTube and Vimeo embeddet videos inside iFrames
                //          see: http://htmlpurifier.org/live/configdoc/plain.html#HTML.SafeIframe
                $config->set('HTML.SafeIframe', true);

                //          see: http://htmlpurifier.org/live/configdoc/plain.html#URI.SafeIframeRegexp
                $config->set('URI.SafeIframeRegexp', '%.*%'); //^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%'); //allow YouTube and Vimeo

                //  [5.2] Caching files
                //          see: http://htmlpurifier.org/live/configdoc/plain.html#Cache.DefinitionImpl
                $config->set('Cache.DefinitionImpl', null); // No local caching during development!

                //  [5.3] Keep ids inside html tags! e.g.: <h1 id="myAnchor">here is top</h1>
                //          see: http://htmlpurifier.org/docs/enduser-id.html
                $config->set('Attr.EnableID', true);

                //  [5.4] Keep "target"
                //          see: http://htmlpurifier.org/live/configdoc/plain.html#HTML.AllowedAttributes
                $config->set('Attr.AllowedFrameTargets', '_blank,_self');

                //  [5.5] add allowed attributes
                $def = $config->getHTMLDefinition(true);
                $def->addAttribute("a", "data-tab", "Text");
                $def->addAttribute("div", "data-tab", "Text");

                $returnValue = new HTMLPurifier($config);
                break;

                //  [6] release 2.0.0 dev plus additional commits up to 2021-09-25, for details please see https://github.com/jeroendesloovere/vcard/tree/2.0.0-dev
                case "vcard":
                    require_once dirname(__dir__)."/vcard/lib_lepton_vcard.php";
                    $returnValue = lib_lepton_vcard::getInstance( $sParam2 );
                    break;
        }

        return $returnValue;
    }
}
