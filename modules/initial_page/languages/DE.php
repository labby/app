<?php

/**
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$MOD_INITIAL_PAGE = array(
	"label_user"	=> "Benutzer",
	"label_page"	=> "Seite",
	"label_default"	=> "Standard Startseite",
	"label_param"	=> "Optionale Parameter",
	"head_select"	=> "Bitte eine Standard Startseite f&uuml;r den Benutzer ausw&auml;hlen",
	"SAVE_OK"	    => "Einstellungen wurden gespeichert.",
	"SAVE_FAILED"   => "Beim speichern der Einstellungen gab es ein Problem.<br />"
						."Bitte nochmals versuchen.<br />"
						."<br />"
						."Im Wiederholungsfall den Fehler bitte <a href='https://forum.lepton-cms.org' target='_blank'>[hier]</a> melden."
);
