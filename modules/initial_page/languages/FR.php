<?php

/**
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:05, 25-06-2020
 * translated from..: EN
 * translated to....: FR
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_INITIAL_PAGE	= array(
	"label_user"		=> "Utilisateur",
	"label_page"		=> "Page",
	"label_default"		=> "Page de démarrage par défaut",
	"label_param"		=> "Paramètres optionnels",
	"head_select"		=> "Veuillez sélectionner une page initiale pour l'utilisateur (ou les utilisateurs)",
	"SAVE_OK"			=> "Les réglages sont enregistrés.",
	"SAVE_FAILED"		=> "Une erreur s'est produite lors de la sauvegarde. <br />Veuillez réessayer. <br /><br />Si l'erreur se reproduit, veuillez signaler le problème <a href='https://forum.lepton-cms.org' target='_blank'>[ici]</a>."
);

?>
