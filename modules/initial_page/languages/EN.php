<?php

/**
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$MOD_INITIAL_PAGE = array(
	"label_user"	=> "User",
	"label_page"	=> "Page",
	"label_default"	=> "Default Startpage",
	"label_param"	=> "Optional params",
	"head_select"	=> "Please select one initial page for the user(-s)",
	"SAVE_OK"		=> "Settings are saved.",
	"SAVE_FAILED"	=> "An error occured during save.<br />"
						."Please try again.<br />"
						."<br />"
						."If error occurs again, please report the issue <a href='https://forum.lepton-cms.org' target='_blank'>[here]</a>."
);
