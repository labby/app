<?php

/**
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:04, 25-06-2020
 * translated from..: EN
 * translated to....: DA
 * translated using.: translate.google.com
 * ==============================================
 */

$MOD_INITIAL_PAGE	= array(
	"label_user"		=> "Bruger",
	"label_page"		=> "Side",
	"label_default"		=> "Standard startside",
	"label_param"		=> "Valgfrie params",
	"head_select"		=> "Vælg en indledende side til brugeren (-er)",
	"SAVE_OK"			=> "Indstillinger gemmes.",
	"SAVE_FAILED"		=> "Der opstod en fejl under gemmen. <br /> Prøv igen. <br /># BR # Hvis der opstår en fejl igen, skal du rapportere problemet <a href='https://forum.lepton-cms.org' target='_blank'>[her]</a>."
);

?>
