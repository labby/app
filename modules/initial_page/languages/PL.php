<?php

/**
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 21:08, 25-06-2020
 * translated from..: EN
 * translated to....: PL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_INITIAL_PAGE	= array(
	"label_user"		=> "Użytkownik",
	"label_page"		=> "Strona",
	"label_default"		=> "Domyślna strona startowa",
	"label_param"		=> "Parametry opcjonalne",
	"head_select"		=> "Proszę wybrać jedną stronę początkową dla użytkownika(-ów)",
	"SAVE_OK"			=> "Ustawienia są zapisywane.",
	"SAVE_FAILED"		=> "Wystąpił błąd podczas zapisywania.<br />Proszę spróbować ponownie.<br /><br />Jeśli błąd wystąpi ponownie, proszę zgłosić problem <a href='https://forum.lepton-cms.org' target='_blank'>[tutaj]</a>."
);

?>
