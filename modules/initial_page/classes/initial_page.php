<?php

/**
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

class initial_page extends LEPTON_abstract
{
    // Own instance for this class!
    static $instance;

    private $db = NULL;

    private $table = "mod_initial_page";

    private $backend_pages = array ();

    public function initialize(  ) {
        global $MENU;
        
        $this->db = LEPTON_database::getInstance();
        
        $this->backend_pages = array (
            "Start"             => "start/index.php",
            $MENU["ADDON"]      => "addons/index.php",
            $MENU["ADMINTOOLS"] => "admintools/index.php",
            $MENU["GROUPS"]     => "groups/index.php",
            $MENU["LANGUAGES"]  => "languages/index.php",
            $MENU["MEDIA"]      => "media/index.php",
            $MENU["MODULES"]    => "modules/index.php",
            $MENU["PAGES"]      => "pages/index.php",
            $MENU["PREFERENCES"]=> "preferences/index.php",
            $MENU["SETTINGS"]   => "settings/index.php",
            $MENU["TEMPLATES"]  => "templates/index.php",
            $MENU["USERS"]      => "users/index.php"
        );
        
        $this->table = TABLE_PREFIX.$this->table;
    }

    public function execute( $aUser_id=NULL, $aPath_ref= NULL )
    {
        if ( ($aUser_id != NULL) && ($aPath_ref != NULL) ) {
            $this->internalTestUser($aUser_id, $aPath_ref);
        } else {
            $this->internalTestEntries();
        }
    }

    public function get_backend_pages_select($name="init_page_select", $selected = "") {
        global $MENU;
        
        $values = array();
        
        /**
         *    first: add pages ...
         *
         */
        require_once( LEPTON_PATH."/framework/functions/function.page_tree.php" );
        $all_pages = array();
        page_tree(0, $all_pages);

        $temp_storage = array();
        $this->internal_get_pagetree_menulevel( $all_pages, $temp_storage);

        foreach($temp_storage as $key => $value)
		{
			$values[ $MENU['PAGES'] ][ $key ] = $value;
		}
            
        /**
         *    second: add tools
         *
         */
        $aTemp = [];
        $this->db->execute_query(
            "SELECT `name`,`directory` from `".TABLE_PREFIX."addons` where `function`='tool' order by `name`",
            true,
            $aTemp,
            true
        );
        
        foreach( $aTemp as $data)
        {
            $values[ $MENU['ADMINTOOLS'] ][ $data['name'] ] = "admintools/tool.php?tool=".$data['directory'];
        }
        
        /**
         *    At last the backend-pages
         *
         */
        $values['Backend'] = &$this->backend_pages;
        $options = array(
            'name' => $name,
            'class' => "init_page_select"
        );
        
        return $this->internalBuildSelect($options, $values, $selected);
    }

    private function internalBuildSelect(&$options, &$values, &$selected) {
        $s = "<select ".$this->internalBuildArgs($options).">\n";
        foreach( $values as $theme=>$sublist ) 
		{
            $s .= "<optgroup label='".$theme."'>";
            foreach($sublist as $item=>$val) {
                $sel = ($val == $selected) ? " selected='selected'" : "";
                $s .= "<option value='".$val."'".$sel.">".$item."</option>\n";
            }
            $s .= "</optgroup>";
        }
        $s.= "</select>\n";
        return $s;
    }

    private function internalBuildArgs(&$aArgs) {
        $s = "";
        foreach($aArgs as $name=>$value)
        {
            $s .= " ".$name."='".$value."'";
        }
        return $s;
    }

    public function get_user_info( &$aUserId=0 ) {
        $aUser = array();
        $this->db->execute_query(
            "SELECT `init_page`, `page_param` from `".$this->table."` where `user_id`='".$aUserId."'",
            true,
            $aUser,
            false
        );

        if (count($aUser) == 0)
        {
            if($aUserId > 0)
            {
                $this->db->simple_query("INSERT into `".$this->table."` (`user_id`, `init_page`,`page_param`) VALUES ('".$aUserId."', 'start/index.php', '')");
            }
                
            return array('init_page' => "start/index.php", 'page_param' => '') ;
            
        }
        else
        {
            return $aUser;
        }
    }

    /**
     *  Update the user entries
     *
     *  @param  integer $iUID   A valid user ID. (pass by reference)
     *  @param  integer $iValue A valid pageID. (pass by reference)
     *  @param  string  $sParam A valid string for additional parameters. (pass by reference)
     */
    public function update_user( &$iUID, &$iValue, &$sParam = "" )
    {
        if(!is_string($sParam))
        {
            $sParam = "";
        }
        
        $aFields = [
            "page_param" => $sParam,
            "init_page"  => $iValue
        ];
        
        $this->db->build_and_execute(
            "update",
            $this->table,
            $aFields,
            "user_id=".$iUID
        );
    }

    private function internalTestUser( $aID, $path_ref ) {
        $info = $this->get_user_info( $aID );
        $path = ADMIN_URL."/".$info['init_page'];
        if (( $path <> $path_ref ) && ($info['init_page'] != "start/index.php" ) && ($info['init_page'] != "") )
        {
            if (strlen($info['page_param']) > 0)
            {
                $path .= str_replace("&amp;", "&", $info['page_param']);
            }
            
            $this->internalAddLeptoken( $path );

            if(true === $this->testPath($path))
            {
                header('Location: '.$path );
                die();
            }
        }
    }

    private function internalAddLeptoken( &$aURL ) {
        if (isset($_GET['leptoken'])) {
            $temp_test = explode("?", $aURL );
            $aURL .= (count($temp_test) == 1) ? "?" : "&amp;";
            
            $aURL .= "leptoken=".$_GET['leptoken'];
        }
    }

    public function get_single_user_select (
        $aUserId,
        $aName,
        $selected="", 
        &$options=array(
            'backend_pages'=>true,
            'tools' => true,
            'pages' => true
            )
        ) {

        global $MENU;
        
        $values = Array();
        
        if (array_key_exists('backend_pages', $options) && ($options['backend_pages'] === true))
        {
            $values['Backend'] = $this->backend_pages;
        }

        /**
         *    Add tools
         *
         */
        if (array_key_exists('tools', $options) && ($options['tools'] === true))
        {
            $temp = [];
            $this->db->execute_query(
                "SELECT `name`,`directory` from `".TABLE_PREFIX."addons` where `function`='tool' order by `name`",
                true,
                $temp,
                true
            );
            
            foreach($temp as $data)
            {
                $values[ $MENU['ADMINTOOLS'] ][ $data['name'] ] = "admintools/tool.php?tool=".$data['directory'];
            }
        }

        /**
         *    Add pages
         *
         */
        if (array_key_exists('pages', $options) && ($options['pages'] === true))
        {
            require_once( LEPTON_PATH."/framework/functions/function.page_tree.php" );
            $all_pages = array();
            page_tree(0, $all_pages);

            $temp_storage = array();
            $this->internal_get_pagetree_menulevel( $all_pages, $temp_storage);

            foreach($temp_storage as $key => $value)
            {
                $values[ $MENU['PAGES'] ][ $key ] = $value;
            }
        }
        
        $options = array(
            'name' => $aName,
            'class' => "init_page_select"
        );
        
        return $this->internalBuildSelect($options, $values, $selected);
    }

    /**
     *    Internal private function for the correct displax of the page(-tree)
     *
     *    @param    array    Array within the pages. (Pass by reference)
     *    @param    array    A storage array for the result. (Pass by Reference)
     *    @param    int        Counter for the recursion deep, correspondence with the menu-level of the page(-s)
     *
     */
    private function internal_get_pagetree_menulevel( &$all, &$storage = array(), $deep = 0 ){
        //    Menu-data is empty, nothing to do
        if(count($all) == 0) 
		{
			return false;
		}

        //    Recursions are more than 50 ... break
        if($deep > 50)
        {
            return false;
        }

        //    Build the 'select-(menu)title prefix
        $prefix = "";
        for($i=0;$i<$deep; $i++) 
		{
			$prefix .= "-";
		}
		
        if($deep > 0) 
		{
			$prefix .= " ";
		}
        
        foreach($all as $ref) 
		{
            $storage[ $prefix.$ref['page_title'] ] = "pages/modify.php?page_id=".$ref['page_id'];

            // Recursive call for the subpages
            $this->internal_get_pagetree_menulevel( $ref['subpages'], $storage, $deep+1 );
        }
        
        return true;
    } 

    /**
     *    Protected function to test the users and delete entries for    
     *    not existing ones.
     *
     */
    protected function internalTestEntries () {
        
        $aResult= [];
        $this->db->execute_query(
            "SELECT `user_id` from `".TABLE_PREFIX."users` order by `user_id`",
            true,
            $aResult,
            true
        );
        if (count($aResult) > 0) {
            $ids = array();
            foreach( $aResult as $data )
            {
                $ids[] = $data['user_id'];
            }

            $this->db->simple_query( "DELETE from `".TABLE_PREFIX."mod_initial_page` where `user_id` not in (".implode (",",$ids).")" );
        }
    }

    public function get_language() {
        return $this->language;
    }

    /**
     *  Does the target page exists, or does the traget admin tool exists?
     *
     *  @param  string $sPath A path/link to the target backend page.
     *  @return bool
     *
     */
    private function testPath( $sPath )
    {
        // edit page?
        $matches = array();
        if( 1 === preg_match( "/modify.php\?page_id=([0-9]*)/", $sPath, $matches ) )
        {
            $iLookupPageID = intval($matches[1]);
            return ( NULL != $this->db->get_one("SELECT `page_title` FROM `".TABLE_PREFIX."pages` WHERE `page_id` =".$iLookupPageID ) );
            
        } else {
            // admin tool?
            if( 1 === preg_match( "/admintools\/tool\.php\?tool=([a-z_0-9]*)/", $sPath, $matches ) )
            {
                $sAdminToolName = $matches[1];
                return ( NULL != $this->db->get_one("SELECT `name` FROM `".TABLE_PREFIX."addons` WHERE `directory` ='".$sAdminToolName."' AND `function`='tool' " ) );
            }
            else {
                return true;
            }
        }
    }
}
