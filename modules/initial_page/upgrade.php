<?php

/**
 *
 * @module          initial_page
 * @author          LEPTON project 
 * @copyright       2010-2021 LEPTON Project 
 * @link            https://lepton-cms.org
 * @license         copyright, all rights reserved
 * @license_terms   please see info.php of this module
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// -------------------------------------------
// delete obsolete files in current version
$file_names = array(
	'/modules/initial_page/templates/interface.lte',		// replaced by tool.lte
	'/templates/lepsem/backend/initial_page/backend.css',	// moved to css folder
	'/modules/initial_page/backend.css',					// old: moved to css folder
	'/modules/initial_page/classes/class.init_page.php',	// no longer needed
	'/modules/initial_page/classes/class.patch.php'			// no longer needed
);

LEPTON_handle::delete_obsolete_files($file_names);

$dir_names = array(
	 '/templates/lepsem/backend/initial_page'				// no longer required
);
LEPTON_handle::delete_obsolete_directories($dir_names);

?>
