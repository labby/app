<?php

/**
 * @module          App-Articles
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_articles_frontend extends LEPTON_abstract
{
	use app_admin_fe;
	
	public $all_articles = array();
	public $article_groups = array();
	public $active_article_groups = array();
	public $article_types = array();
	public $active_article_types = array();
	public $article_units = array();
	public $active_article_units = array();
	public $active_vat_keys = array();
	public $active_eu_vat_keys = array();
	public $supplier = array();
	
	public static $instance;
	

	public function initialize() 
	{
		// all vars from class app_admin_fe
		$this->get_all_vars();
		$this->fe_help = LEPTON_URL.'/modules/'.$this->module_directory.'/';		
	}
	
	
public function init_addon( $iPageId = -1, $iSectionId = -1, $iClientId = -99 )
	{
		$this->page_id = $iPageId;
		$this->section_id = $iSectionId;
		if($iClientId != -99)
		{
			$this->client_id = $iClientId;
		}
		else
		{
			die(LEPTON_tools::display($this->language['no_client'], 'pre','ui red message'));
		}
	
		//get array of all articles
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_articles WHERE client_id = ".$this->client_id." AND active = 1",
			true,
			$this->all_articles,
			true
		);
		
		//get array of article groups
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_article_group WHERE client_id = ".$this->client_id,
			true,
			$this->article_groups,
			true
		);
		
		//get array of active article groups
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_article_group WHERE client_id = ".$this->client_id." AND active = 1 ",
			true,
			$this->active_article_groups,
			true
		);		

		//get array of article types
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_article_type WHERE client_id = ".$this->client_id,
			true,
			$this->article_types,
			true
		);

		//get array of active article types
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_article_type WHERE client_id = ".$this->client_id." AND active = 1 ",
			true,
			$this->active_article_types,
			true
		);		
		
		//get array of article units
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_article_units WHERE client_id = ".$this->client_id,
			true,
			$this->article_units,
			true
		);

		//get array of active article units
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_article_units WHERE client_id = ".$this->client_id." AND active = 1 ",
			true,
			$this->active_article_units,
			true
		);
		
		//get array of active vat_keys
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_vat_key WHERE active = 1 AND vat_eu = 0 ",
			true,
			$this->active_vat_keys,
			true
		);
		
		//get array of active EU vat_keys
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_vat_key WHERE active = 1 AND vat_eu = 1 ",
			true,
			$this->active_eu_vat_keys,
			true
		);		

		//get array of active suppliers
		$secure_supplier_data = app_supplier_frontend::getInstance()->secure_supplier_data;
		$this->database->secure_execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_supplier WHERE client_id = ".$this->client_id." AND active = 1 ",
			true,
			$this->supplier,
			true,
			$secure_supplier_data
		);		
		
		//get group for summary
		foreach($this->all_articles as &$temp_group) {
			$temp_group['article_group'] = $this->database->get_one("SELECT name FROM ".TABLE_PREFIX."mod_app_article_group WHERE id = ".$temp_group['article_group']." ");
		}

		//get type for summary
		foreach($this->all_articles as &$temp_type) {
			$temp_group['article_type'] = $this->database->get_one("SELECT name FROM ".TABLE_PREFIX."mod_app_article_type WHERE id = ".$temp_group['article_type']." ");
		}			
	}
	
public function get_dataform()
	{
//echo(LEPTON_tools::display($_POST,'pre', 'ui blue message'));
		
		// force user to chose client
		if($this->client_id < 1 )
		{
			header("Refresh:0; url=".$this->client_url."");
		}


		 // display article group form
		if(isset($_POST['display_groups'])&& $_POST['display_groups'] == 2 ) 
		{
	
			// data for twig template engine	
			$data = array(
				'oAFE'	=> $this,
				'job'	=> $_POST['display_groups']
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_articles');
						
			echo $oTwig->render( 
				"@app_articles/article_groups.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}
		
		 // edit article group form
		if(isset($_POST['group_details'])&& $_POST['group_details'] != -1 ) 
		{				
			$group_id = intval($_POST['group_details']);
			$current_group = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_article_group WHERE id = ".$group_id,
				true,
				$current_group,
				false
			);
	
			// data for twig template engine	
			$data = array(
				'oAFE'	=> $this,
				'current_group'	=>$current_group
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_articles');
						
			echo $oTwig->render( 
				"@app_articles/edit_group.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}		
		
		 // copy group form
		if(isset($_POST['group_copy'])&& $_POST['group_copy'] != -1 ) 
		{
			$group_id = intval($_POST['group_copy']);
			$current_group = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_article_group WHERE id = ".$group_id,
				true,
				$current_group,
				false,
			);
			
			$current_group['id'] = NULL;
			$current_group['client_id'] = $current_group['client_id'];
			$current_group['section_id'] = $current_group['section_id'];
			$current_group['page_id'] = $current_group['page_id'];
			$current_group['name'] = $current_group['name'].'-Kopie';
			$current_group['active'] = 0;
			
			$this->database->build_and_execute(
				"INSERT",
				TABLE_PREFIX."mod_app_article_group",
				$current_group
			);

			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
			header("Refresh:0; url=".$this->fe_action."");	
			exit(0);					
		}		


		 // save group form
		if(isset($_POST['save_group'])&& $_POST['save_group'] != -1 ) 
		{
			$request = LEPTON_request::getInstance();			
			
			$group_id = intval($_POST['save_group']);	
			$_POST['active'] = (($_POST['active'] == 'on' || $_POST['active'] == 1)  ? 1 : 0); 			

			$all_names = array (
				'name'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'active'		=> array ('type' => 'integer', 'default' => 0)
			);
			
			$all_values = $request->testPostValues($all_names);
			$table = TABLE_PREFIX."mod_app_article_group";
			
			$this->database->build_and_execute(
				'UPDATE', 
				$table, 
				$all_values, 
				'id ='.$group_id,
			);

			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
			header("Refresh:0; url=".$this->fe_action."");	
			exit(0);					
		}
		

		 // display article type form
		if(isset($_POST['display_types'])&& $_POST['display_types'] == 3 ) 
		{	
	
			// data for twig template engine	
			$data = array(
				'oAFE'	=> $this,
				'job'	=> $_POST['display_types']
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_articles');
						
			echo $oTwig->render( 
				"@app_articles/article_types.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}
			
		
		 // edit article type form
		if(isset($_POST['type_details'])&& $_POST['type_details'] != -1 ) 
		{				
			$type_id = intval($_POST['type_details']);
			$current_type = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_article_type WHERE id = ".$type_id,
				true,
				$current_type,
				false
			);
	
			// data for twig template engine	
			$data = array(
				'oAFE'	=> $this,
				'current_type'	=>$current_type
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_articles');
						
			echo $oTwig->render( 
				"@app_articles/edit_type.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}

		 // copy type form
		if(isset($_POST['type_copy'])&& $_POST['type_copy'] != -1 ) 
		{
			$group_id = intval($_POST['type_copy']);
			$current_type = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_article_type WHERE id = ".$group_id,
				true,
				$current_type,
				false,
			);
			
			$current_type['id'] = NULL;
			$current_type['client_id'] = $current_type['client_id'];
			$current_type['section_id'] = $current_type['section_id'];
			$current_type['page_id'] = $current_type['page_id'];
			$current_type['name'] = $current_type['name'].'-Kopie';
			$current_type['active'] = 0;
			
			$this->database->build_and_execute(
				"INSERT",
				TABLE_PREFIX."mod_app_article_type",
				$current_type
			);

			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
			header("Refresh:0; url=".$this->fe_action."");
			exit(0);		
		}

		 // save type form
		if(isset($_POST['save_type'])&& $_POST['save_type'] != -1 ) 
		{
			$request = LEPTON_request::getInstance();			
			
			$group_id = intval($_POST['save_type']);	
			$_POST['active'] = (($_POST['active'] == 'on' || $_POST['active'] == 1)  ? 1 : 0); 			

			$all_names = array (
				'name'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'active'=> array ('type' => 'integer', 'default' => 0)
			);
			
			$all_values = $request->testPostValues($all_names);
			$table = TABLE_PREFIX."mod_app_article_type";
			
			$this->database->build_and_execute(
				'UPDATE', 
				$table, 
				$all_values, 
				'id ='.$group_id,
			);

			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
			header("Refresh:0; url=".$this->fe_action."");	
			exit(0);					
		}

		
		 // edit article
		if(isset($_POST['article_details'])&& $_POST['article_details'] != -1 ) 
		{				
			$article_id = intval($_POST['article_details']);
			$current_article = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_articles WHERE id = ".$article_id,
				true,
				$current_article,
				false
			);
			
			$secure_account_data = app_accounts_frontend::getInstance()->secure_account_data;
			$accounts_in = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_accounts WHERE client_id = ".$current_article['client_id']." AND account_type = 6 AND active = 1 ",
				true,
				$accounts_in,
				true,
				$secure_account_data
			);

			$accounts_out = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_accounts WHERE client_id = ".$current_article['client_id']." AND account_type = 5 AND eu_relation = -99 AND active = 1 ",
				true,
				$accounts_out,
				true,
				$secure_account_data
			);

			$accounts_out_eu = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_accounts WHERE client_id = ".$current_article['client_id']." AND account_type = 5 AND eu_relation = 1 AND active = 1 ",
				true,
				$accounts_out_eu,
				true,
				$secure_account_data
			);				

			// data for twig template engine	
			$data = array(
				'oAFE'				=> $this,
				'current_article'	=> $current_article,
				'accounts_in'		=> $accounts_in,
				'accounts_out_eu'	=> $accounts_out_eu,
				'accounts_out'		=> $accounts_out
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_articles');
						
			echo $oTwig->render( 
				"@app_articles/edit_article.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}

		 // edit article
		if(isset($_POST['save_article'])&& $_POST['save_article'] != -1 ) 
		{	
			$request = LEPTON_request::getInstance();			
			
			$article_id = intval($_POST['save_article']);
			$_POST['last_modified'] = $this->timestamp;
			$_POST['active'] = (($_POST['active'] == 'on' || $_POST['active'] == 1)  ? 1 : 0);

			$all_names = array (
				'client_id'=> array ('type' => 'integer', 'default' => "-99", 'range' =>""),
				'section_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'page_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'article_type'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'article_group'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'article_no'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),				
				'article_name'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'article_unit'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),				
				'in_description'=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'in_netto'		=> array ('type' => 'decimal', 'default' => -1),
				'in_brutto'		=> array ('type' => 'decimal', 'default' => -1),
				'in_vat_key'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'in_account_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'in_supplier_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'out_description'=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'out_netto'		=> array ('type' => 'decimal', 'default' => -1),
				'out_brutto'	=> array ('type' => 'decimal', 'default' => -1),
				'out_vat_key'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'out_account_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'eu_vat_key'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'eu_account_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'last_modified'	=> array ('type' => 'datetime', 'default' => "0000-00-00 00:00:00", 'range' =>""),
				'active'		=> array ('type' => 'integer', 'default' => 0)
			);
			
			$all_values = $request->testPostValues($all_names);
			$table = TABLE_PREFIX."mod_app_articles";
//die(LEPTON_tools::display($all_values,'pre', 'ui blue message'));				
			$this->database->build_and_execute(
				'UPDATE', 
				$table, 
				$all_values, 
				'id ='.$article_id,
			);

			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
			header("Refresh:0; url=".$this->fe_action."");	
			exit(0);	
	
	
		}


		
		// display standard page
		// data for twig template engine
		
		$data = array(
			'oAFE'	=> $this,
			'job'	=> 1
		);
		//	get the template-engine.
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('app_articles');
					
		echo $oTwig->render( 
			"@app_articles/summary.lte",	//	template-filename
			$data						//	template-data
		);

		return 0;
	}	
 } // end class
