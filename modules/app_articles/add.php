<?php

/**
 * @module          App-Articles
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

// this addon can only be used on 1 page in one section
$double = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."sections` WHERE module = 'app_articles'",
	true,
	$double,
	true
);			
			
if(count($double) > 1 ) {
	echo (LEPTON_tools::display('This addon can only be used on one page and one section!','pre','ui red message'));
	
	// get last inserted section
	$to_delete = $database->get_one("SELECT LAST_INSERT_ID() FROM `".TABLE_PREFIX."sections`");
	
	$database->simple_query ("DELETE FROM `".TABLE_PREFIX."sections` WHERE `section_id` = ".$to_delete." ");
	exit();
}
?>