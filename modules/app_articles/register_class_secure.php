<?php

/**
 * @module          App-Articles
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 

$files_to_register = array(
	'help.php',
);

LEPTON_secure::getInstance()->accessFiles($files_to_register);

?>