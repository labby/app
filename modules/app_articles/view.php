<?php

/**
 * @module          App-Articles
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php
$debug = true;

if (true === $debug) {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}

$client_id = $_SESSION['current_client'] ?? -1;
$oAFE = app_articles_frontend::getInstance();
$oAFE->init_addon($page_id, $section_id,$client_id );

$oAFE->get_dataform();

 ?>