<?php

/**
 * @module          App-Articles
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `client_id` int(11) NOT NULL DEFAULT '-1',
	  `section_id` int(11) NOT NULL DEFAULT '-1',
	  `page_id` int(11) NOT NULL DEFAULT '-1',
	  `article_type` int(11) NOT NULL DEFAULT '-1',
	  `article_group` int(11) NOT NULL DEFAULT '-1',
	  `article_no` int(11) NOT NULL DEFAULT '-1',
	  `article_name` varchar(255) NOT NULL DEFAULT '',
	  `article_unit` int(11) NOT NULL DEFAULT '-1',
	  `in_description` varchar(255) NOT NULL DEFAULT '',
	  `in_netto` decimal(15,2) NOT NULL DEFAULT '-1.00',
	  `in_brutto` decimal(15,2) NOT NULL DEFAULT '-1.00',
	  `in_vat_key` int(11) NOT NULL DEFAULT '-1',
	  `in_account_id` int(11) NOT NULL DEFAULT '-1',
	  `in_supplier_id` int(11) NOT NULL DEFAULT '-1',
	  `out_description` varchar(255) NOT NULL DEFAULT '',
	  `out_netto` decimal(15,2) NOT NULL DEFAULT '-1.00',
	  `out_brutto` decimal(15,2) NOT NULL DEFAULT '-1.00',
	  `out_vat_key` int(11) NOT NULL DEFAULT '-1',
	  `out_account_id` int(11) NOT NULL DEFAULT '-1',
	  `eu_vat_key` int(11) NOT NULL DEFAULT '-1',
	  `eu_account_id` int(11) NOT NULL DEFAULT '-1',
	  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `article_no` (`article_no`)
	";
LEPTON_handle::install_table("mod_app_articles", $table_fields);

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `client_id` int(11) NOT NULL DEFAULT -1,
	  `section_id` int(11) NOT NULL DEFAULT -1,
	  `page_id` int(11) NOT NULL DEFAULT -1,
	  `name` varchar(255) NOT NULL DEFAULT '',
	  `active` int NOT NULL DEFAULT '1',	  
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_article_type", $table_fields);

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `client_id` int(11) NOT NULL DEFAULT -1,
	  `section_id` int(11) NOT NULL DEFAULT -1,
	  `page_id` int(11) NOT NULL DEFAULT -1,
	  `name` varchar(255) NOT NULL DEFAULT '',
	  `active` int NOT NULL DEFAULT '1',	  
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_article_group", $table_fields);

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `client_id` int(11) NOT NULL DEFAULT -1,
	  `section_id` int(11) NOT NULL DEFAULT -1,
	  `page_id` int(11) NOT NULL DEFAULT -1,
	  `name` varchar(255) NOT NULL DEFAULT '',
	  `name_short` varchar(255) NOT NULL DEFAULT '',
	  `active` int NOT NULL DEFAULT '1',	  
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_article_units", $table_fields);

?>