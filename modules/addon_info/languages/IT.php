<?php

/**
 *	@module			Addon Info
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2017-2021 cms-lab
 *	@link			http://www.cms-lab.com
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 */

 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 13:48, 25-06-2020
 * translated from..: EN
 * translated to....: IT
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_ADDON_INFO	= array(
	"sort_alphabet"		=> "In ordine alfabetico",
	"sort_updated"		=> "Ultimo aggiornamento"
);


?>
