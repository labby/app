<?php

/**
 *	@module			addon_info
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2017-2021 cms-lab
 *	@license		GNU General Public License
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// -------------------------------------------
// delete obsolete files in current version
$file_names = array(
	'/modules/addon_info/templates/display.lte',		// replaced by tool.lte
	'/modules/addon_info/templates/info.lte',			// replaced by global showmodinfo.lte
	'/templates/talgos/backend/addon_info/display.lte',	// replaced by tool.lte
	'/templates/talgos/backend/addon_info/info.lte'		// replaced by global showmodinfo.lte
);

LEPTON_handle::delete_obsolete_files($file_names);

?>
