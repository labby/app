{#
/**
 * @module          App-Customer
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 #}
 
<!-- start twig content -->
{% autoescape false %}

<div class="ui {{ oAFE.addon_color }} segment">
	<div class="ui basic segment">
		<div class="ui warning message">
			<div class="header">{{ oAFE.language.info_1 }}</div>
		</div>
		<div class="spacer2"></div>
		<form class="ui form" action="{{ oAFE.fe_action }}" method="post">
			{% for item, value in current_customer %}
				<input type="hidden" name="{{ item }}" value="{{ value }}" />
			{% endfor %}

			<div class="field">
				<label class="required">{{ oAFE.language.bank_name }}</label>
				<input name="bank_name" type="text" required value="{{ current_customer.bank_name }}">
			</div>
			
			<div class="two fields">
				<div class="field">
					<label class="required">{{ oAFE.language.iban }}</label>
					<input name="iban" type="text" required value="{{ current_customer.iban }}">
				</div>
				<div class="field">
					<label class="required">{{ oAFE.language.bic }}</label>
					<input name="bic" type="text" required value="{{ current_customer.bic }}">
				</div>				
			</div>		
		
			<div class="spacer2"></div>
			<h5 class="ui dividing header">{{ oAFE.language.additional_values }}</h5>
			<div class="two fields">
				<div class="field">
					<label class="required">{{ oAFE.language.account_no }}</label>
					<input {% if -99 != current_customer.account_no %} disabled{% endif %} name="account_no" type="text" required value="{{ current_customer.account_no }}">
				</div>
				<div class="field">
					<label class="required">{{ oAFE.language.invoice_type }}</label>
					<select name="invoice_type" class="ui fluid dropdown">						
						<option value ="-1">{{ oAFE.language.chose }}</option>	
						{% for item in oAFE.invoices %}
							<option value ="{{ item }}" {% if current_customer.invoice_type == item %} selected="selected"{% endif %}>{{ item }}</option>
						{% endfor %}
					</select>					
				</div>				
			</div>	
			
			<div class="spacer2"></div>
			
			<div class="two fields">
				<div class="field">
					<label>{{ oAFE.language.sepa }} </label>
					<select name="sepa" class="ui fluid dropdown">						
						<option value ="-1">{{ oAFE.language.chose }}</option>	
						<option value ="0" {% if current_customer.sepa == 0 %} selected="selected"{% endif %}>{{ TEXT.NO }}</option>
						<option value ="1" {% if current_customer.sepa == 1 %} selected="selected"{% endif %}>{{ TEXT.YES }}</option>
					</select>
				</div>

				<div class="field">
					<label>{{ oAFE.language.send_invoice }}</label>
					<select name="send_invoice" class="ui fluid dropdown">						
						<option value ="-1">{{ oAFE.language.chose }}</option>	
						<option value ="0" {% if current_customer.send_invoice == 0 %} selected="selected"{% endif %}>{{ TEXT.NO }}</option>
						<option value ="1" {% if current_customer.send_invoice == 1 %} selected="selected"{% endif %}>{{ TEXT.YES }}</option>
					</select>
				</div>									
			</div>	
						
			<div class="spacer2"></div>
			
			<div class="two fields">
				<div class="field">
					<label>{{ oAFE.language.terms_payment }} </label>
					<select name="payment_terms" class="ui fluid dropdown">						
						<option value ="-1">{{ oAFE.language.chose }}</option>	
						{% for item in oAFE.payment_terms %}
							<option value ="{{ item.id }}" {% if current_customer.payment_terms == item.id %} selected="selected"{% endif %}>{{ item.text }}</option>
						{% endfor %}
					</select>
				</div>

				<div class="field">
					<label>{{ oAFE.language.terms_price }}</label>
					<select name="price_terms" class="ui fluid dropdown">						
						<option value ="-1">{{ oAFE.language.chose }}</option>	
						<option value ="0" {% if current_customer.price_terms == 0 %} selected="selected"{% endif %}>{{ oAFE.language.terms_price_0 }}</option>
						<option value ="1" {% if current_customer.price_terms == 1 %} selected="selected"{% endif %}>{{ oAFE.language.terms_price_1 }}</option>
					</select>
				</div>									
			</div>				
			
				
			
			<div class="spacer2"></div>		
			<button class="ui positive button" type="submit" name="save_customer_data" value="{{ current_customer.id }}"><i class="save icon"></i>{{ TEXT.SAVE }}</button>
			<a href="{{ oAFE.fe_action }}" class="ui orange right floated button"><i class="reply icon"></i>{{ TEXT.CANCEL }}</a>
			<div class="spacer2"></div>
			
		</form>
		
	</div>
</div>	
{% endautoescape %}
<!-- end twig content -->