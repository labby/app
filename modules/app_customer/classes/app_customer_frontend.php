<?php

/**
 * @module          App-Customer
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_customer_frontend extends LEPTON_abstract
{
	use app_admin_fe;

	public $all_customer = array();
	public $payment_terms = array();
	public $invoices = array();

	public $secure_customer_data = array('company_short','company','extra_1','street','no','zip','city','country','fon','fax','email','internet','company_type','eu_trade','vat_id','bank_name','iban','bic');
	
	public static $instance;
	

	public function initialize() 
	{
		// all vars from class app_admin_fe
		$this->get_all_vars();
		$this->fe_help = LEPTON_URL.'/modules/'.$this->module_directory.'/';
		
		// get invoice templates
		$dir = LEPTON_PATH."/modules/app_admin/templates/invoice/";
		LEPTON_handle::register( "scan_current_dir" );
		$result = scan_current_dir( $dir, 'lte' );		
		$this->invoices = $result['filename'];
		
	}
	
	
public function init_addon( $iPageId = -1, $iSectionId = -1, $iClientId = -99 )
	{
		$this->page_id = $iPageId;
		$this->section_id = $iSectionId;
		if($iClientId != -99)
		{
			$this->client_id = $iClientId;
		}
		else
		{
			die(LEPTON_tools::display($this->language['no_client'], 'pre','ui red message'));
		}
	
		//get array of all customers
		$this->all_customer = array();
		$this->database->secure_execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_customer WHERE client_id = ".$this->client_id." AND active = 1",
			true,
			$this->all_customer,
			true,
			$this->secure_customer_data
		);
			
		//get array of payment terms
		$this->payment_terms = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_payment_terms WHERE active = 1",
			true,
			$this->payment_terms,
			true
		);			
	}
	
public function get_dataform()
	{
		// force user to chose client
		if($this->client_id < 1 )
		{
			header("Refresh:0; url=".$this->client_url."");
		}

		 // add customer form
		if(isset($_POST['add_customer'])&& $_POST['add_customer'] = -1 ) 
		{
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_customer');
						
			echo $oTwig->render( 
				"@app_customer/customer_add.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}	

		
		 // delete customer data
		if(isset($_POST['customer_delete']) && is_numeric($_POST['customer_delete']) ) 
		{	

			$customer_id = intval($_POST['customer_delete']);	
			$table = TABLE_PREFIX."mod_app_customer";		
			$result = $this->database->simple_query("UPDATE ".$table." SET active = 0, last_modified ='".$this->timestamp."' WHERE id = ".$customer_id."  ");			
	
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}	
			else
			{
				// write log entry
				$this->log_entry->insert_secure_entry( 'UPDATE',$this->language['data_id'].$this->language['customer_deleted'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);
			}
		}

		
		 // insert new customer data
		if(isset($_POST['save_new_customer'])&& is_numeric($_POST['save_new_customer']) ) 
		{	
			$last_customer_id = $this->database->get_one("SELECT LAST_INSERT_ID() FROM ".Table_PREFIX."mod_app_customer");
			$current_no = $this->database->get_one("SELECT account_no FROM ".Table_PREFIX."mod_app_customer WHERE id = ".$last_customer_id );
	
			$_POST['client_id'] = $this->client_id;	
			$_POST['company_del'] = $_POST['company'];
			$_POST['extra_1_del'] = $_POST['extra_1'];
			$_POST['street_del'] = $_POST['street'];
			$_POST['no_del'] = $_POST['no'];
			$_POST['zip_del'] = $_POST['zip'];
			$_POST['city_del'] = $_POST['city'];
			$_POST['country_del'] = $_POST['country'];
			$_POST['account_no'] = $current_no + 1 ;
			$_POST['last_modified'] = $this->timestamp;					
			$request = LEPTON_request::getInstance();	
			
			$all_names = array (
				'client_id'=> array ('type' => 'integer', 'default' => "-99", 'range' =>""),
				'section_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'page_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'company_short'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'extra_1'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'street'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'no'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'zip'		=> array ('type' => 'string_clean', 'default' => NULL),
				'city' 		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'country'	=> array ('type' => 'string_clean', 'default' => "Deutschland", 'range' =>""),
				'company_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'extra_1_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'street_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'no_del'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'zip_del'		=> array ('type' => 'string_clean', 'default' => NULL),
				'city_del' 		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'country_del'	=> array ('type' => 'string_clean', 'default' => "Deutschland", 'range' =>""),					
				'fon'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'email'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'internet'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_type'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'eu_trade'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'vat_id'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'account_no'	=> array ('type' => 'integer', 'default' => "-99", 'range' =>""),				
				'last_modified'	=> array ('type' => 'string_clean', 'default' => "0000-00-00 00:00:00", 'range' =>"")
			);
			
			$all_values = $request->testPostValues($all_names);	
			$table = TABLE_PREFIX."mod_app_customer";		
			$result = $this->database->secure_build_and_execute( 'INSERT', $table, $all_values, '', $this->secure_customer_data);			
						
			
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}	
			else
			{			
				// Create customer directory
				$customer_id = $this->database->get_one("SELECT LAST_INSERT_ID() FROM ".$table." ");
				$app_media_path = LEPTON_PATH.MEDIA_DIRECTORY.'/app_'.$this->random_value;
				$customer_media_path = $app_media_path.'/out/'.$this->language['media_client'].'_'.$this->client_id.'/'.$this->language['media_customer'].'_'.$customer_id;
				LEPTON_handle::register( "make_dir" );	
				make_dir($customer_media_path);
				
				// copy files to new directories first index.php
				$source = $app_media_path.'/index.php';
				$destination = $customer_media_path.'/index.php';
				copy($source, $destination);
							
				// copy files to new directories second htacces
				$source = $app_media_path.'/.htaccess';
				$destination = $customer_media_path.'/.htaccess';
				copy($source, $destination);
				
				// write log entry
				$this->log_entry->insert_secure_entry( 'INSERT',$this->language['data_id'].$this->language['customer_new'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);
			}
		}	
		
		
		 // save customer data
		if(isset($_POST['save_customer_data'])&& is_numeric($_POST['save_customer_data']) ) 
		{
		
			$current_customer_id = intval($_POST['save_customer_data']);
			$_POST['last_modified'] = $this->timestamp;
			if($_POST['account_no'] == -99)
			{
				$_POST['account_no'] = $this->basic_settings['customer_account'] + $current_customer_id;
			}
//die(LEPTON_tools::display($_POST, 'pre','ui blue message'));			
			$request = LEPTON_request::getInstance();	
			
			$all_names = array (
				'client_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'section_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'page_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'company_short'	=> array ('type' => 'string_clean', 'default' => "-1", 'range' =>""),
				'company'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'extra_1'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'vat_id'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'eu_trade'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_type'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'street'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'no'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'zip'		=> array ('type' => 'string_clean', 'default' => NULL),
				'city' 		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'country'	=> array ('type' => 'string_clean', 'default' => "Deutschland", 'range' =>""),
				'company_salutation'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_name1'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_name2'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'fon'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'fax'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'email'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'internet'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'extra_1_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'street_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'no_del'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'zip_del'		=> array ('type' => 'string_clean', 'default' => NULL),
				'city_del' 		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'country_del'	=> array ('type' => 'string_clean', 'default' => "Deutschland", 'range' =>""),				
				'bank_name'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'iban'			=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'bic'			=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'sepa'			=> array ('type' => 'integer', 'default' => "0", 'range' =>""),
				'account_no'	=> array ('type' => 'integer', 'default' => "-99", 'range' =>""),				
				'payment_terms'	=> array ('type' => 'string_clean', 'default' => "-1", 'range' =>""),
				'price_terms'	=> array ('type' => 'string_clean', 'default' => "-1", 'range' =>""),
				'send_invoice'	=> array ('type' => 'integer', 'default' => "0", 'range' =>""),	
				'invoice_type'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'last_modified'	=> array ('type' => 'string_clean', 'default' => "0000-00-00 00:00:00", 'range' =>"")
			);
			
			$all_values = $request->testPostValues($all_names);	
			$table = TABLE_PREFIX."mod_app_customer";		
			$result = $this->database->secure_build_and_execute( 'UPDATE', $table, $all_values, 'id ='.$current_customer_id, $this->secure_customer_data);			
						
			
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}
			else
			{
				// write log entry
				$this->log_entry->insert_secure_entry( 'UPDATE',$this->language['data_id'].$current_customer_id.' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);
			}
		}

		 // display customer details
		if(isset($_POST['customer_details'])&& is_numeric($_POST['customer_details']) ) 
		{
			$current_customer_id = intval($_POST['customer_details']);
			
			//get array of current_customer
			$current_customer = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_customer WHERE id = ".$current_customer_id." ",
				true,
				$current_customer,
				false,
				$this->secure_customer_data
			);	
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_customer'	=> $current_customer,
				
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_customer');
						
			echo $oTwig->render( 
				"@app_customer/customer_details.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}




		// display customer contact
		if(isset($_POST['customer_contact'])&& is_numeric($_POST['customer_contact']) ) 
		{
			$current_customer_id = intval($_POST['customer_contact']);
			
			//get array of current_customer
			$current_customer = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_customer WHERE id = ".$current_customer_id." ",
				true,
				$current_customer,
				false,
				$this->secure_customer_data
			);	
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_customer'	=> $current_customer,
				
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_customer');
						
			echo $oTwig->render( 
				"@app_customer/customer_contact.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}	

		// display customer delivery_address
		if(isset($_POST['customer_delivery'])&& is_numeric($_POST['customer_delivery']) ) 
		{
			$current_customer_id = intval($_POST['customer_delivery']);
			
			//get array of current_customer
			$current_customer = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_customer WHERE id = ".$current_customer_id." ",
				true,
				$current_customer,
				false,
				$this->secure_customer_data
			);	
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_customer'	=> $current_customer,
				
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_customer');
						
			echo $oTwig->render( 
				"@app_customer/customer_delivery.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}			
	
		 // display customer bank
		if(isset($_POST['customer_bank'])&& is_numeric($_POST['customer_bank']) ) 
		{
			$current_customer_id = intval($_POST['customer_bank']);
			
			//get array of current_customer
			$current_customer = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_customer WHERE id = ".$current_customer_id." ",
				true,
				$current_customer,
				false,
				$this->secure_customer_data
			);	
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_customer'	=> $current_customer,
				
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_customer');
						
			echo $oTwig->render( 
				"@app_customer/customer_bank.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}

		// display standard page
		// data for twig template engine	
		$item_active = 1;
		$data = array(
			'oAFE'	=> $this,
			'job'	=> $item_active			
		);
		//	get the template-engine.
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('app_customer');
					
		echo $oTwig->render( 
			"@app_customer/summary.lte",	//	template-filename
			$data						//	template-data
		);

		return 0;
	}
 } // end class
