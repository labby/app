<?php

/**
 * @module          App-Customer
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_customer extends LEPTON_abstract
{
	use app_admin_be;	

	public static $instance;

	public function initialize() 
	{
		// all vars from class app_admin_be
		$this->get_all_vars();
		$this->action_url = LEPTON_URL.'/modules/'.$this->module_directory.'/';		
		$this->init_page();
	}
	
	public function init_page( $iPageID = 0 )
	{

	}	
	
 } // end class