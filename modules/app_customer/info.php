<?php

/**
 * @module          App-Customer
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$module_directory     = 'app_customer';
$module_name          = 'App Customer';
$module_function      = 'page';
$module_version       = '1.0.0';
$module_platform      = '5.x';
$module_delete        =  false;
$module_author        = 'cms-lab';
$module_home          = 'https://os-app.org';
$module_guid          = '9d449353-de7b-45cc-bbd8-a216b370746d';
$module_license       = '<a href="https://os-app.org/free/license.php" target="_blank">Custom License</a>';
$module_license_terms = '<a href="https://os-app.org/free/license.php" target="_blank">see License</a>';
$module_description   = 'Part of complete App';


?>