<?php

/**
 * @module          App-Customer
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `client_id` int(11) NOT NULL DEFAULT -1,
	  `section_id` int(11) NOT NULL DEFAULT -1,
	  `page_id` int(11) NOT NULL DEFAULT -1,
	  `company_short` varchar(255) NOT NULL DEFAULT '',
	  `company` varchar(255) NOT NULL DEFAULT '',
	  `extra_1` varchar(255) NOT NULL DEFAULT '',
	  `vat_id` varchar(128) NOT NULL DEFAULT '',
	  `eu_trade` varchar(128) NOT NULL DEFAULT '',
	  `company_type` varchar(128) NOT NULL DEFAULT '',
	  
	  `street` varchar(255) NOT NULL DEFAULT '',
	  `no` varchar(32) NOT NULL DEFAULT '',
	  `zip` varchar(32) NOT NULL DEFAULT '',
	  `city` varchar(128) NOT NULL DEFAULT '',	  
	  `country` varchar(128) NOT NULL DEFAULT 'DEUTSCHLAND',
	  `company_salutation` varchar(64) NOT NULL DEFAULT '',
	  `company_name1` varchar(255) NOT NULL DEFAULT '',
	  `company_name2` varchar(255) NOT NULL DEFAULT '',
	  `fon` varchar(128) NOT NULL DEFAULT '',
	  `fax` varchar(128) NOT NULL DEFAULT '',
	  `email` varchar(128) NOT NULL DEFAULT '',
	  `internet` varchar(128) NOT NULL DEFAULT '',
	  
	  `company_del` varchar(255) NOT NULL DEFAULT '',
	  `extra_1_del` varchar(255) NOT NULL DEFAULT '',
	  `street_del` varchar(255) NOT NULL DEFAULT '',
	  `no_del` varchar(32) NOT NULL DEFAULT '',
	  `zip_del` varchar(32) NOT NULL DEFAULT '',
	  `city_del` varchar(128) NOT NULL DEFAULT '',	  
	  `country_del` varchar(128) NOT NULL DEFAULT '',	  
	  
	  `bank_name` varchar(128) NOT NULL DEFAULT '',
	  `iban` varchar(64) NOT NULL DEFAULT '',
	  `bic` varchar(32) NOT NULL DEFAULT '',
	  `sepa` int(11) NOT NULL DEFAULT 0,
	  
	  `account_no` int(11) NOT NULL DEFAULT -99,
	  `payment_terms` varchar(32) NOT NULL DEFAULT '',
	  `price_terms` varchar(32) NOT NULL DEFAULT '',
	  `send_invoice` int(11) NOT NULL DEFAULT 0,
	  `invoice_type` varchar(32) NOT NULL DEFAULT '',
	  
	  `last_modified` datetime NOT NULL DEFAULT'0000-00-00 00:00:00',
	  `active` int NOT NULL DEFAULT '1', 
	  PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_app_customer", $table_fields);


?>