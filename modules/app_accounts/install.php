<?php

/**
 * @module          App-Accounts
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `client_id` int(11) NOT NULL DEFAULT '-1',
	  `skr` int(11) NOT NULL DEFAULT '3',
	  `section_id` int(11) NOT NULL DEFAULT '-1',
	  `page_id` int(11) NOT NULL DEFAULT '-1',
	  `account_type` int(11) NOT NULL DEFAULT '-1',
	  `account_no` int(11) NOT NULL DEFAULT '-1',
	  `account_name` varchar(256) NOT NULL DEFAULT '',
	  `account_description` varchar(256) NOT NULL DEFAULT '',
	  `need_details` int(11) NOT NULL DEFAULT '-1',
	  `account_use` int(11) NOT NULL DEFAULT '-1' COMMENT 'display account_use table',
	  `automatic_account` int(11) NOT NULL DEFAULT '0' COMMENT 'Automatisches Konto für UST-Berechnung/EU-Einkauf',
	  `form_relation_1` int(11) NOT NULL DEFAULT '-1' COMMENT 'Formular Einnahme-Überschuss Rechnung',
	  `form_relation_2` int(11) NOT NULL DEFAULT '-1' COMMENT 'Formular Zusammenfassende Meldung',
	  `form_relation_3` int(11) NOT NULL DEFAULT '-1' COMMENT 'Formular Umsatzsteuererklärung',
	  `vat_key` int(11) NOT NULL DEFAULT '-99' COMMENT 'Umsatzsteuerschlüssel',
	  `eu_relation` int(11) NOT NULL DEFAULT '-99' COMMENT 'EU-Steuerschlüssel ja/nein',
	  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `account_no` (`account_no`)
";
LEPTON_handle::install_table("mod_app_accounts", $table_fields);


$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `account_id` int NOT NULL DEFAULT -1,     
	  `iban` varchar(256) NOT NULL DEFAULT 'DE',
	  `bic` varchar(256) NOT NULL DEFAULT '',
	  `owner` varchar(256) NOT NULL DEFAULT '',
	  `bank_name` varchar(256) NOT NULL DEFAULT '',
	  `active` int NOT NULL DEFAULT 1, 
	  PRIMARY KEY ( `id` )
";
LEPTON_handle::install_table("mod_app_accounts_details_iban", $table_fields);

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `account_id` int NOT NULL DEFAULT -1,     
	  `account_start` date NOT NULL DEFAULT'0000-00-00',
	  `account_amount` decimal(15,2) NOT NULL DEFAULT -1, 
	  `active` int NOT NULL DEFAULT 1, 
	  PRIMARY KEY ( `id` )
";
LEPTON_handle::install_table("mod_app_accounts_details_start", $table_fields);

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `action_type` varchar(256) NOT NULL DEFAULT '' COMMENT 'Zahlung, Überweisung, Buchung, etc',
	  `action_date` date NOT NULL DEFAULT '0000-00-00' COMMENT 'Tag der Buchung, NICHT Aktualisierung',
	  `account_no` int(11) NOT NULL DEFAULT '-1' COMMENT 'Konto-Nummer',
	  `account_no_contra` int(11) NOT NULL DEFAULT '-1' COMMENT 'Gegenkonto-Nummer',
	  `record_no` varchar(128) NOT NULL DEFAULT '' COMMENT 'Beleg-Nr.',
	  `purpose` varchar(256) NOT NULL DEFAULT '' COMMENT 'Verwendungszweck',
	  `debit` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT 'Account SOLL',
	  `credit` decimal(15,2) NOT NULL DEFAULT '0.00' COMMENT 'Account HABEN',
	  `note1` varchar(256) NOT NULL DEFAULT '',
	  `last modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Aktualisierung der Buchung',
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`id`)
";
LEPTON_handle::install_table("mod_app_accounts_booking", $table_fields);

?>