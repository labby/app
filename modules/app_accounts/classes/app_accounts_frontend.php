<?php

/**
 * @module          App-Accounts
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_accounts_frontend extends LEPTON_abstract
{
	use app_admin_fe;

	public $all_accounts = array();
	public $account_types = array();
	public $vat_keys = array();
	public $form_relation_1 = array();
	public $form_relation_2 = array();	
	public $form_relation_3 = array();		

	public $day = 0;
	public $secure_account_data = array('account_name','account_description');
	public $secure_iban_data = array('iban','bic','owner','bank_name');
	
	public static $instance;	

public function initialize() 
	{	
		// all vars from class app_admin_fe
		$this->get_all_vars();
		$this->fe_help = LEPTON_URL.'/modules/'.$this->module_directory.'/';		
		$this->day = date('d-m-Y',time());				
	}
	
	
public function init_addon( $iPageId = -1, $iSectionId = -1, $iClientId = -99 )
	{
		$this->page_id = $iPageId;
		$this->section_id = $iSectionId;
		if($iClientId != -99)
		{
			$this->client_id = $iClientId;
		}
		else
		{
			die(LEPTON_tools::display($this->language['no_client'], 'pre','ui red message'));
		}

	
		//get array of all accounts
		$this->database->secure_execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_accounts WHERE client_id = ".$this->client_id." ORDER BY account_no ",
			true,
			$this->all_accounts,
			true,
			$this->secure_account_data
		);

		//get array of all account_types
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_account_type WHERE active = 1 ",
			true,
			$this->account_types,
			true
		);
		


		//get array of active vat keys
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_vat_key WHERE active = 1 ",
			true,
			$this->vat_keys,
			true
		);

		//get array of active form_relations type 1
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_form_relation WHERE type = 1 OR type = -1 AND active = 1 ",
			true,
			$this->form_relation_1,
			true
		);

		//get array of active form_relations type 2
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_form_relation WHERE type = 2 OR type = -1 AND active = 1 ",
			true,
			$this->form_relation_2,
			true
		);

		//get array of active form_relations type 3
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_form_relation WHERE type = 3 OR type = -1 AND active = 1 ",
			true,
			$this->form_relation_3,
			true
		);		
	}
	
public function get_dataform()
	{
		// force user to chose client
		if($this->client_id < 1 )
		{
			header("Refresh:0; url=".$this->client_url."");
		}

		
		 // copy account
		if(isset($_POST['account_duplicate']) && is_numeric($_POST['account_duplicate']) ) 
		{	
			$account_id = intval($_POST['account_duplicate']);
			$current_account = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_accounts WHERE id = ".$account_id,
				true,
				$current_account,
				false,
				$this->secure_account_data
			);
			
			$current_account['id'] = NULL;
			$current_account['account_no'] = -1;
			$current_account['account_name'] = $current_account['account_name'].'-Kopie';
			$current_account['last_modified'] = $this->timestamp;
			$current_account['active'] = 0;
			
			$table = TABLE_PREFIX."mod_app_accounts";		
			
			$this->database->secure_build_and_execute(
				"INSERT",
				$table,
				$current_account,
				$this->secure_account_data
			);
			
			// write log entry
			$this->log_entry->insert_secure_entry( 'INSERT',$this->language['data_id'].$this->language['account_new'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );
						

			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
			header("Refresh:0; url=".$this->fe_action."");	
			die();		
		}
		
		
		// display account details
		if(isset($_POST['account_details'])&& $_POST['account_details'] > -1 ) 
		{
			$account_id = intval($_POST['account_details']);
			$current_account = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_accounts WHERE id = ".$account_id,
				true,
				$current_account,
				false,
				$this->secure_account_data
			);
			
			$current_details_iban =array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_accounts_details_iban WHERE account_id = ".$account_id,
				true,
				$current_details_iban,
				false,
				$this->secure_iban_data
			);

			$current_details_start =array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_accounts_details_start WHERE account_id = ".$account_id,
				true,
				$current_details_start,
				false
			);

			$current_account_use =array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_account_use WHERE account_type = ".$current_account['account_type'],
				true,
				$current_account_use,
				true
			);				

			$start = $this->database->get_one("SELECT account_id FROM ".TABLE_PREFIX."mod_app_accounts_details_start WHERE account_id = ".$account_id);
			$has_start = ($start == NULL ? 0 : 1); 
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_account' => $current_account,
				'current_account_use' => $current_account_use,
				'current_details_iban' => $current_details_iban,
				'current_details_start' => $current_details_start,
				'has_start' => $has_start
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_accounts');
						
			echo $oTwig->render( 
				"@app_accounts/account_details.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}	

		 // save all account details
		if(isset($_POST['save_account_data'])&& $_POST['save_account_data'] > -1 ) 
		{			
			$account_id = intval($_POST['save_account_data']);
	
			$request = LEPTON_request::getInstance();	
			
			$_POST['last_modified'] = $this->timestamp;
			$_POST['active'] = (($_POST['active'] == 'on' || $_POST['active'] == 1)  ? 1 : 0); 
			
			$all_names = array (
				'skr'			=> array ('type' => 'integer', 'default' => -1, 'range' =>"0-99"),	
				'account_type'	=> array ('type' => 'integer', 'default' => -1),
				'account_no'	=> array ('type' => 'integer', 'default' => -1),
				'account_name'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'account_description'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'need_details'	=> array ('type' => 'integer', 'default' => -1),
				'account_use'	=> array ('type' => 'integer', 'default' => -1),
				'vat_relation_no'	=> array ('type' => 'integer', 'default' => -1),
				'form_relation_1'=> array ('type' => 'integer', 'default' => -1),
				'form_relation_2'=> array ('type' => 'integer', 'default' => -1),
				'form_relation_3'=> array ('type' => 'integer', 'default' => -1),
				'vat_key'		=> array ('type' => 'integer', 'default' => -1),
				'eu_relation'	=> array ('type' => 'integer', 'default' => -1),
				'last_modified'	=> array ('type' => 'datetime', 'default' => "0000-00-00 00:00:00", 'range' =>""),
				'active'		=> array ('type' => 'integer', 'default' => 0)
			);
			
			$all_values = $request->testPostValues($all_names);
			$table = TABLE_PREFIX."mod_app_accounts";
			$this->database->secure_build_and_execute( 
				'UPDATE', 
				$table, 
				$all_values, 
				'id ='.$account_id,
				$this->secure_account_data
			);	
		
			// write log entry
			$this->log_entry->insert_secure_entry( 'UPDATE',$this->language['data_id'].$this->language['account_new'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );
				
			$_POST['account_id'] = 	$account_id;
			
			// only needed if "need_details" = 2
			if( 2 == $_POST['need_details'])
			{
				
				$all_names = array (
					'account_id'	=> array ('type' => 'integer', 'default' => -1),
					'iban'	=> array ('type' => 'string_clean', 'default' => "DE", 'range' =>""),
					'bic'	=> array ('type' => 'string_clean', 'default' => "DE", 'range' =>""),
					'owner'	=> array ('type' => 'string_clean', 'default' => "DE", 'range' =>""),
					'bank_name'	=> array ('type' => 'string_clean', 'default' => "DE", 'range' =>""),
					'active'		=> array ('type' => 'integer', 'default' => 1)
				);
				
				$all_values = $request->testPostValues($all_names);			
				$table = TABLE_PREFIX."mod_app_accounts_details_iban";
				// check if iban is set
				$has_bank = $this->database->get_one("SELECT account_id FROM ".TABLE_PREFIX."mod_app_accounts_details_iban WHERE account_id = ".$account_id);				
				if($has_bank == NULL)
				{
					$this->database->secure_build_and_execute( 
						'INSERT', 
						$table, 
						$all_values,
						'',
						$this->secure_iban_data
					);
					// write log entry
					$this->log_entry->insert_secure_entry( 'INSERT',$this->language['data_id'].$this->language['account_new'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );					
				}
				else
				{
					$this->database->secure_build_and_execute(  
						'UPDATE', 
						$table,
						$all_values, 
						'account_id ='.$account_id,
						$this->secure_iban_data
					);
					
					// write log entry
					$this->log_entry->insert_secure_entry( 'UPDATE',$this->language['data_id'].$this->language['account_new'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );					
				}


						
			}

			// only needed if "need_details" = 2 or = 1
			if( 2 == $_POST['need_details'] || 1 == $_POST['need_details'])
			{	
				$all_names = array (
					'account_id'	=> array ('type' => 'integer', 'default' => -1),
					'account_start'	=> array ('type' => 'date', 'default' => "0000-00-00", 'range' =>""),					
					'account_amount'=> array ('type' => 'decimal', 'default' => -1),
					'active'		=> array ('type' => 'integer', 'default' => 1)
				);
				
				$all_values = $request->testPostValues($all_names);
				$table = TABLE_PREFIX."mod_app_accounts_details_start";				
				// check if start amount is set
				$has_start = $this->database->get_one("SELECT account_id FROM ".TABLE_PREFIX."mod_app_accounts_details_start WHERE account_id = ".$account_id);						
				if($has_start == NULL)
				{					
					$this->database->build_and_execute(  
						'INSERT', 
						$table, 
						$all_values					
					);
					
					// write log entry
					$this->log_entry->insert_secure_entry( 'INSERT',$this->language['data_id'].$this->language['account_new'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );			
				}
			

			}
			
			// save = ok and forward to start screen
			echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
			header("Refresh:0; url=".$this->fe_action."");	
			die();			
		}			

		// display standard page
		// data for twig template engine	
		$item_active = 1;
		$data = array(
			'oAFE'	=> $this,
			'job'	=> $item_active			
		);
		//	get the template-engine.
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('app_accounts');
					
		echo $oTwig->render( 
			"@app_accounts/summary.lte",	//	template-filename
			$data						//	template-data
		);

		return 0;
	}
 } // end class
