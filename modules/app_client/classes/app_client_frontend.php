<?php

/**
 * @module          App-Client
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_client_frontend extends LEPTON_abstract
{
	use app_admin_fe;
	
	public $all_clients = array();

	public $secure_client_data = array('company','extra_1','street','no','zip','city','country','fon','fax','email','internet','company_type','legal_form','tax_number','vat_id','bank_name','iban','bic');	
	
	public static $instance;
	

	public function initialize() 
	{
		// all vars from class app_admin_fe
		$this->get_all_vars();
		$this->fe_help = LEPTON_URL.'/modules/'.$this->module_directory.'/';
	}
	
	
public function init_addon( $iPageId = -1, $iSectionId = -1, $iClientId = -99 )
	{
		$this->page_id = $iPageId;
		$this->section_id = $iSectionId;
		if($iClientId != -99)
		{
			$this->client_id = $iClientId;
		}
		else
		{
			die(LEPTON_tools::display($this->language['no_client'], 'pre','ui red message'));
		}		

		//get array of client data
		$this->all_clients = array();
		$this->database->secure_execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_basic ",
			true,
			$this->all_clients,
			true,
			$this->secure_client_data
		);			
	}
	
public function get_dataform()
	{
		
		 // save client data
		if(isset($_POST['save_client'])&& $_POST['save_client'] == -98 ) 
		{
			$client_id = intval($_POST['client_id']);
			
			if($client_id == -1)
			{
				header("Refresh:0; url=".$this->fe_action."");
			}
			else
			{
				$_SESSION['current_client'] = $client_id;
				// write log entry
				$this->log_entry->insert_secure_entry( 'SET',$this->language['set_session'].$client_id.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);				
			}
		}
		
		
		 // add new client
		if(isset($_POST['new_client_data'])&& $_POST['new_client_data'] == -98 ) 
		{	
			$random_value = $this->database->get_one("SELECT random FROM ".TABLE_PREFIX."mod_app_basic ");	
	
			$data = array(
				'oAFE'	=> $this,
				'random' => $random_value
			);
			
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_client');
						
			echo $oTwig->render( 
				"@app_client/client.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;
		}			
		
		 // save new client
		if(isset($_POST['save_new_client']) && $_POST['save_new_client'] == -98 ) 
		{
			$client_id = intval($_POST['save_new_client']);
			
			if($client_id == -1)
			{
				header("Refresh:0; url=".$this->fe_action."");
			}
			else
			{
				$client_id = NULL;
				$_POST['page_id'] = $this->page_id;
				$_POST['section_id'] = $this->section_id;
				$_POST['last_modified'] = $this->timestamp;	
				$request = LEPTON_request::getInstance();	
				
				$all_names = array (
					'section_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
					'page_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
					'random'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
					'company'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
					'company_type'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
					'legal_form'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
					'tax_number'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
					'vat_id'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
					'last_modified'	=> array ('type' => 'string_clean', 'default' => "0000-00-00 00:00:00", 'range' =>"")
				);
				
				$all_values = $request->testPostValues($all_names);	
				$table = TABLE_PREFIX."mod_app_basic";		
				$result = $this->database->secure_build_and_execute( 'INSERT', $table, $all_values, '', $this->secure_client_data);			
							
				
				if($result == false) {
					echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
				}
				else
				{
					$client_id = $database->get_one("SELECT LAST_INSERT_ID() FROM ".TABLE_PREFIX."mod_app_basic ");
					// write log entry
					$this->log_entry->insert_secure_entry( 'INSERT',$this->language['added_client'].' '.$client_id .', '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );
					
					// insert new client in settings table
					$result = $database->simple_query ("INSERT INTO ".TABLE_PREFIX."mod_app_basic_settings (client_id, section_id, page_id, last_modified) Values(".$client_id.", ".$this->section_id.", ".$this->page_id.", ".$this->timestamp.")");					
					
					if($result == false) {
						echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
					}
					
					// write log entry
					$this->log_entry->insert_secure_entry( 'INSERT',$this->language['data_id'].$client_id.' '.$this->language['data_table'].TABLE_PREFIX.'mod_app_basic_settings'.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );
					
					// save = ok and forward to start screen
					echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
					header("Refresh:0; url=".$this->fe_action."");	
					exit(0);
				}
			}				
		}
		
		
		// display standard page
		// data for twig template engine	
		$current_client = array();
		$this->database->secure_execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_basic WHERE client_id = ".$this->client_id." ",
			true,
			$current_client,
			false,
			$this->secure_client_data
		);			

		$data = array(
			'oAFE'				=> $this,
			'client'			=> $this->client_id,
			'current_client'	=> $current_client
		);
		
		//	get the template-engine.
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('app_client');
					
		echo $oTwig->render( 
			"@app_client/view.lte",	//	template-filename
			$data						//	template-data
		);

		return 0;
	}
 } // end class
