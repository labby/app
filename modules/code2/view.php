<?php

/**
 *  @module         code2
 *  @version        see info.php of this module
 *  @authors        Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @copyright      2010-2021 Ryan Djurovich, Chio Maisriml, Thomas Hornik, Dietrich Roland Pehlke
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */
 
 // include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php
 
/**
 *	Get content
 */
$fetch_content = array();
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_code2 WHERE section_id = '".$section_id."' ",
	true,
	$fetch_content,
	false
);
$whatis = (int)($fetch_content['whatis'] % 10);

$content = $fetch_content['content'];

$oLEPTON = LEPTON_frontend::getInstance();

switch ($whatis) {

	case 0:	// PHP 
		eval($content);
		break;

	case 1:	// HTML
		$oLEPTON->preprocess($content);
		echo $content;
		break;

	case 2:	// JS
		echo "\n<script type=\"text/javascript\">\n<!--\n".$content."\n// -->\n</script>\n";
		break;

	case 3:
	case 4:
		echo "";	// Keine Ausgabe: Kommentar
		break;
		
	default:
	    echo "[c2] Unknown type!";
	    break;
}
