<?php

/**
 *  @module         TinyMCE
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2012-2022 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        GNU General Public License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *  Please note: TINYMCE is distibuted under the <a href="https://opensource.org/licenses/LGPL-2.1">GNU LGPL License 2.1</a> 
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$module_directory     = 'tinymce';
$module_name          = 'TinyMCE';
$module_function      = 'WYSIWYG';
$module_version       = '5.10.3.0';
$module_platform      = '5.x';
$module_author        = 'erpe, Aldus';
$module_home          = 'http://lepton-cms.org';
$module_guid          = '0ad7e8dd-2f6b-4525-b4bf-db326b0f5ae8';
$module_license       = 'GNU General Public License, TINYMCE is LGPL';
$module_license_terms  = '-';
$module_description   = '<a href="https://www.tiny.cloud" target="_blank">Current TinyMCE </a>allows you to edit the content<br />of a page and see media image folder.
						<br />Please keep in mind that there are <a href="'.LEPTON_URL.'/modules/tinymce/tinymce/TinyMCE-kbd-shortcuts-rev1701.pdf" target="_blank"><b>shortcuts</b></a> to use tinymce';
