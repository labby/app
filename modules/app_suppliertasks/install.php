<?php

/**
 * @module          App-Suppliertasks
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `client_id` int(11) NOT NULL DEFAULT '-1',
	  `section_id` int(11) NOT NULL DEFAULT '-1',
	  `page_id` int(11) NOT NULL DEFAULT '-1',
	  `supplier_id` int(11) NOT NULL DEFAULT '-1',
	  `record_type` int(11) NOT NULL DEFAULT '-1' COMMENT 'invoice or credit note',
	  `record_kind` int(11) NOT NULL DEFAULT '-1' COMMENT 'short or with articles',
	  `record_date` date NOT NULL DEFAULT '0000-00-00',
	  `record_no` varchar(64) NOT NULL DEFAULT '',
	  `record_link` varchar(256) NOT NULL DEFAULT '',
	  `payable` date NOT NULL DEFAULT '0000-00-00',
	  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
	  `payment_terms` int(11) NOT NULL DEFAULT '-1',
	  `purpose` varchar(128) NOT NULL DEFAULT '',
	  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`id`)
	";
LEPTON_handle::install_table("mod_app_suppliertasks", $table_fields);


$table_fields="
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `task_id` int(11) NOT NULL DEFAULT '-1',
	  `article_id` int(11) NOT NULL DEFAULT '-1',
	  `count` decimal(6,2) NOT NULL DEFAULT '-1.00',
	  `article_price` decimal(15,2) NOT NULL DEFAULT '-1.00',
	  `vat_id` int(11) NOT NULL DEFAULT '-1',
	  `article_row` decimal(15,2) NOT NULL DEFAULT '-1.00',
	  `vat_row` decimal(15,2) NOT NULL DEFAULT '-1.00',
	  `last_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
	  `active` int(11) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`id`)
	";
LEPTON_handle::install_table("mod_app_suppliertasks_details", $table_fields);
?>