<?php

/**
 * @module          App-Suppliertasks
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_suppliertasks_frontend extends LEPTON_abstract
{
	use app_admin_fe;
	
	public $all_supplier = array();
	public $vat_keys = array();
	public $payment_terms = array();
	public $invoices = array();
	public $ext_action = LEPTON_URL.PAGES_DIRECTORY.'/mandant/firma/lieferanten.php';

	public $secure_supplier_data = array('company_short','company','extra_1','street','no','zip','city','country','fon','fax','email','internet','company_type','eu_trade','vat_id','bank_name','iban','bic');
	
	public static $instance;
	

	public function initialize() 
	{
		// all vars from class app_admin_fe
		$this->get_all_vars();
		$this->fe_help = LEPTON_URL.'/modules/'.$this->module_directory.'/';
		
		// get invoice templates
		$dir = LEPTON_PATH."/modules/app_admin/templates/invoice/";
		LEPTON_handle::register( "scan_current_dir" );
		$result = scan_current_dir( $dir, 'lte' );		
		$this->invoices = $result['filename'];
		
	}
	
	
public function init_addon( $iPageId = -1, $iSectionId = -1, $iClientId = -99 )
	{
		$this->page_id = $iPageId;
		$this->section_id = $iSectionId;
		if($iClientId != -99)
		{
			$this->client_id = $iClientId;
		}
		else
		{
			die(LEPTON_tools::display($this->language['no_client'], 'pre','ui red message'));
		}

		//get array of all suppliers
		$this->database->secure_execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_supplier WHERE client_id = ".$this->client_id." AND active = 1",
			true,
			$this->all_supplier,
			true,
			$this->secure_supplier_data
		);
		

		//get array of all vat_keys
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_vat_key WHERE active = 1",
			true,
			$this->vat_keys,
			true
		);
	
			
		//get array of payment terms
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_payment_terms WHERE active = 1",
			true,
			$this->payment_terms,
			true
		);			
	}
	
public function get_dataform()
	{
		// force user to chose client
		if($this->client_id < 1 )
		{
			header("Refresh:0; url=".$this->client_url."");
		}

		 // add task short
		if(isset($_POST['add_invoice_short']) || isset($_POST['add_creditnote_short']))
		{
			if(isset($_POST['add_invoice_short']))
			{
				$supplier_id = intval($_POST['add_invoice_short']);
				$record_type = 1; // invoice
			}
			
			if(isset($_POST['add_creditnote_short']))
			{
				$supplier_id = intval($_POST['add_creditnote_short']);
				$record_type = 2; // credit note
			}
			
			$record_kind = 1; // short
			
			//get array of current supplier
			$current_supplier = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_supplier WHERE id = ".$supplier_id,
				true,
				$current_supplier,
				false,
				$this->secure_supplier_data
			);				
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_supplier'	=> $current_supplier,
				'payment_text'	=> $this->database->get_one("SELECT text FROM ".TABLE_PREFIX."mod_app_admin_payment_terms WHERE id = ".$current_supplier['payment_terms']),
				'record_type'	=> $record_type,
				'record_kind'	=> $record_kind
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_suppliertasks');
						
			echo $oTwig->render( 
				"@app_suppliertasks/task_short.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}	



		// display standard page
		// data for twig template engine	
		$item_active = 1;
		$data = array(
			'oAFE'	=> $this,
			'job'	=> $item_active			
		);
		//	get the template-engine.
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('app_suppliertasks');
					
		echo $oTwig->render( 
			"@app_suppliertasks/summary.lte",	//	template-filename
			$data						//	template-data
		);

		return 0;
	}
 } // end class
