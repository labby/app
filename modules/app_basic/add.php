<?php

/**
 * @module          App-Basic
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
 // include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

// this addon can only be used on 1 page in one section
$double = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."sections` WHERE module = 'app_basic'",
	true,
	$double,
	true
);			
			
if(count($double) > 1 ) {
	echo (LEPTON_tools::display('This addon can only be used on one page and one section!','pre','ui red message'));
	
	// get last inserted section
	$to_delete = $database->get_one("SELECT LAST_INSERT_ID() FROM `".TABLE_PREFIX."sections`");
	
	$database->simple_query ("DELETE FROM `".TABLE_PREFIX."sections` WHERE `section_id` = ".$to_delete." ");
	exit();
}


$random_value = rand(100000, 999999);
	
$result = $database->simple_query ("INSERT into ".TABLE_PREFIX."mod_app_basic (page_id, section_id, random) Values(".$page_id.",".$section_id.", ".$random_value.")");
if ($result) {
	echo $database->get_error();
}

$app_media_path = LEPTON_PATH.MEDIA_DIRECTORY.'/app_'.$random_value;

// Create directory
LEPTON_handle::register( "make_dir" );	
if(make_dir($app_media_path)) {
// Add a index.php file to prevent directory
$content = ''.
"<?php

/**
 * @module          App-Basic
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

header('Location: ../');

?>";
$handle = fopen($app_media_path.'/index.php', 'w');
fwrite($handle, $content);
fclose($handle);
change_mode($app_media_path.'/index.php', 'file');
}

	
// add .htaccess file to media folder if not already exist
if (!file_exists($app_media_path.'/.htaccess') or (filesize($app_media_path.'/.htaccess') < 90))
	   {
		// create a .htaccess file to prevent execution of PHP, HMTL files
		$content = '
		<Files .htaccess>
			order allow,deny
			deny from all
		</Files>
	
		<Files ~ "\.(php|pl)$">  
			ForceType text/plain
		</Files>
	
		Options -Indexes -ExecCGI
';
	
$handle = fopen($app_media_path.'/.htaccess', 'w');
fwrite($handle, $content);
fclose($handle);
change_mode($app_media_path.'/.htaccess', 'file');
};

// add additional media directories
make_dir($app_media_path.'/in');
// copy files to new directories first index.php
$source = $app_media_path.'/index.php';
$destination = $app_media_path.'/in/index.php';
copy($source, $destination);
			
// copy files to new directories second htacces
$source = $app_media_path.'/.htaccess';
$destination = $app_media_path.'/in/.htaccess';
copy($source, $destination);

// add additional media directories
make_dir($app_media_path.'/out');
// copy files to new directories first index.php
$source = $app_media_path.'/index.php';
$destination = $app_media_path.'/out/index.php';
copy($source, $destination);
			
// copy files to new directories second htacces
$source = $app_media_path.'/.htaccess';
$destination = $app_media_path.'/out/.htaccess';
copy($source, $destination);
?>