<?php

/**
 * @module          App-Basic
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_basic_frontend extends LEPTON_abstract
{
	use app_admin_fe;

	public $states = array();
	public $months = array();

	public $secure_client_data = array('company','extra_1','street','no','zip','city','country','fon','fax','email','internet','company_type','legal_form','tax_number','vat_id','bank_name','iban','bic');
	
	public static $instance;
	

	public function initialize() 
	{
		// all vars from class app_admin_fe
		$this->get_all_vars();
		$this->fe_help = LEPTON_URL.'/modules/'.$this->module_directory.'/';		
	}
	
	
public function init_addon( $iPageId = -1, $iSectionId = -1, $iClientId = -99 )
	{
		$this->page_id = $iPageId;
		$this->section_id = $iSectionId;
		if($iClientId != -99)
		{
			$this->client_id = $iClientId;
		}
		else
		{
			die(LEPTON_tools::display($this->language['no_client'], 'pre','ui red message'));
		}

		//get array of client data
		$this->client = array();
		$this->database->secure_execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_basic WHERE client_id = ".$this->client_id." ",
			true,
			$this->client,
			false,
			$this->secure_client_data
		);	
		
		//get array of months
		$this->months = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_month ",
			true,
			$this->months,
			true
		);			


		//get array of states
		$this->states = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_state  ",
			true,
			$this->states,
			true
		);

		//get array of settings
		$this->settings = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_basic_settings WHERE client_id = ".$this->client_id." ",
			true,
			$this->settings,
			false
		);			
	}
	
public function get_dataform()
	{
		// set initial value
		$item_active = 0;		
		// force user to chose client
		if($this->client_id < 1 )
		{
			header("Refresh:0; url=".$this->client_url."");
		}
		
		 // save client data
		if(isset($_POST['save_client_data'])&& is_numeric($_POST['save_client_data']) ) 
		{
			$client_id = intval($_POST['save_client_data']);
			$_POST['page_id'] = $this->page_id;
			$_POST['section_id'] = $this->section_id;
			$_POST['last_modified'] = $this->timestamp;	
			$request = LEPTON_request::getInstance();	
//die(LEPTON_tools::display($_POST, 'pre','ui green message'));			
			$all_names = array (
				'section_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'page_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'random'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'company'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'extra_1'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'street'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'no'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'zip'		=> array ('type' => 'string_clean', 'default' => NULL),
				'city' 		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'country'	=> array ('type' => 'string_clean', 'default' => "Deutschland", 'range' =>""),
				'fon'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'fax'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'email'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'internet'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_type'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'legal_form'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'state'			=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'first_month'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'tax_number'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'vat_id'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'bank_name'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'iban'			=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'bic'			=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'last_modified'	=> array ('type' => 'string_clean', 'default' => "0000-00-00 00:00:00", 'range' =>"")
			);
			
			$all_values = $request->testPostValues($all_names);	
			$table = TABLE_PREFIX."mod_app_basic";		
			$result = $this->database->secure_build_and_execute( 'UPDATE', $table, $all_values, 'client_id ='.$client_id, $this->secure_client_data);			
						
			
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}
			else
			{
				// write log entry
				$this->log_entry->insert_secure_entry( 'UPDATE',$this->language['data_id'].$client_id.' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);
			}
		}
		
		 // save client values
		if(isset($_POST['save_client_values'])&& is_numeric($_POST['save_client_values']) ) 
		{
			$client_id = intval($_POST['save_client_values']);
			$_POST['client_id'] = $client_id;
			$_POST['page_id'] = $this->page_id;
			$_POST['section_id'] = $this->section_id;
			$_POST['last_modified'] = $this->timestamp;
			
			$request = LEPTON_request::getInstance();	
//die(LEPTON_tools::display($_POST, 'pre','ui green message'));			
			$all_names = array (
				'client_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'section_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'page_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'customer_account'	=> array ('type' => 'integer', 'default' => "10000", 'range' =>""),
				'supplier_account'	=> array ('type' => 'integer', 'default' => "30000", 'range' =>""),
				'customer_invoice'	=> array ('type' => 'integer', 'default' => "1", 'range' =>""),
				'skr'			=> array ('type' => 'integer', 'default' => "-1", 'range' =>"1-99"),
				'last_modified'	=> array ('type' => 'string_clean', 'default' => "0000-00-00 00:00:00", 'range' =>"")
			);
			
			$all_values = $request->testPostValues($all_names);	
			$table = TABLE_PREFIX."mod_app_basic_settings";		
			$result = $this->database->build_and_execute( 'UPDATE', $table, $all_values, 'client_id ='.$client_id);			
						
			
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}
			else
			{
				// write log entry
				$this->log_entry->insert_secure_entry( 'UPDATE',$this->language['data_id'].$client_id.' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);
			}
		}		
		

		 // display client contact
		if(isset($_POST['client_contact'])&& is_numeric($_POST['client_contact']) ) 
		{
			$client_id = intval($_POST['client_contact']);

			if($_POST['client_contact'] == $client_id ){$item_active = 2;}
			
			// data for twig template engine	
			$data = array(
				'oABFE'	=> $this,
				'client'=> $client_id,
				'job'	=> $item_active
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_basic');
						
			echo $oTwig->render( 
				"@app_basic/client_contact.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}			
	
		 // display client bank
		if(isset($_POST['client_bank'])&& is_numeric($_POST['client_bank']) ) 
		{
			$client_id = intval($_POST['client_bank']);

			if($_POST['client_bank'] == $client_id ){$item_active = 3;}
			
			// data for twig template engine	
			$data = array(
				'oABFE'	=> $this,
				'client'=> $client_id,
				'job'	=> $item_active
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_basic');
						
			echo $oTwig->render( 
				"@app_basic/client_bank.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}	

		 // display client values
		if(isset($_POST['client_values'])&& is_numeric($_POST['client_values']) ) 
		{
			$client_id = intval($_POST['client_values']);

			if($_POST['client_values'] == $client_id ){$item_active = 4;}
			
			// data for twig template engine	
			$data = array(
				'oABFE'	=> $this,
				'client'=> $client_id,
				'job'	=> $item_active
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_basic');
						
			echo $oTwig->render( 
				"@app_basic/client_values.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}			
		
		
		// display standard page
		// data for twig template engine	
		if(!isset($_POST['client_basic']) || $_POST['client_basic'] == $this->client_id ){$item_active = 1;}
		
		$data = array(
			'oABFE'	=> $this,
			'client'=> $this->client_id,
			'job'	=> $item_active
		);
		//	get the template-engine.
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('app_basic');
					
		echo $oTwig->render( 
			"@app_basic/client_basic.lte",	//	template-filename
			$data						//	template-data
		);

		return 0;
	}
 } // end class
