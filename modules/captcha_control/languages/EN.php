<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$MOD_CAPTCHA_CONTROL = array(
// Headings and text outputs
	'HEADING'				=> 'Captcha and ASP control',
	'HOWTO'					=> 'Here you can control the behavior of "CAPTCHA" and "Advanced Spam Protection" (ASP). To get ASP work with a given module, this special module has to be adapted to make use of ASP.',
	'CAPTCHA_EXP'			=> 'CAPTCHA settings for modules are located in the respective module settings',

// Text and captions of form elements
	'ENABLED'				=> 'Enabled',
	'DISABLED'				=> 'Disabled',

	'USE_SIGNUP_CAPTCHA'	=> 'Activate CAPTCHA for signup',

	'ASP_CONF'				=> 'Advanced Spam Protection Configuration',
	'ASP_TEXT'				=> 'Activate ASP (if available)',
	'ASP_EXP'				=> 'ASP tries to determine if a form-input was originated from a human or a spam-bot.',

	'CAPTCHA_TYPE'			=> 'Type of CAPTCHA',

	'GENERIC_CONF'			=> 'Generic Configuration for all captchas',
	'RELOAD_TYPE'			=> 'CAPTCHA reload with',
	'RELOAD_TYPE_IFRAME'	=> 'Classic iframe (if supported by CAPTCHA)',
	'RELOAD_TYPE_JS'		=> 'JavaScript (JQUERY in frontend required)',
	'CAPTCHA_SPEECH'		=> 'Add CAPTCHA to speech',
	'CAPTCHA_SPEECH_RATE'	=> 'Speed rate to be spoken (higher = faster)',
	'CAPTCHA_SPEECH_PITCH'	=> 'Speed pitch for the voice',

	'CAPTCHA_CONF'			=> 'CAPTCHA Configuration',
	'NO_CAPTCHA_CONF'		=> 'No CAPTCHA specific configuration available.',
	'CAPTCHA_DEPRECATED'	=> 'Please note that this CAPTCHA is marked as deprecated and might get removed in future releases.'
								. '<br />'
								. 'Please do not longer use it and switch to another captcha!',
	'PLEASE_SAVE'			=> 'Please save first after CAPTCHA type change, then modify the CAPTCHA configuration next (if available).',
	'SAVE_DONE'				=> 'Configuration is saved.',
	'DEPRECATED'			=> 'Deprecated !',

	'TEST'					=> 'Test',

// labels used on test site
	"RESULT"				=> 'CAPTCHA result',
	"OUTPUT"				=> 'CAPTCHA generated output',
	"PARAMETER"				=> 'Used parameters',
	"TEST_HEADER"			=> 'Playground',
	"TEST_INTRO"			=> 'Note that any on below attribute settings are not saved but just to show you the effect after set.'
								. '<br />'
								. 'Use them in your own modules or templates!',
	"TEST_ACTION"			=> 'Test available actions',
	"TEST_TEXT_ATTR"		=> '[TEXT_ATTR]  &lt;span> attributes for question request',
	"TEST_IMAGE_ATTR"		=> '[IMAGE_ATTR] &lt;img> / &lt;span> attributes for the image',
	"TEST_INPUT_ATTR"		=> '[INPUT_ATTR] &lt;input> attributes for answers entry',

	"VERIFICATION_SUCCEED"	=> ' CAPTCHA Verification successfull.',
	"VERIFICATION_FAILED"	=> ' CAPTCHA Verification failed. Please try again!',

	// captcha calculation settings
	'ENTER_RESULT'			=> 'Fill in the result',

	// CCL = captcha_control Label
	'CCL_ACTION'			=> 'Action',
	"CCL_ITEM_ALL"			=> 'Complete Output with iFrame for reload',
	"CCL_ITEM_ALL_JS"		=> 'Complete Output with JS for reload',
	"CCL_ITEM_IMAGE"		=> 'Output Captcha image',
	"CCL_ITEM_IMAGE_IFRAME"	=> 'Output Captcha as iFrame',
	"CCL_ITEM_INPUT"		=> 'Output Captcha Input Field only',
	"CCL_ITEM_TEXT"			=> 'Output Captcha Input request text only',
	"CCL_ITEM_JS"			=> 'Output JQUERY JS Code for reload',
	"CCL_ITEM_DATA"			=> 'Output Captcha Info as array',

	// captcha calculation settings
	'ADD'					=> 'add',
	'SUBTRACT'				=> 'subtract',
	'MULTIPLY'				=> 'multiply',
	'DIVIDE'				=> 'divide by'
);

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the plugin related
 * translation file, whereas the plugin translation has a higher priority and may 
 * overwrite any translation defined here in.
 * However, this translation file here should contain only translations used in all 
 * or various plugins or inside the captcha module itself.
 */

