<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:02, 28-06-2020
 * translated from..: EN
 * translated to....: FR
 * translated using.: translate.google.com
 * ==============================================
 */

$module_description	= "Outil d'administration pour contrôler CAPTCHA et ASP";

$MOD_CAPTCHA_CONTROL	= array(
	"ASP_CONF"				=> "Configuration avancée de la protection antispam",
	"ASP_EXP"				=> "L'ASP tente de déterminer si un formulaire provient d'un humain ou d'un robot spammeur.",
	"ASP_TEXT"				=> "Activer l'ASP (si disponible)",
	"CAPTCHA_CONF"			=> "Configuration du CAPTCHA",
	"CAPTCHA_DEPRECATED"	=> "Veuillez noter que ce CAPTCHA est marqué comme obsolète et pourrait être supprimé dans les prochaines versions.<br />Veuillez ne plus l'utiliser et passer à un autre captcha !",
	"CAPTCHA_EXP"			=> "Les paramètres CAPTCHA des modules se trouvent dans les paramètres des modules respectifs",
	"CAPTCHA_SPEECH"		=> "Ajouter CAPTCHA au discours",
	"CAPTCHA_SPEECH_PITCH"	=> "Vitesse de la voix",
	"CAPTCHA_SPEECH_RATE"	=> "Vitesse à laquelle on doit parler (plus haut = plus vite)",
	"CAPTCHA_TYPE"			=> "Type de CAPTCHA",
	"DEPRECATED"			=> "Dépréciation !",
	"DISABLED"				=> "Désactivé",
	"ENABLED"				=> "Activé",
	"GENERIC_CONF"			=> "Configuration générique pour tous les captchas",
	"HEADING"				=> "Contrôle de la Captcha et de l'ASP",
	"HOWTO"					=> "Vous pouvez ici contrôler le comportement de 'CAPTCHA' et de 'Advanced Spam Protection' (ASP). Pour que l'ASP fonctionne avec un module donné, ce module spécial doit être adapté à l'utilisation de l'ASP.",
	"NO_CAPTCHA_CONF"		=> "Pas de configuration spécifique CAPTCHA disponible.",
	"OUTPUT"				=> "Production générée par le CAPTCHA",
	"PARAMETER"				=> "Paramètres utilisés",
	"PLEASE_SAVE"			=> "Veuillez d'abord enregistrer après le changement de type de CAPTCHA, puis modifiez ensuite la configuration du CAPTCHA (si disponible).",
	"RELOAD_TYPE"			=> "CAPTCHA recharger avec",
	"RELOAD_TYPE_IFRAME"	=> "Iframe classique (si soutenu par CAPTCHA)",
	"RELOAD_TYPE_JS"		=> "JavaScript (JQUERY dans le frontend requis)",
	"RESULT"				=> "Résultat du CAPTCHA",
	"SAVE_DONE"				=> "La configuration est sauvegardée.",
	"TEST"					=> "Test",
	"TEST_ACTION"			=> "Tester les actions disponibles",
	"TEST_HEADER"			=> "Terrain de jeux",
	"TEST_IMAGE_ATTR"		=> "[IMAGE_ATTR];img> / &lt;span> attributs pour l'image",
	"TEST_INPUT_ATTR"		=> "[INPUT_ATTR] [input> attributes for answers entry",
	"TEST_INTRO"			=> "Notez que tous les paramètres des attributs ci-dessous ne sont pas sauvegardés, mais juste pour vous montrer l'effet après le réglage. <br />Utilisez-les dans vos propres modules ou modèles !",
	"TEST_TEXT_ATTR"		=> "[TEXT_ATTR];span> attributs pour la demande de questions",
	"USE_SIGNUP_CAPTCHA"	=> "Activer le CAPTCHA pour l'inscription",
	"VERIFICATION_FAILED"	=> "Échec de la vérification CAPTCHA. Veuillez réessayer !",
	"VERIFICATION_SUCCEED"	=> "CAPTCHA Vérification réussie.",

	"ADD"						=> "ajouter",
	"CCL_ACTION"				=> "Action",
	"CCL_ITEM_ALL"				=> "Sortie complète avec iFrame pour rechargement",
	"CCL_ITEM_ALL_JS"			=> "Sortie complète avec JS pour rechargement",
	"CCL_ITEM_DATA"				=> "Produire des informations Captcha sous forme de tableau",
	"CCL_ITEM_IMAGE"			=> "Sortie de l'image Captcha",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Sortie Captcha comme iFrame",
	"CCL_ITEM_INPUT"			=> "Champ de saisie du Captcha de sortie uniquement",
	"CCL_ITEM_JS"				=> "Sortie JQUERY JS Code pour le rechargement",
	"CCL_ITEM_TEXT"				=> "Captcha de sortie Texte de demande d'entrée uniquement",
	"DIVIDE"					=> "diviser par",
	"ENTER_RESULT"				=> "Remplir le résultat",
	"MULTIPLY"					=> "multiplier",
	"SUBTRACT"					=> "soustraire"
);

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the plugin related
 * translation file, whereas the plugin translation has a higher priority and may 
 * overwrite any translation defined here in.
 * However, this translation file here should contain only translations used in all 
 * or various plugins or inside the captcha module itself.
 */

?>
