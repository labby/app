<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$module_description 	= 'Admin-Tool um das Verhalten von CAPTCHA und ASP zu kontrollieren';

$MOD_CAPTCHA_CONTROL = array(
// Headings and text outputs
	'HEADING'				=> 'Captcha- und ASP Steuerung',
	'HOWTO'					=> 'Hiermit kann das Verhalten von "CAPTCHA" und "Advanced Spam Protection" (ASP) gesteuert werden. Damit ASP in einem Modul wirken kann, muss das verwendete Modul entsprechend angepasst sein.',
	'CAPTCHA_EXP'			=> 'Die CAPTCHA-Einstellungen für die Module befinden sich in den jeweiligen Modul-Optionen',

// Text and captions of form elements
	'ENABLED'				=> 'Aktiviert',
	'DISABLED'				=> 'Ausgeschaltet',

	'USE_SIGNUP_CAPTCHA'	=> 'CAPTCHA für Registrierung aktivieren',

	'ASP_CONF'				=> 'Erweiterter-Spam-Schutz (ASP) Einstellungen',
	'ASP_TEXT'				=> 'ASP benutzen (wenn im Modul vorhanden)',
	'ASP_EXP'				=> 'ASP versucht anhand der verschiedenen Verhaltensweisen zu erkennen, ob eine Formular-Eingabe von einem Menschen oder einem Spam-Bot kommt.',

	'CAPTCHA_TYPE'			=> 'CAPTCHA-Typ',

	'GENERIC_CONF'			=> 'Generische Konfiguration für alle CAPTCHA',
	'RELOAD_TYPE'			=> 'CAPTCHA neu laden mit',
	'RELOAD_TYPE_IFRAME'	=> 'Klassisches iframe (sofern unterstützt vom CAPTCHA)',
	'RELOAD_TYPE_JS'		=> 'JavaScript (JQUERY im Frontend erforderlich)',
	'CAPTCHA_SPEECH'		=> 'CAPTCHA Sprachausgabe hinzufügen',
	'CAPTCHA_SPEECH_RATE'	=> 'Geschwindigkeit beim Sprechen (höher = schneller)',
	'CAPTCHA_SPEECH_PITCH'	=> 'Geschwindigkeits-Pitch',

	'CAPTCHA_CONF'			=> 'CAPTCHA-Einstellungen',
	'NO_CAPTCHA_CONF'		=> 'Keine spezifischen Einstellungen für das ausgewählte Captcha vorhanden',
	'CAPTCHA_DEPRECATED'	=> 'Bitte beachte das dieses CAPTCHA als veraltet markiert ist und in einer zukünftigen LEPTON Version entfernt wird.'
								. '<br />'
								. 'Bitte verwende ein anderes CAPTCHA!',
	'PLEASE_SAVE'			=> 'Bitte nach Captcha Typ Wechsel zuerst speichern.',
	'SAVE_DONE'				=> 'Konfiguration wurde gespeichert.',
	'DEPRECATED'			=> 'Veraltet !',

	'TEST'					=> 'Test',

// labels used on test site
	"RESULT"				=> 'Generiertes CAPTCHA Resultat',
	"OUTPUT"				=> 'Generierter CAPTCHA Code',
	"PARAMETER"				=> 'Verwendete Parameter',
	"TEST_HEADER"			=> 'Spielwiese',
	"TEST_INTRO"			=> 'Beachte bitte das diese Attribute nur zum Spielen da sind und nicht gespeichert werden.'
								. '<br />'
								. 'Verwende diese Attribute in deinem Modul!',
	"TEST_ACTION"			=> 'Test available actions',
	"TEST_TEXT_ATTR"		=> '[TEXT_ATTR]  &lt;span> Attribute für die Frage',
	"TEST_IMAGE_ATTR"		=> '[IMAGE_ATTR] &lt;img> / &lt;span> Attribute für das Bild',
	"TEST_INPUT_ATTR"		=> '[INPUT_ATTR] &lt;input> Attribute für das Antwort Feld',

	"VERIFICATION_SUCCEED"	=> ' CAPTCHA Prüfung erfolgreich.',
	"VERIFICATION_FAILED"	=> ' CAPTCHA Prüfung fehlgeschlagen. Bitte versuche es nochmals!',

	"ADD"						=> "plus",
	"CCL_ACTION"				=> "Aktion",
	"CCL_ITEM_ALL"				=> "Vollständige Ausgabe mit iFrame zum Nachladen",
	"CCL_ITEM_ALL_JS"			=> "Vollständige Ausgabe mit JS zum Nachladen",
	"CCL_ITEM_DATA"				=> "Captcha-Informationen als Array ausgeben",
	"CCL_ITEM_IMAGE"			=> "Captcha-Bild ausgeben",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Captcha als iFrame ausgeben",
	"CCL_ITEM_INPUT"			=> "Ausgabe nur Captcha-Eingabefeld",
	"CCL_ITEM_JS"				=> "JQUERY JS-Code zum erneuten Laden ausgeben",
	"CCL_ITEM_TEXT"				=> "Ausgabe der Eingabeaufforderung",
	"DIVIDE"					=> "geteilt durch",
	"ENTER_RESULT"				=> "Bitte Ergebnis eintragen",
	"MULTIPLY"					=> "mal",
	"SUBTRACT"					=> "minus"
);

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the plugin related
 * translation file, whereas the plugin translation has a higher priority and may 
 * overwrite any translation defined here in.
 * However, this translation file here should contain only translations used in all 
 * or various plugins or inside the captcha module itself.
 */
