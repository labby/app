<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:08, 28-06-2020
 * translated from..: EN
 * translated to....: RU
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$module_description	= "Admin-Tool для управления CAPTCHA и ASP";

$MOD_CAPTCHA_CONTROL	= array(
	"ASP_CONF"				=> "Расширенная конфигурация защиты от спама",
	"ASP_EXP"				=> "ASP пытается определить, была ли форма-входная была получена от человека или от спам-бота.",
	"ASP_TEXT"				=> "Активировать ASP (при наличии).",
	"CAPTCHA_CONF"			=> "CAPTCHA Конфигурация",
	"CAPTCHA_DEPRECATED"	=> "Обратите внимание, что эта CAPTCHA помечена как устаревшая и может быть удалена в будущих релизах.<br />Please не используйте ее больше и переключитесь на другую капчу!",
	"CAPTCHA_EXP"			=> "Настройки CAPTCHA для модулей находятся в соответствующих настройках модуля.",
	"CAPTCHA_SPEECH"		=> "Добавить CAPTCHA в речь",
	"CAPTCHA_SPEECH_PITCH"	=> "Скорость для голоса",
	"CAPTCHA_SPEECH_RATE"	=> "Скорость разговора (выше = быстрее)",
	"CAPTCHA_TYPE"			=> "Тип КАПТЧА",
	"DEPRECATED"			=> "Искренне!",
	"DISABLED"				=> "&#1042;&#1099;&#1082;&#1083;&#1102;&#1095;&#1077;&#1085;&#1086;",
	"ENABLED"				=> "Включено",
	"GENERIC_CONF"			=> "Общая конфигурация для всех каптх",
	"HEADING"				=> "Контроль Captcha и ASP",
	"HOWTO"					=> "Здесь вы можете контролировать поведение 'CAPTCHA' и 'Advanced Spam Protection' (ASP). Чтобы заставить ASP работать с данным модулем, этот специальный модуль должен быть адаптирован для использования ASP.",
	"NO_CAPTCHA_CONF"		=> "Конфигурация, специфичная для CAPTCHA, не доступна.",
	"OUTPUT"				=> "CAPTCHA генерирует выход",
	"PARAMETER"				=> "Использованные параметры",
	"PLEASE_SAVE"			=> "Пожалуйста, сохраните сначала после смены типа CAPTCHA, а затем измените конфигурацию CAPTCHA (если она доступна).",
	"RELOAD_TYPE"			=> "перезарядка CAPTCHA с",
	"RELOAD_TYPE_IFRAME"	=> "Классический iframe (если поддерживается CAPTCHA)",
	"RELOAD_TYPE_JS"		=> "JavaScript (требуется JQUERY во фронтенде)",
	"RESULT"				=> "Результат КАПТЧА",
	"SAVE_DONE"				=> "Конфигурация сохранена.",
	"TEST"					=> "Тест",
	"TEST_ACTION"			=> "Тестирование доступных действий",
	"TEST_HEADER"			=> "Детская площадка",
	"TEST_IMAGE_ATTR"		=> "[IMAGE_ATTR] &lt;img> / &lt;span> атрибуты для изображения",
	"TEST_INPUT_ATTR"		=> "[INPUT_ATTR] &lt;input> атрибуты для ввода ответа",
	"TEST_INTRO"			=> "Обратите внимание, что любые настройки атрибутов ниже не сохраняются, а только для того, чтобы показать вам эффект после set.<br />Используйте их в своих собственных модулях или шаблонах!",
	"TEST_TEXT_ATTR"		=> "[TEXT_ATTR] &lt;span> атрибуты для запроса вопросов",
	"USE_SIGNUP_CAPTCHA"	=> "Активировать CAPTCHA для регистрации",
	"VERIFICATION_FAILED"	=> "Проверка CAPTCHA не прошла. Пожалуйста, попробуйте еще раз!",
	"VERIFICATION_SUCCEED"	=> "CAPTCHA Проверка прошла успешно.",

	"ADD"						=> "плюс",
	"CCL_ACTION"				=> "Действие",
	"CCL_ITEM_ALL"				=> "Полный выход с iFrame для перезагрузки",
	"CCL_ITEM_ALL_JS"			=> "Полный выход с JS для перезарядки",
	"CCL_ITEM_DATA"				=> "Выходная информация Captcha в виде массива.",
	"CCL_ITEM_IMAGE"			=> "Выходное изображение Captcha",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Выходной Captcha как iFrame",
	"CCL_ITEM_INPUT"			=> "Выход Только поле ввода Captcha",
	"CCL_ITEM_JS"				=> "Выход JQUERY JS Код для перезагрузки",
	"CCL_ITEM_TEXT"				=> "Выходной запрос Captcha Входной запрос только текстовый",
	"DIVIDE"					=> "делить на",
	"ENTER_RESULT"				=> "Заполните результат",
	"MULTIPLY"					=> "умножить на",
	"SUBTRACT"					=> "минус"
);

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the plugin related
 * translation file, whereas the plugin translation has a higher priority and may 
 * overwrite any translation defined here in.
 * However, this translation file here should contain only translations used in all 
 * or various plugins or inside the captcha module itself.
 */

