<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


// get instance of functions file
$oCAPTCHA_CONTROL = captcha_control::getInstance();

// get validation instance
$oREQUEST = LEPTON_request::getInstance();

// connect to database and read out captcha settings
$data = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX . "mod_captcha_control" . "` ",
	true,
	$data,
	false
);

// check if data was submitted
$message = array();
if (( isset( $_POST['save'])) || ( isset( $_POST['run'])))
{
	// add default fields
	$input_fields = array (
		'hash'					=> array ('type' => 'string_clean', 'default' => ''),
		'enabled_captcha'		=> array ('type' => 'integer+', 'default' => $data[ "enabled_captcha" ]),
		'enabled_asp'			=> array ('type' => 'integer+', 'default' => $data[ "enabled_asp" ]),
		'asp_session_min_age'	=> array ('type' => 'integer+', 'default' => $data[ "asp_session_min_age" ]),
		'asp_view_min_age'		=> array ('type' => 'integer+', 'default' => $data[ "asp_view_min_age" ]),
		'asp_input_min_age'	=> array ('type' => 'integer+', 'default' => $data[ "asp_input_min_age" ]),
		'captcha_type'			=> array ('type' => 'string_clean', 'default' => $data[ "captcha_type" ])
	);

	// set validation
	$valid_fields = $oREQUEST->testPostValues($input_fields);

	// check hash: with invalid hash no save and no info ...
	if (( true === isset( $_SESSION['captcha_control_hash'] ))
	 || ( $_SESSION['captcha_control_hash'] == $valid_fields[ "hash" ] ))
	{
		unset( $_SESSION[ "captcha_control_hash" ] );
		unset( $_POST[ "hash" ] );
		unset( $valid_fields[ "hash" ] );

		// update database default settings
		$database->build_and_execute(
			"UPDATE",
			TABLE_PREFIX . "mod_captcha_control",
			$valid_fields,
			""
		);

		// update plugin specific fields
		$oCAPTCHA_CONTROL->captcha_control_save( $valid_fields );

		// reload captcha settings
		$data = array();
		$database->execute_query(
			"SELECT * FROM `".TABLE_PREFIX . "mod_captcha_control" . "` ",
			true,
			$data,
			false
		);

		// show status message
		$message = array( "status"=>true, "text"=>$oCAPTCHA_CONTROL->language[ "SAVE_DONE" ] );
	}
}
elseif ( isset( $_POST['validate']))
{
	// add default fields
	$input_fields = array (
		'hash'		=> array ('type' => 'string_clean', 'default' => ''),
		'captcha'	=> array ('type' => 'string_clean', 'default' => '')
	);

	// set validation
	$valid_fields = $oREQUEST->testPostValues($input_fields);

	// check hash: with invalid hash no save and no info ...
	if (( true === isset( $_SESSION['captcha_control_hash'] ))
	 || ( $_SESSION['captcha_control_hash'] == $valid_fields[ "hash" ] ))
	{
		unset( $_SESSION[ "captcha_control_hash" ] );
		unset( $_POST[ "hash" ] );
		unset( $valid_fields[ "hash" ] );

		//validate the captcha
		$message = array( "status"=>true, "text"=>$oCAPTCHA_CONTROL->language[ "VERIFICATION_SUCCEED" ] );
		if ( false === $oCAPTCHA_CONTROL->test_captcha( false, 0 ))
		{
			$message = array( "status"=>false, "text"=>$oCAPTCHA_CONTROL->language[ "VERIFICATION_FAILED" ] );
		}
	}
}

// build hash
$sHash = $oCAPTCHA_CONTROL->generateHash();
$_SESSION['captcha_control_hash'] = $sHash;

// build field values for twig template
$fieldvalues = array(
	"oCAPTCHA_CONTROL"	=> $oCAPTCHA_CONTROL,
	"hash"				=> $sHash,
	"DATA"				=> $data,
	"GENERIC"			=> $oCAPTCHA_CONTROL->get_generic_config( $data ),
	"PLUGIN"			=> $oCAPTCHA_CONTROL->captcha_control( $data ),
	"USEABLE_PLUGINS"	=> $oCAPTCHA_CONTROL->get_useable_plugins( $data ),
	"MESSAGE"			=> $message
);

// choose template
$template = "tool.lte";

// test handling
if (( isset( $_POST['run'])) || ( isset( $_POST['test'])) || ( isset( $_POST['validate'])))
{
	$template = "test.lte";

	// get plugin defaults of current plugin
	$defaults = $oCAPTCHA_CONTROL->get_plugin_defaults( $data );

	// get testing fields
	$input_fields = array (
		'action'		=> array ('type' => 'string_clean', 'default' => "" ),
		'text_attr'		=> array ('type' => 'string_clean', 'default' => $defaults[ "TEXT_ATTR" ]),
		'image_attr'	=> array ('type' => 'string_clean', 'default' => $defaults[ "IMAGE_ATTR" ]),
		'input_attr'	=> array ('type' => 'string_clean', 'default' => $defaults[ "INPUT_ATTR" ])
	);

	// set validation
	$valid_fields = $oREQUEST->testPostValues($input_fields);

	// get all captcha variations
	$fieldvalues[ "CAPTCHA" ]	= $oCAPTCHA_CONTROL->build_captcha( $valid_fields );
	// return input fields
	$fieldvalues[ "TEST" ]		= $valid_fields;
	// overwrite hash to avoid save on return
	$fieldvalues[ "sHash" ]		= 0;
}

// get Twig-Instance
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule('captcha_control');

// register plugin specific template path
if ( true === array_key_exists( "TEMPLATEPATH", $fieldvalues[ "PLUGIN" ] ))
{
	$oTWIG->registerPath( $fieldvalues[ "PLUGIN" ][ "TEMPLATEPATH" ], "captcha_plugin");
}

echo $oTWIG->render(
	"@captcha_control/" . $template,
	$fieldvalues
);

