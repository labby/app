<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// includes
include( LEPTON_PATH . "/modules/captcha_control/functions/function.get_font.php" );
include( LEPTON_PATH . "/modules/captcha_control/functions/function.get_background.php" );
include( LEPTON_PATH . "/modules/captcha_control/functions/function.get_captcha.php" );


// create the captcha
function create_ttf_image( $page_id, $section_id, $captcha, $dir )
{
	// Get a random font
	$font = get_font( __DIR__ );

	// Get a random background
	$background = get_background( $page_id, $section_id, __DIR__ );
	list( $width, $height, $type, $attr ) = getimagesize( $background );

	if( mt_rand(0,2)==0)
	{ // 1 out of 3

		// draw each character individualy
		$image = ImageCreateFromPNG( $background ); // background image
		$grey = mt_rand(0,50);
		$color = ImageColorAllocate($image, $grey, $grey, $grey); // font-color
		$ttf = $font;
		$ttfsize = 25; // fontsize
		$count = 0;
		$image_failed = true;
		$angle = mt_rand(-10,10);
		$x = mt_rand(20,35);
		$y = mt_rand($height-10,$height-2);
		do 
		{
			for( $i=0; $i < strlen( $captcha[ "CAPTCHA" ] ); $i++ )
			{
				$res = imagettftext($image, $ttfsize, $angle, $x, $y, $color, $ttf, $captcha[ "CAPTCHA" ][ $i ] );
				$angle = mt_rand(-10,10);
				$x = mt_rand($res[4],$res[4]+10);
				$y = mt_rand($height-12,$height-7);
			}
			if ( $res[4] > $width )
			{
				$image_failed = true;
			} 
			else 
			{
				$image_failed = false;
			}
			if ( ++$count > 4) // too many tries! Use the image
			{	
				break;
			}
		} 
		while( $image_failed );
	} 
	else 
	{	
		// draw whole string at once
		$image_failed = true;
		$count=0;
		do 
		{
			$image = ImageCreateFromPNG( $background ); // background image
			$grey = mt_rand(0,50);
			$color = ImageColorAllocate($image, $grey, $grey, $grey); // font-color
			$ttf = $font;
			$ttfsize = 25; // fontsize
			$angle = mt_rand(0,5);
			$x = mt_rand(20,35);
			$y = mt_rand($height-10,$height-2);
			$res = imagettftext( $image, $ttfsize, $angle, $x, $y, $color, $ttf, $captcha[ "CAPTCHA" ] );
			// check if text fits into the image
			if (( $res[0]>0 && $res[0]<$width) && ($res[1]>0 && $res[1]<$height)
			 && ( $res[2]>0 && $res[2]<$width) && ($res[3]>0 && $res[3]<$height)
			 && ( $res[4]>0 && $res[4]<$width) && ($res[5]>0 && $res[5]<$height)
			 && ( $res[6]>0 && $res[6]<$width) && ($res[7]>0 && $res[7]<$height))
			{
				$image_failed = false;
			}
			if ( ++$count > 4 ) // too many tries! Use the image
			{	
				break;
			}
		} 
		while( $image_failed );
	}

	return $image;
}
