<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:57, 28-06-2020
 * translated from..: EN
 * translated to....: IT
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_PLUGIN	= array(
	"CCL_ADD_LANGUAGE"			=> "Aggiungi una nuova lingua con il prossimo salvataggio",
	"CCL_ANSWER"				=> "Risposte",
	"CCL_CASE_SENSITIVE"		=> "Rispondi al controllo delle risposte",
	"CCL_DEL_LANGUAGE"			=> "Per cancellare un set di domande in una lingua, rimuovere tutte le domande e le risposte di quella lingua.",
	"CCL_ITEM_IMAGE"			=> "Uscita Captcha come campata stilizzata",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Solo Captcha",
	"CCL_ITEM_NOT_SENSITIVE"	=> "No, il controllo delle risposte non è sensibile alle maiuscole e minuscole",
	"CCL_ITEM_SENSITIVE"		=> "Sì, il controllo delle risposte è sensibile alle maiuscole e minuscole",
	"CCL_NBR_QUESTIONS"			=> "Numero di domande per la scelta (1-10)",
	"CCL_QA"					=> "Domande e risposte",
	"CCL_QUESTION"				=> "Domande",
	"DEFAULT_ANSWER"			=> "0",
	"DEFAULT_QUESTION"			=> "1 meno 1 è ?",
	"ENTER_RESULT"				=> "Rispondere alla domanda",
	"TEXT"						=> "Testo-CAPTCHA"
);

?>
