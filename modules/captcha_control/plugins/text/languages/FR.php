<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:56, 28-06-2020
 * translated from..: EN
 * translated to....: FR
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_PLUGIN	= array(
	"CCL_ADD_LANGUAGE"			=> "Ajouter une nouvelle langue lors de la prochaine sauvegarde",
	"CCL_ANSWER"				=> "Réponses",
	"CCL_CASE_SENSITIVE"		=> "La vérification des réponses est sensible à la casse",
	"CCL_DEL_LANGUAGE"			=> "Pour supprimer un ensemble de questions sur une langue, il faut supprimer toutes les questions et réponses de cette langue.",
	"CCL_ITEM_IMAGE"			=> "Sortie Captcha en tant que span stylisé",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Captcha uniquement",
	"CCL_ITEM_NOT_SENSITIVE"	=> "Non, le contrôle des réponses n'est pas sensible à la casse",
	"CCL_ITEM_SENSITIVE"		=> "Oui, la vérification des réponses est sensible à la casse",
	"CCL_NBR_QUESTIONS"			=> "Nombre de questions pour le choix (1-10)",
	"CCL_QA"					=> "Questions et réponses",
	"CCL_QUESTION"				=> "Questions",
	"DEFAULT_ANSWER"			=> "0",
	"DEFAULT_QUESTION"			=> "1 moins 1 est ?",
	"ENTER_RESULT"				=> "Répondre à la question",
	"TEXT"						=> "Texte-CAPTCHA"
);

?>
