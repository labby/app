<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:54, 28-06-2020
 * translated from..: EN
 * translated to....: DA
 * translated using.: translate.google.com
 * ==============================================
 */

$MOD_PLUGIN	= array(
	"CCL_ADD_LANGUAGE"			=> "Tilføj nyt sprog med næste gem",
	"CCL_ANSWER"				=> "Svar",
	"CCL_CASE_SENSITIVE"		=> "Besvar kontrolfølsomhed",
	"CCL_DEL_LANGUAGE"			=> "For at slette et sprogspørgsæt skal du fjerne alle spørgsmål og svar på det sprog.",
	"CCL_ITEM_IMAGE"			=> "Captcha output som stylet span",
	"CCL_ITEM_IMAGE_IFRAME"		=> "Kun Captcha",
	"CCL_ITEM_NOT_SENSITIVE"	=> "Nej, svarskontrol er ikke store og små bogstaver",
	"CCL_ITEM_SENSITIVE"		=> "Ja, svarskontrol er store og små bogstaver",
	"CCL_NBR_QUESTIONS"			=> "Antal spørgsmål til valg (1-10)",
	"CCL_QA"					=> "Spørgsmål og svar",
	"CCL_QUESTION"				=> "Spørgsmål",
	"DEFAULT_ANSWER"			=> "0",
	"DEFAULT_QUESTION"			=> "1 minus 1 er?",
	"ENTER_RESULT"				=> "Besvar spørgsmålet",
	"TEXT"						=> "Tekst-CAPTCHA"
);

?>
