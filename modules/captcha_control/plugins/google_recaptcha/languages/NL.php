<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:58, 28-06-2020
 * translated from..: EN
 * translated to....: NL
 * translated using.: www.DeepL.com/Translator
 * ==============================================
 */

$MOD_PLUGIN	= array(
	"CCL_CAPTCHALABEL"			=> "Gedefinieerd Label voor Captcha",
	"CCL_GOOGLE_CONFIG"			=> "Google reCAPTCHA-configuratie-instellingen",
	"CCL_GOOGLE_REGISTER"		=> "Google reCAPTCHA Registratie-instellingen",
	"CCL_ITEM_V2_CHECKBOX"		=> "reCAPTCHA Versie 2 - Checkbox",
	"CCL_ITEM_V2_COMPACT"		=> "compact",
	"CCL_ITEM_V2_DARK"			=> "donker",
	"CCL_ITEM_V2_INVISIBLE"		=> "reCAPTCHA Versie 2 - Onzichtbaar",
	"CCL_ITEM_V2_LIGHT"			=> "licht (* standaard )",
	"CCL_ITEM_V2_NORMAL"		=> "normaal (* standaard )",
	"CCL_ITEM_V3"				=> "reCAPTCHA Versie 3",
	"CCL_SECUREKEY"				=> "Google beveiligde sleutel",
	"CCL_SITEKEY"				=> "Google-sitesleutel",
	"CCL_SIZE"					=> "Grootte",
	"CCL_THEME"					=> "Thema",
	"CCL_VERSION"				=> "Google reCAPTCHA-versie",
	"GOOGLE_RECAPTCHA"			=> "Google Recaptcha"
);

?>
