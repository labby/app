<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

/*
 * Important note
 * ==============
 * This translation file is merged during plugin processing with the captcha module
 * translation file, whereas this plugin translation has a higher priority and may 
 * overwrite any translation defined in the captcha main.
 * However, this translation file here should contain only translations used in this plugin
 * if not already defined in main captcha translation file or divers from there.
 */
 
/* ==============================================
 * translated via...: LEPTON CMS module languager
 * translated at....: 12:54, 28-06-2020
 * translated from..: EN
 * translated to....: DA
 * translated using.: translate.google.com
 * ==============================================
 */

$MOD_PLUGIN	= array(
	"CCL_CAPTCHALABEL"			=> "Defineret etiket til Captcha",
	"CCL_GOOGLE_CONFIG"			=> "Konfigurationsindstillinger for Google reCAPTCHA",
	"CCL_GOOGLE_REGISTER"		=> "Indstillinger for Google reCAPTCHA-registrering",
	"CCL_ITEM_V2_CHECKBOX"		=> "reCAPTCHA version 2 - Afkrydsningsfelt",
	"CCL_ITEM_V2_COMPACT"		=> "kompakt",
	"CCL_ITEM_V2_DARK"			=> "mørk",
	"CCL_ITEM_V2_INVISIBLE"		=> "reCAPTCHA version 2 - Usynlig",
	"CCL_ITEM_V2_LIGHT"			=> "lys (* standard)",
	"CCL_ITEM_V2_NORMAL"		=> "normal (* standard)",
	"CCL_ITEM_V3"				=> "reCAPTCHA version 3",
	"CCL_SECUREKEY"				=> "Googles sikre nøgle",
	"CCL_SITEKEY"				=> "Googles webstedsnøgle",
	"CCL_SIZE"					=> "Størrelse",
	"CCL_THEME"					=> "Tema",
	"CCL_VERSION"				=> "Google reCAPTCHA-version",
	"GOOGLE_RECAPTCHA"			=> "Google Recaptcha"
);

?>
