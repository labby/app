<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 

// include the trait(s)
require_once( LEPTON_PATH . "/modules/captcha_control/classes/plugin_trait.php" );

// includes of functions
include( LEPTON_PATH . "/modules/captcha_control/functions/function.get_captcha.php" );

// create plugin class
class calc_text
{
	// use trait(s)
	use plugin_trait;

	/* ===============================================================================
	 * define defaults for build_captcha
	 *
	 * scope:	frontend,backend
	 * access:	private
	 * param:	none
	 * output:	none
	 */
	private function define_defaults()
	{
		// set plugin specific defaults
		$defaults = array(
			"TEXT_ATTR"		=> array( 
									"class"		=> "captcha_text"
								),
			"IMAGE_ATTR"	=> array( 
									"class"		=> "captcha_image"
								),
			"INPUT_ATTR"	=> array( 
									"type"		=> "number",
									"class"		=> "captcha_input",
									"maxlength"	=> "10",
									"required"	=> "required"
								),
			"COMPARE_CHAR"	=> "&nbsp;=&nbsp;"
		);

		// merge plugin specific defaults with generic defaults
		$this->defaults = $this->merge_arrays( $defaults, $this->defaults );

		// define the captcha input field
		$this->captcha_field = array( "KEY" => "captcha"
									,"REQUEST" => array ('type' => 'integer+', 'default' => -1 ));

		// define list of supported actions
		$this->supported_actions = array( "all", "image", "input", "text", "js", "data" );

		// mark plugin as deprecated
		$this->is_deprecated = false;

		// define if plugin is useable in current configuration
		$this->is_useable = true;
	}

	/* ===============================================================================
	 * calculate the captcha string
	 *
	 * scope:	frontend
	 * access:	public
	 * param:	array with attributes
	 * output:	the captcha
	 */
	public function calculate_captcha( $params )
	{
// Captcha
		$section_id = $_SESSION[ 'captcha_id' ];

		// init
		$t = time();
		$_SESSION[ 'captcha_time' ]	= $t;
		$_SESSION[ 'captcha_id' ]	= $params[ "SECTION_ID" ];

		// calculate the captcha
		$_SESSION['captcha' . $section_id] = '';
		$captcha = get_captcha( "calc" );
		$_SESSION['captcha' . $section_id] = $captcha[ "RESULT" ];
		$_SESSION['captcharequest' . $section_id] = $captcha;

		return $captcha[ "CAPTCHA" ];
	}

}

