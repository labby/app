{#
/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 #}

{% autoescape false %}

<script>

{# reload via JS/JQUERY #}
	{# the calling reference is different against standard #}
{% if RELOAD_TYPE != "IFRAME" %}
	$('#captcha_image_reload').on('click', function()
	{
		$.ajax({
			type: "POST",
			url: "{{ LEPTON_URL }}/modules/captcha_control/return_request.php",
			data: { section_id: "{{ SECTION_ID }}", action : "reload"},
			dataType  : 'json',
			success: function (response) {
				if (response.status)
				{
					$('#captcha_image').prop('title', response.title );
	{# the following line is different against standard #}
					$("#captcha_image").text(response.image );
				}
			}
		});
	});
{% endif %}

{# CAPTCHA to Speech #}
{% if CAPTCHA_SPEECH == 1 %}
	$('#captcha_image_speech').on('click', function()
	{
		if ( 'speechSynthesis' in window ) 
		{
			var lang = "{{ LANGUAGE }}";
			lang = lang.toLowerCase();

			// get text to speech:
			$.ajax({
				type: "POST",
				url: "{{ LEPTON_URL }}/modules/captcha_control/return_request.php",
				data: { section_id: "{{ SECTION_ID }}", action : "title"},
				dataType  : 'json',
				success: function (response) {
					if (response.status)
					{
						// set new title
						$('#captcha_image').prop('title', response.title );
						// Note: comma are not spoken but used as pause between the words
						var text = response.title;

						// new SpeechSynthesisUtterance object
						var utter = new SpeechSynthesisUtterance();
						utter.lang = lang;
						utter.text = text.replace(" ", ",");
						utter.rate = {{ CAPTCHA_SPEECH_RATE }};
						utter.pitch = {{ CAPTCHA_SPEECH_PITCH }};

						// speak
						window.speechSynthesis.speak(utter);
					}
				}
			});
		}
		else
		{
			// hide icon if speech is not supported
			$('#captcha_image_speech').addClass('hidden');
		}
	})
{% endif %}

</script>

{% endautoescape %}
