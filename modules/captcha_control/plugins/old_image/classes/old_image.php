<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */

// include the trait(s)
require_once( LEPTON_PATH . "/modules/captcha_control/classes/plugin_trait.php" );

// create plugin class
class old_image
{
	// use trait(s)
	use plugin_trait;

	/* ===============================================================================
	 * define defaults for build_captcha
	 *
	 * scope:	frontend,backend
	 * access:	private
	 * param:	none
	 * output:	none
	 */
	private function define_defaults()
	{
		// set plugin specific defaults
		$defaults = array(
			"TEXT_ATTR"			=> array( 
									"class"		=> "captcha_text"
								),
			"IMAGE_ATTR"		=> array( 
									"class"		=> "captcha_image",
									"alt"		=> "captcha"
								),
			"INPUT_ATTR"		=> array( 
									"type"		=> "text",
									"class"		=> "captcha_input",
									"maxlength"	=> "10",
									"required"	=> "required"
								),
			"CAPTCHA_WIDTH"		=> 142,
			"CAPTCHA_HEIGHT"	=> 30
		);

		// merge plugin specific defaults with generic defaults
		$this->defaults = $this->merge_arrays( $defaults, $this->defaults );

		// define the captcha input field
		$this->captcha_field = array( "KEY" => "captcha"
									,"REQUEST" => array ('type' => 'string_clean', 'default' => "" ));

		// define list of supported actions
		$this->supported_actions = array( "all", "image", "image_iframe", "input", "text", "js", "data" );

		// mark plugin as deprecated
		$this->is_deprecated = true;

		// define if plugin is useable in current configuration
		if (extension_loaded('gd') && function_exists('imagepng') && function_exists('imagettftext'))
			{ $this->is_useable = true; }
		elseif (extension_loaded('gd') && function_exists('imagepng'))
			{ $this->is_useable = true; }
		else
			{ $this->is_useable = false; }
	}

}

?>
