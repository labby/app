<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


// includes
include( LEPTON_PATH . "/modules/captcha_control/functions/function.get_captcha.php" );


if ( false === isset($_SESSION['captcha_time']))
{
	header("Location: ../index.php");
	die();
}

// Captcha
$section_id = $_SESSION[ 'captcha_id' ];

// get a captcha
$_SESSION['captcha' . $section_id] = '';
$captcha = get_captcha( "random", 5, "num" );
$_SESSION['captcha' . $section_id] = $captcha[ "RESULT" ];
$_SESSION['captcharequest' . $section_id] = $captcha;

// create reload-image
$reload		= ImageCreateFromPNG( __DIR__ . "/images/reload_120_30.png" ); // reload-overlay

$w=120;
$h=30;
$image		= imagecreate($w, $h);
$white		= imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
$gray		= imagecolorallocate($image, 0xC0, 0xC0, 0xC0);
$darkgray	= imagecolorallocate($image, 0x50, 0x50, 0x50);

srand((double)microtime()*1000000);
for($i = 0; $i < 30; $i++) {
	$x1 = rand(0,$w);
	$y1 = rand(0,$h);
	$x2 = rand(0,$w);
	$y2 = rand(0,$h);
	imageline($image, $x1, $y1, $x2, $y2 , $gray);  
}

$x = 0;
for($i = 0; $i < 5; $i++) {
	$fnt = rand(3,5);
	$x = $x + rand(12 , 20);
	$y = rand(7 , 12); 
	imagestring($image, $fnt, $x, $y, substr($_SESSION['captcha'.$section_id], $i, 1), $darkgray); 
}

imagealphablending($reload, TRUE);
imagesavealpha($reload, TRUE);

// overlay
imagecopy($reload, $image, 0,0,0,0, 120,30);
imagedestroy($image);
$image = $reload;

header("Expires: Mon, 1 Jan 1990 05:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, proxy-revalidate");
header("Pragma: no-cache");
header("Content-type: image/png");

ob_start();
imagepng($image);
header("Content-Length: ".ob_get_length()); 
ob_end_flush();
imagedestroy($image);

