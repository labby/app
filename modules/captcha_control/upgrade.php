<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// -------------------------------------------

$module = '/modules/captcha_control';

// delete obsolete files in current version
$file_names = array(
	$module.'/languages/NO.php',				// language not supported by LEPTON
	$module.'/languages/SK.php',				// language not supported by LEPTON
	$module.'/backend.css',						// old & obsolete
	$module.'/frontend.css',					// moved to css folder
	$module.'/captcha/reload_120_30.png',		// moved to related plugin
	$module.'/captcha/reload_140_40.png',		// moved to related plugin
	$module.'/captcha/readme.txt'				// moved into LEPTON documention
);

LEPTON_handle::delete_obsolete_files($file_names);

$dir_names = array(
	$module.'/captcha/fonts',					// moved to lib_lepton
	$module.'/captcha/captchas',				// moved to plugins
	'/templates/lepsem/backend/captcha_control'	// no longer required
);
LEPTON_handle::delete_obsolete_directories($dir_names);

// check the content of text
$database = LEPTON_database::getInstance();
// get questions and answers
$text_ct = $database->get_one("SELECT ct_text FROM ".TABLE_PREFIX."mod_captcha_control ");

// process only if not empty, not already a json and also not default example
if (( false === is_null( $text_ct )) 
 && ( false === empty( $text_ct ))
 && ( "{" != substr( $text_ct, 0, 1 ))
 && ( !preg_match('/### .*? ###/', $text_ct )))
{
	$settings = array();
	$max = array();

	// transform old content into new json format
	$content = explode("\n", $text_ct);

	reset( $content );
	while( $s = current($content))
	{
		// get question
		$s=trim(rtrim(rtrim($s,"\n"),"\r")); // remove newline
		if($s=='' || $s[0] !='?') {
			next($content);
			continue;
		}
		if(isset($s[3]) && $s[3] ==':')
		{
			$lang=substr($s,1,2);
			$q=substr($s,4);
		} else
		{
			$lang='XX';
			$q=substr($s,1);
			if($q=='')
			{
				next($content);
				continue;
			}
		}

		// get answer
		$s=next($content);
		$s=trim(rtrim(rtrim($s,"\n"),"\r")); // remove newline
		if(isset($s[0]) && $s[0] =='!') {
			$a=substr($s,1);

			// prepare array for save
			if ( false === array_key_exists( $lang, $max ))
				{ $max[ $lang ] = 0; }
			$max[ $lang ]++;
			$settings[ $lang . "_question_" . $max[ $lang ] ] = $q;
			$settings[ $lang . "_answer_" . $max[ $lang ] ]   = $a;

			next($content);
		}
	}

	// set number of questions
	$c = 0;
	foreach( $max as $lang => $i )
	{
		if ( $i > $c )
		{ 
			$c = $i;
			$settings[ "nbr_questions"] = $c;
		}
	}

	$final[ "text" ] = $settings;

	// update database
	$database->build_and_execute(
		"UPDATE"
		,TABLE_PREFIX . "mod_captcha_control"
		,array( "ct_text" => json_encode( $final ) )
		,""
	);
}

// delete obsolete files in current version
$plugin = $module.'/plugins';
$file_names = array(
	$plugin.'/calc_image/info.php',
	$plugin.'/calc_text/info.php',
	$plugin.'/calc_ttf_image/info.php',
	$plugin.'/google_recaptcha/info.php',
	$plugin.'/old_image/info.php',
	$plugin.'/text/info.php',
	$plugin.'/ttf_image/info.php'
);

LEPTON_handle::delete_obsolete_files($file_names);
