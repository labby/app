<?php

/**
 * This file is part of an ADDON for use with LEPTON Core.
 * This ADDON is released under the GNU GPL.
 * Additional license terms can be seen in the info.php of this module.
 *
 *
 *	@module			captcha
 *	@version		see info.php of this module
 *	@authors		LEPTON Project, W. Studer
 *	@copyright		2020-2021 LEPTON Project
 *	@link			https://lepton-cms.org
 *	@license		http://www.gnu.org/licenses/gpl.html
 *	@license_terms	please see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/class.secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


/* ------------------------------
 *
 * Note:
 * *****
 *
 * Normally, the captcha creation & validation should be called directly.
 * However, modules might not have been adapted and still use old call.
 * With this script, we support this old behaviour.
 *
 * Also note that in old behaviour, google recaptcha was not supported 
 * in previous captcha_control module!
 *
 * ------------------------------
 */


function call_captcha( $action, $style, $section_id )
{
	// set default params
	$params = array(
		 "ACTION"		=> $action
		,"SECTION_ID"	=> $section_id
	);

	// set - based on old logic - action specific styles
	switch( $action )
	{
		case "image":
			$params[ "IMAGE_ATTR" ]	= $style;
			break;
		case "image_iframe":
			if ( CAPTCHA_TYPE != "calc_text" )
				{ $params[ "IMAGE_ATTR" ]	= $style; }
			break;
		case "input":
			$params[ "INPUT_ATTR" ]	= $style;
			break;
		case "text":
			$params[ "TEXT_ATTR" ]	= $style;
			break;
	}

	// get instance of captcha plugin
	require_once(LEPTON_PATH . "/modules/captcha_control/classes/captcha.php" );
	$oCAPTCHA = captcha::getInstance();

	// build the captcha
	echo $oCAPTCHA->build_captcha( $params );
}
