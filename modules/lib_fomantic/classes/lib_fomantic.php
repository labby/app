<?php

/**
 *  @module      	Library Fomantic
 *  @version        see info.php of this module
 *  @author         LEPTON project
 *	@copyright      2018-2020 LEPTON Project
 *  @license        http://opensource.org/licenses/MIT
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

class lib_fomantic extends LEPTON_abstract
{

    const CHECKBOX_DEFAULT_STATE = 1;
    
    const BASEPATH = LEPTON_PATH."/modules/lib_fomantic/dist/";
    
    // Own instance for this class!
    static $instance;
    
    public function initialize()
    {
    
    }
    
    /**
     *  Public static function to generate the HTML-Source for a checkbox.
     *
     *  @param  string  $sName      The name if the element.
     *  @param  string  $sElementID The ID if the element.
     *  @param  string  $sLabel     The label if the element.
     *  @param  string  $sValue     The Value of the element.
     *  @param  integer $iState     The state of the element (1 == checked, 0 == unchecked").
     *  @param  string  $iClass     Optional (css-)classname.
     *
     *  @return string  The generated HTML source code.
     */
    static public function BuildCheckbox($sName, $sElementID, $sLabel, $sValue, $iState=0, $iClass="")
    {
        $iState= intval($iState);
        if( ($iState != 0) && ($iState != 1) )
        {
            $iState = self::CHECKBOX_DEFAULT_STATE;
        }
        
        if($iClass != "")
        {
            $iClass = " class='".$iClass."'";
        }
        
        $sHTMLCode  = "\n<div class='ui checkbox".($iState==1 ? " checked" : "")."'>";
        $sHTMLCode .= "\n<label for='".$sElementID."'>".$sLabel."</label>\n";
        $sHTMLCode .= "\n<input type='checkbox' name='".$sName."' id='".$sElementID."' value='".$sValue."' ". $iClass . ($iState == 1 ? " checked='checked' " : " ")." >\n";
        $sHTMLCode .= "</div>\n";
        
        return $sHTMLCode;
    }

}
