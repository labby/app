<?php

/**
 * @module          App-Supplier
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */
 
class app_supplier_frontend extends LEPTON_abstract
{
	use app_admin_fe;
	
	public $all_supplier = array();
	public $basic_settings = array();
	public $payment_terms = array();
	public $invoices = array();

	public $secure_supplier_data = array('company_short','company','extra_1','street','no','zip','city','country','fon','fax','email','internet','company_type','eu_trade','vat_id','bank_name','iban','bic');
	
	public static $instance;
	

	public function initialize() 
	{
		// all vars from class app_admin_fe
		$this->get_all_vars();
		$this->fe_help = LEPTON_URL.'/modules/'.$this->module_directory.'/';		
		
		// get invoice templates
		$dir = LEPTON_PATH."/modules/app_admin/templates/invoice/";
		LEPTON_handle::register( "scan_current_dir" );
		$result = scan_current_dir( $dir, 'lte' );		
		$this->invoices = $result['filename'];
		
	}
	
	
public function init_addon( $iPageId = -1, $iSectionId = -1, $iClientId = -99 )
	{
		$this->page_id = $iPageId;
		$this->section_id = $iSectionId;
		if($iClientId != -99)
		{
			$this->client_id = $iClientId;
		}
		else
		{
			die(LEPTON_tools::display($this->language['no_client'], 'pre','ui red message'));
		}
	
		//get array of all suppliers
		$this->database->secure_execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_supplier WHERE client_id = ".$this->client_id." AND active = 1",
			true,
			$this->all_supplier,
			true,
			$this->secure_supplier_data
		);
		
		//get array of payment terms
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_app_admin_payment_terms WHERE active = 1",
			true,
			$this->payment_terms,
			true
		);			
	}
	
public function get_dataform()
	{
		// force user to chose client
		if($this->client_id < 1 )
		{
			header("Refresh:0; url=".$this->client_url."");
		}

		 // add supplier form
		if(isset($_POST['add_supplier'])&& $_POST['add_supplier'] = -1 ) 
		{
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_supplier');
						
			echo $oTwig->render( 
				"@app_supplier/supplier_add.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}	

		
		 // delete supplier data
		if(isset($_POST['supplier_delete']) && is_numeric($_POST['supplier_delete']) ) 
		{	

			$supplier_id = intval($_POST['supplier_delete']);	
			$table = TABLE_PREFIX."mod_app_supplier";		
			$result = $this->database->simple_query("UPDATE ".$table." SET active = 0, last_modified ='".$this->timestamp."' WHERE id = ".$supplier_id."  ");			
	
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}	
			else
			{
				// write log entry
				$this->log_entry->insert_secure_entry( 'UPDATE',$this->language['data_id'].$this->language['supplier_deleted'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);
			}
		}

		
		 // insert new supplier data
		if(isset($_POST['save_new_supplier'])&& is_numeric($_POST['save_new_supplier']) ) 
		{	
			$last_supllier_id = $this->database->get_one("SELECT LAST_INSERT_ID() FROM ".Table_PREFIX."mod_app_supplier");
			$current_no = $this->database->get_one("SELECT account_no FROM ".Table_PREFIX."mod_app_supplier WHERE id = ".$last_supllier_id );
			
			$_POST['client_id'] = $this->client_id;	
			$_POST['last_modified'] = $this->timestamp;	
			
			$request = LEPTON_request::getInstance();	
			
			$all_names = array (
				'client_id'=> array ('type' => 'integer', 'default' => "-99", 'range' =>""),
				'section_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'page_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'company_short'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'extra_1'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'street'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'no'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'zip'		=> array ('type' => 'string_clean', 'default' => NULL),
				'city' 		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'country'	=> array ('type' => 'string_clean', 'default' => "Deutschland", 'range' =>""),					
				'fon'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'email'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'internet'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_type'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'eu_trade'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'vat_id'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'account_no'	=> array ('type' => 'integer', 'default' => "-99", 'range' =>""),				
				'last_modified'	=> array ('type' => 'string_clean', 'default' => "0000-00-00 00:00:00", 'range' =>"")
			);
			
			$all_values = $request->testPostValues($all_names);	
			$table = TABLE_PREFIX."mod_app_supplier";		
			$result = $this->database->secure_build_and_execute( 'INSERT', $table, $all_values, '', $this->secure_supplier_data);			
						
			
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}	
			else
			{			
				// Create supplier directory
				$supplier_id = $this->database->get_one("SELECT LAST_INSERT_ID() FROM ".$table." ");
				$app_media_path = LEPTON_PATH.MEDIA_DIRECTORY.'/app_'.$this->random_value;
				$supplier_media_path = $app_media_path.'/in/'.$this->language['media_client'].'_'.$this->client_id.'/'.$this->language['media_supplier'].'_'.$supplier_id;
				LEPTON_handle::register( "make_dir" );	
				make_dir($supplier_media_path);
				
				// copy files to new directories first index.php
				$source = $app_media_path.'/index.php';
				$destination = $supplier_media_path.'/index.php';
				copy($source, $destination);
							
				// copy files to new directories second htacces
				$source = $app_media_path.'/.htaccess';
				$destination = $supplier_media_path.'/.htaccess';
				copy($source, $destination);
				
				// write log entry
				$this->log_entry->insert_secure_entry( 'INSERT',$this->language['data_id'].$this->language['supplier_new'].' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);
			}
		}	
		
		
		 // save supplier data
		if(isset($_POST['save_supplier_data'])&& is_numeric($_POST['save_supplier_data']) ) 
		{
		
			$current_supplier_id = intval($_POST['save_supplier_data']);
			$_POST['last_modified'] = $this->timestamp;
			if($_POST['account_no'] == -99)
			{
				$_POST['account_no'] = $this->basic_settings['supplier_account'] + $current_supplier_id;
			}
//die(LEPTON_tools::display($_POST, 'pre','ui blue message'));			
			$request = LEPTON_request::getInstance();	
			
			$all_names = array (
				'client_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'section_id'=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'page_id'	=> array ('type' => 'integer', 'default' => "-1", 'range' =>""),
				'company_short'	=> array ('type' => 'string_clean', 'default' => "-1", 'range' =>""),
				'company'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'extra_1'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'vat_id'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'eu_trade'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_type'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'street'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'no'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'zip'		=> array ('type' => 'string_clean', 'default' => NULL),
				'city' 		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'country'	=> array ('type' => 'string_clean', 'default' => "Deutschland", 'range' =>""),
				'company_salutation'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_name1'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_name2'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'fon'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'fax'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'email'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'internet'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'company_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'extra_1_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'street_del'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'no_del'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'zip_del'		=> array ('type' => 'string_clean', 'default' => NULL),
				'city_del' 		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'country_del'	=> array ('type' => 'string_clean', 'default' => "Deutschland", 'range' =>""),				
				'bank_name'		=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'iban'			=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'bic'			=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),
				'sepa'			=> array ('type' => 'integer', 'default' => "0", 'range' =>""),
				'account_no'	=> array ('type' => 'integer', 'default' => "-99", 'range' =>""),				
				'payment_terms'	=> array ('type' => 'string_clean', 'default' => "-1", 'range' =>""),
				'price_terms'	=> array ('type' => 'string_clean', 'default' => "-1", 'range' =>""),
				'send_invoice'	=> array ('type' => 'integer', 'default' => "0", 'range' =>""),	
				'invoice_type'	=> array ('type' => 'string_clean', 'default' => "", 'range' =>""),				
				'last_modified'	=> array ('type' => 'string_clean', 'default' => "0000-00-00 00:00:00", 'range' =>"")
			);
			
			$all_values = $request->testPostValues($all_names);	
			$table = TABLE_PREFIX."mod_app_supplier";		
			$result = $this->database->secure_build_and_execute( 'UPDATE', $table, $all_values, 'id ='.$current_supplier_id, $this->secure_supplier_data);			
						
			
			if($result == false) {
				echo (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}
			else
			{
				// write log entry
				$this->log_entry->insert_secure_entry( 'UPDATE',$this->language['data_id'].$current_supplier_id.' '.$this->language['data_table'].$table.' '.$this->language['from_user_id'].$_SESSION['USER_ID'] );

				// save = ok and forward to start screen
				echo(LEPTON_tools::display($this->language['save_ok'],'pre','ui green message'));
				header("Refresh:0; url=".$this->fe_action."");	
				exit(0);
			}
		}

		 // display supplier details
		if(isset($_POST['supplier_details'])&& is_numeric($_POST['supplier_details']) ) 
		{
			$current_supplier_id = intval($_POST['supplier_details']);
			
			//get array of current_supplier
			$current_supplier = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_supplier WHERE id = ".$current_supplier_id." ",
				true,
				$current_supplier,
				false,
				$this->secure_supplier_data
			);	
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_supplier'	=> $current_supplier,
				
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_supplier');
						
			echo $oTwig->render( 
				"@app_supplier/supplier_details.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}




		// display supplier contact
		if(isset($_POST['supplier_contact'])&& is_numeric($_POST['supplier_contact']) ) 
		{
			$current_supplier_id = intval($_POST['supplier_contact']);
			
			//get array of current_supplier
			$current_supplier = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_supplier WHERE id = ".$current_supplier_id." ",
				true,
				$current_supplier,
				false,
				$this->secure_supplier_data
			);	
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_supplier'	=> $current_supplier,
				
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_supplier');
						
			echo $oTwig->render( 
				"@app_supplier/supplier_contact.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}	

		// display supplier delivery_address
		if(isset($_POST['supplier_delivery'])&& is_numeric($_POST['supplier_delivery']) ) 
		{
			$current_supplier_id = intval($_POST['supplier_delivery']);
			
			//get array of current_supplier
			$current_supplier = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_supplier WHERE id = ".$current_supplier_id." ",
				true,
				$current_supplier,
				false,
				$this->secure_supplier_data
			);	
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_supplier'	=> $current_supplier,
				
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_supplier');
						
			echo $oTwig->render( 
				"@app_supplier/supplier_delivery.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}			
	
		 // display supplier bank
		if(isset($_POST['supplier_bank'])&& is_numeric($_POST['supplier_bank']) ) 
		{
			$current_supplier_id = intval($_POST['supplier_bank']);
			
			//get array of current_supplier
			$current_supplier = array();
			$this->database->secure_execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_app_supplier WHERE id = ".$current_supplier_id." ",
				true,
				$current_supplier,
				false,
				$this->secure_supplier_data
			);	
			
			// data for twig template engine	
			$data = array(
				'oAFE'			=> $this,
				'current_supplier'	=> $current_supplier,
				
			);
			//	get the template-engine.
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('app_supplier');
						
			echo $oTwig->render( 
				"@app_supplier/supplier_bank.lte",	//	template-filename
				$data						//	template-data
			);

			return 0;			
		}

		// display standard page
		// data for twig template engine	
		$item_active = 1;
		$data = array(
			'oAFE'	=> $this,
			'job'	=> $item_active			
		);
		//	get the template-engine.
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('app_supplier');
					
		echo $oTwig->render( 
			"@app_supplier/summary.lte",	//	template-filename
			$data						//	template-data
		);

		return 0;
	}
 } // end class
