<?php

/**
 * @module          App-Supplier
 * @author          erpe
 * @copyright       2020-2021 erpe
 * @link            https://os-app.org
 * @license         Custom License
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php


//get instance of own module class
$oABE = app_supplier::getInstance();

	// Additional marker settings
$form_values = array(
	'oABE'			=> $oABE,
	'section_id'	=> $section_id,
	'page_id'		=> $page_id,	
	'leptoken'		=> get_leptoken()
);

/**	
 *	get the template-engine.
 */
$oTwig = lib_twig_box::getInstance();
$oTwig->registerModule('app_supplier');
	
echo $oTwig->render( 
	"@app_supplier/modify.lte",	//	template-filename
	$form_values				//	template-data
);	


